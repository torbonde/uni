﻿namespace Simulation

open Nessos.Streams
open MathNet.Numerics.Distributions

module Payout =
    /// Call payout function
    let call K S = max (S-K) 0.0

    /// Put payout function
    let put K S = max (K-S) 0.0

module Hobson =
    /// Random number generator
    let N01 =
        let N = Normal()
        let r = System.Random(1)
        N.RandomSource <- r
        N
       
    /// Next Gaussian random number
    let rnd () = N01.Sample()

    /// State carried through calculations
    type State =
        {
            t : float;
            S : float;
            Y : float;
        }
        static member getPrice state = state.S

    /// Parameters specifying model
    type Model =
        {
            lambda : float -> float;
            rho : float -> float;
            theta : float;
            mu : float;
            sigma : float;
        }

    /// Function to do a single simulation
    let private simulate m T model payout initState =
        let rho' t = 1. - (model.rho t)**2. |> sqrt
        let h = T/(float m)
        [|1..m|]
        |> Stream.ofArray
        |> Stream.fold (fun s i ->
            let B = rnd()
            let Z = rnd()
            let Y' = 
                if i = 1 
                then s.Y
                else s.Y + (model.theta * (model.mu - s.Y)
                    - model.sigma * s.Y * (model.rho s.t)) *
                    + model.sigma * s.Y * (sqrt h)
                        * ((model.rho s.t) * B + (rho' s.t) * Z)
            let S' = s.S + (sqrt h) * s.S * Y' * B
            { t = s.t + h; S = S'; Y = Y'})
            initState
        |> State.getPrice
        |> payout

    /// Function to do N simulations
    let valuate N m T model payout initState =
        let sum =
            [|1..N|]
            |> Stream.ofArray
            |> Stream.map (fun _ -> simulate m T model payout initState)
            |> Stream.sum
        sum/(float N)
