\documentclass[a4paper]{article}
\input{../preamble}
\begin{document}
\title{Risk measures}
\author{Gyongi}
\maketitle
Let $X\in\calX$, wher $\calX$ denotes the set of bounded random variables. We
want to find a number $\rho(X)\in\reals$, ``Minimum capital $m$ such that having
$X+m$ eliminates the risk''. We consider the following natural properties natural
(or required):
\begin{enumerate}
\item\label{item:1} (Monotonicity) If $X \leq Y$ then we will have $\rho(X)\geq\rho(Y)$.
\item\label{item:2} (Cash invariance) For every $m\in\reals$, $\rho(X+m) = \rho(X)-m$.
\end{enumerate}

These properties are not very restrictive, so there is a lot of monetary
measures of risk.

Assume that $\calX$ contains the constants.

\begin{defn}
  If $\rho : \calX \to \reals$ is a mapping, such that \ref{item:1} and
  \ref{item:2} hold, then $\rho$ is called a \emph{monetary measure of risk}.
\end{defn}

Clearly, $\rho(\rho(X)+X) = \rho(X) - \rho(X) = 0$. In particular,
$\rho(\rho(0)) = 0$. Also, $\rho(m) = \rho(0)-m$ for every $m\in\reals$. We say
that $\rho$ is normalised if $\rho(0) = 0$.

\begin{prop}
  If $\rho$ is a monetary measure of risk (MMR), then
  \begin{equation}
    \label{eq:1}
    |\rho(X) - \rho(Y)| \leq \sup_\Omega |X-Y|,
  \end{equation}
  for every $X,Y\in\calX$.
\end{prop}

In the following, we will write $\norm{X} = \sup_{\omega\in\Omega}|X(\omega)|$.
\begin{proof}
  $X\leq Y + \norm{X-Y}$ gives us that $\rho(X) \geq \rho(X + \norm{X-Y}) =
  \rho(Y) - \norm{X-Y}$. So then $\norm{X-Y} \geq \rho(Y) - \rho(X)$. We can
  make the same argument with $X$ and $Y$ interchanged, so we get $\norm{X-Y}
  \geq |\rho(Y) - \rho(X)|$.
\end{proof}

``The risk is not increasing, by diversifying the resources.''
\begin{defn}
  $\rho$ is convex if
  \begin{equation}
    \label{eq:2}
    \rho(\lambda X + (1-\lambda)Y) \leq \lambda\rho(X) + (1-\lambda)\rho(Y)
  \end{equation}
  for all $\lambda\in[0,1]$, $X,Y\in\calX$.
\end{defn}

\begin{defn}
  If a MMR $\rho$ is convex, it is called a convex risk-measure.
\end{defn}

\begin{defn}
  We say that $\rho$ is \emph{positively homogenous} if $\rho(\lambda X) =
  \lambda\rho(X)$ for all $\lambda\geq 0$ and $X\in\calX$.
\end{defn}

\begin{defn}
  If $\rho$ is a positively homogenous convex risk measure, then it is called a
  coherent risk measure.
\end{defn}

Notice that if $\rho$ is positively homogenous convex, then it is subadditive, i.e.
\begin{equation}
  \rho(X+Y) \leq \rho(X) + \rho(Y)
\end{equation}
for all $X,Y\in\calX$.
\begin{proof}
  $\rho(2(X+Y)/2) = 2\rho((X+Y)/2) \leq 2(\rho(X)/2 + \rho(Y)/2) = \rho(X) + \rho(Y)$.
\end{proof}

Now consider $\calA_\rho = \{X\in\calX : \rho(X) \leq 0\}$ for an MMR
$\rho$. This is called an acceptance set.

\begin{prop}\label{prop:1}
  Let $\calA = \calA_\rho$ be an acceptance set, for an MMR $\rho$. Then the
  following statements hold:
  \begin{enumerate}
  \item\label{item:3} $\calA \neq \emptyset$ (\textbf{Proof:} trivially $\rho(0)\in\calA$).
  \item\label{item:4} $\inf\{c\in\reals : c\in\calA\} > -\infty$ (\textbf{Proof:} $\rho(c) =
    \rho(0) - c$ for all $c\in\reals$. For $c < -\rho(0)$ we get that
    $c\not\in\calA$).
  \item\label{item:5} If $X\in\calA$ and $Y\in\calX$, such that $X\leq Y$, then $Y\in\calA$
    (\textbf{Proof:} $\rho(Y)\leq\rho(X)\leq 0$).
  \item For $X\in\calA$ and $Y\in\calX$,
    \begin{equation}
      \label{eq:3}
      \Lambda = \{ \lambda\in[0,1] : \lambda X + (1-\lambda)Y\in\calA\}
    \end{equation}
    is a closed set in $[0,1]$. (\textbf{Proof:} Notice that $f(\lambda) =
    \rho(\lambda X + (1-y) Y)$ is continuous in $\lambda$. $f(\lambda)\leq 0$ if
    $\lambda\in\Lambda$, by definition. Taking a convergent sequence
    $(\lambda_n)$, and taking limits we see that $f(\lambda_n)\in\Lambda$, which
    means that $\Lambda$ is closed).
  \item $\rho(X) = \inf\{m : m + X\in\calA\}$. (\textbf{Proof:} $\inf\{m :
    m+X\in\calA\} = \inf\{m\in\reals : \rho(m + X)\leq 0\} = \inf\{m\in\reals :
    \rho(X) - m \leq 0\} = \rho(X)$).
  \item $\rho$ is a convex measure of risk iff $\calA$ is
    convex. (\textbf{Proof:} $X,Y\in\calA$ iff $\rho(X) \leq 0$, $\rho(Y)\leq
    0$, which implies that $\rho(\lambda X + (1-\lambda)Y) \leq \lambda \rho(X)
    + (1-\lambda)\rho(Y) \leq 0$. Then $\lambda X + (1-\lambda) Y\in\calA$. The
    other way is a little more difficult. But let's suppose $\calA$ is
    convex. Then $X,Y\in\calX$ implies that $X + \rho(X)\in\calA$ and $X +
    \rho(Y)\in\calA$. By convexity of $\calA$, $\lambda X + (1-\lambda) +
    \lambda\rho(X) + (1-\lambda) \lambda(Y) = \lambda X + (1-\lambda) +
    m\in\calA$. Then $\rho(\lambda X + (1-\lambda) + m) \leq 0$, which means
    that $\rho(\lambda X + (1-\lambda)) \leq m = \lambda\rho(X) + (1-\lambda)
    \lambda(Y)$).
  \item $\rho$ is positively homogenous iff $\calA$ is a cone (i.e. $\lambda
    X\in\calA$ whenever $\lambda\geq 0$, $X\in\calA$).
  \end{enumerate}
\end{prop}

\begin{prop}
  Let $\calA\subset\calX$ satisfying \ref{item:3}, \ref{item:4} and \ref{item:5}
  in Proposition \ref{prop:1}. Then the following statements hold
  \begin{enumerate}[(a)]
  \item $\rho_\calA(X) = \inf\{m\in\reals : X + m\in\calA\}$ for $X\in\calX$ is
    an MMR.
  \item $\calA$ is convex implies that $\rho_\calA$ is a convex risk measure.
  \item $\calA$ is positively homogenous implies that $\rho_\calA$ is positively
    homogenous.
  \end{enumerate}
\end{prop}

\section*{Problems}

\end{document}
