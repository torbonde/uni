#r "/Users/torbonde/Projects/FooBar/packages/FSharp.Data.2.1.1/lib/net40/FSharp.Data.dll"

open FSharp.Data

type CashTri = CsvProvider< @"/Users/torbonde/windows/Cash TRI.csv",";">

let tri = CashTri.GetSample()
tri.Rows
|> Seq.map (fun row -> row.``ESG.Economies.GBP.NominalYieldCurves.NominalYieldCurve.CashTotalReturnIndex``)
|> Seq.nth 2
