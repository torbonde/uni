\select@language {english}
\contentsline {section}{\numberline {1}Risk-neutral pricing}{1}
\contentsline {section}{\numberline {2}The model}{3}
\contentsline {section}{\numberline {3}Choosing measure}{4}
\contentsline {subsection}{\numberline {3.1}The $q$-optimal measure}{5}
\contentsline {section}{\numberline {4}An application}{7}
\contentsline {subsection}{\numberline {4.1}Simulation basics}{7}
\contentsline {subsection}{\numberline {4.2}Results}{9}
\contentsline {section}{\numberline {A}Code}{9}
