## Script for producing PnL histograms
# Set auto scaling
set autoscale

# Remove legend
unset key

# Which file to load. You better hope it's there!
ndays = 10

# Width of the bins -- note that this is not the number of bins!
binwidth = 0.25

# This function determines where to put the boxes in the histogram.
# These are centered around the middle value they represent.
bin(x,width) = width*floor(x/width) + width/2.0

# Set output type
# PNG
#set term postscript size 800,600
set terminal postscript eps size 3.5,2.62 enhanced color \
    font 'Helvetica,10' linewidth 1
# Set output
imagefile = sprintf("ibm_histogram_%d_days.eps", ndays)
set output imagefile

# Set input
datafile = sprintf("ibm_returns_%d_days.txt", ndays)

# Fix box width
set boxwidth binwidth

# Basic statistics
stats datafile using 1 prefix "S"

# Distribution
Gauss(x,mu,sigma) = 1./(sigma*sqrt(2*pi)) * exp( -(x-mu)**2 / (2*sigma**2))

# VaR 
VaR = 4.93778309*sqrt(ndays)

#set arrow from (VaR),0 to (VaR),Gauss(VaR,S_mean,S_stddev) nohead
#set xtics add (VaR VaR)

ymax = 0.13
#ymax = 0.35
set arrow from -VaR,0 to -VaR,ymax nohead lt 0

# Plot the histogram
plot [] [0:ymax] datafile using (bin($1,binwidth)):(1/(binwidth*S_records)) smooth freq with boxes, \
     Gauss(x,0,S_stddev) with lines title "Normal distribution", \
     Gauss(x,0,S_stddev) with filledcurves below x1=-VaR