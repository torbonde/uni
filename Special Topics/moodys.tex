\documentclass[a4paper]{article}
\input{../preamble}
\usepackage{natbib}
%\bibliographystyle{alpha}

\DeclareMathOperator{\CTRI}{CTRI}

\DeclareRobustCommand{\actuarial}[2][]{%
  \def\arraystretch{0}%
  \setlength\arraycolsep{0.5pt}%
  \setlength\arrayrulewidth{0.5pt}%
  \setbox0=\hbox{$\scriptstyle#1#2$}%
  \begin{array}[b]{*2{@{}>{\scriptstyle}c}|}
    \cline{2-2}%
    \rule[1.25pt]{0pt}{\ht0}%
    #1 & #2%
  \end{array}%
}
\begin{document}
\title{Liability valuation\\Moody's assignment 1}
\author{David Tor Bonde}
\date{April 1, 2015}
\maketitle
\tableofcontents

\section{Introduction}
In this paper we will concern ourselves with the valuation of two parts of a
product offered by a UK based insurance company. It is assumed that this company
has sold a number of products, that offers the policyholders a lump sum of
\textsterling 500,000 at retirement. It is furthermore assumed, rather
unrealistically perhaps, that all policyholders will retire in precisely 15
years. This will make the valuation a great deal simpler.

As part of the product, the insurance company offers their customers to use the
lump sum to buy an annuity with a maturity of 20 years, that pays a fixed amount
each year. This amount, and thus the price of the annuity, depends on the
interest rates at retirement. In particular, as a guard against low interest
rates, the insurance company offers their policyholders a guarantee that pays at
least \textsterling 35,000 at the end of each year, for 20 years.

The actual valuation will make use of the Economic Scenario Generator (ESG). The
following section will describe how the ESG is set up, and Sections
\ref{sec:model} and \ref{sec:antithetic-variates} will describe certain parts of
the model and a variance reduction technique used in the simulations,
respectively. Section \ref{sec:valuation} describes the actual valuation in
three parts in Sections \ref{sec:pricing-liability}, \ref{sec:determ-annu-paym}
and \ref{sec:pricing-guarantee}.

\subsection{Setting up the Economic Scenario Generator}
The ESG was set up to run 10,000 trials and to use antithetic variates for
variance reduction purposes, as described below. Since the purpose of using the
ESG was to do valuation we ran our simulation on a market-consistent basis, as
opposed to a real-world basis, which is more commonly used for risk management
tasks. That is, we used risk-neutral valuation. We added a GBP economy, using
the Extended 2 Factor Black-Karasinski model for the nominal interest rates, as
presented in Section \ref{sec:model}. The ESG was calibrated using the
\texttt{EndDec2013\_MC\_GOVT\_Global\_62+\_Complete.bhc}-calibration file, and
the base date was set accordingly to December 31, 2013.

\subsection{Extended 2 Factor Black-Karasinski model}\label{sec:model}
For modeling the nominal interest rates we have used an extension of the 2
Factor Black-Karasinski model, which takes the yield curve as an explicit
input. In this model, the log-short rate $\ln(r)$ is modeled as a mean-reverting
process, with the mean-reversion level $\ln(m)$ itself being a mean-reverting
process. Under the risk-neutral measure $\probQ$ the dynamics of these processes
are given by
\begin{align}
  \diff\ln(r_t) &= \alpha\pex{1}(\ln(m_t) - \ln(r_t))\,\diff t +
  \sigma\pex{1}\,\diff W\pex{1}_t\\
  \diff\ln(m_t) &= \alpha\pex{2}(\mu_t-\ln(m_t))\,\diff t + \sigma\pex{2}\,\diff
  W_t\pex{2}
\end{align}
where $W\pex{1}$ and $W\pex{2}$ are independet Brownian motions and
$\alpha\pex{1}$, $\alpha\pex{2}$, $\sigma\pex{1}$ and $\sigma\pex{2}$ are input
parameters for the model. The process $\mu$ is chosen such that the model fits
the inital terms structure of interest rates. Under this model the short rate
has a log-normal distribution, with explicit expressions for the mean and
variance. In particular, this ensures that the short rate is positive at all
times. For more info see \cite{moodydoc}.

\subsection{Antithetic variates}\label{sec:antithetic-variates}
As a method of reducing variance in our estimates, we have used the technique
called \emph{antithetic variates}. The goal of this method is to introduce a
negative correlation between consecutive trials. Suppose that we wish to
estimate $\expect[f(U)]$, where $f$ is some function, and $U$ is a uniform
variable on the $[0,1]$-interval. Then we will also have that $1-U\sim U(0,1)$.

Now suppose that at first we estimate the expectation by
$\frac{1}{N}\sum_{i=1}^Nf(U_i)$ for some $N>0$ and independent $U_i\sim
U(0,1)$. Using the method of antithetic variates we may approximate the
expecation instead by
\begin{equation}
  \label{eq:12}
  \theta = \frac{1}{2N}\sum_{i=1}^N(f(U_i) + f(1-U_i))
\end{equation}

By the Central Limit Theorem
\begin{equation}
  \label{eq:13}
  \frac{\theta-\expect[f(U)]}{\sigma/\sqrt{N}} \Rightarrow N(0,1),
\end{equation}
where $\Rightarrow$ is notation for convergence in distribution and the standard
deviation $\sigma$ is given by
\begin{equation}
  \label{eq:14}
  \sigma^2 = \Var\Biggl[\frac{f(U_i) + f(1-U_i)}{2}\Biggr]
\end{equation}
The method of antithetic variates gives a reduction in variance of the estimate
if
\begin{equation}
  \label{eq:15}
  2\Var[f(U_i)] > \Var[f(U_i) + f(1-U_i)] = 2\Var[f(U_i)] + 2\Cov[f(U_i),f(1-U_i)],
\end{equation}
i.e. when the variables $f(U_i)$ and $f(1-U_i)$ are negatively correlated. 

In this short desription, we have explained antithetic variates in terms of
uniformly distributed random variables. Note that we could equally well have
focused on, say, normally distributed random variables $X_i\sim
N(\mu,\sigma^2)$, with antithetic partners $\tilde{X}_i = 2\mu - X_i\sim
N(\mu,\sigma^2)$. For further references on antithetic variates and other
variance reduction techniques in Monte Carlo simulation, see
\cite{glasserman2003monte}.

\section{Valuation}\label{sec:valuation}
In this section we will describe the actual valuation of the two parts of the
product, namely the lump sum and the guarantee. These prices were calculated
using the following formula, for calculating the value at time 0 of a future cash
flow $X$ at time $T$
\begin{equation}
  \label{eq:16}
  V_0 = \expect_\probQ\Biggl[\frac{X_T}{C_T}\Biggr],
\end{equation}
wherein $C_T$ is the cash roll-up until time $T$ and $\probQ$ is the
risk-neutral probability measure. In our simulation, we have used $C_T =
\CTRI(T)$, the Cash Total Return Index.

\subsection{Pricing the liability}\label{sec:pricing-liability}
In the following we will ignore fees, commisions, margins for profits, etc. Now
the price of the product paying the lump sum must be determined by the size
of the lump sum, and the interest rates over the following 15 years. In fact,
the price of the liability is given by
\begin{equation}
  \label{eq:1}
  \text{Price of liability} = \textsterling 500,000 \cdot \expect_\probQ\Bigl[e^{-\int_0^{15}r_t\,\diff t}\Bigr]
\end{equation}
where $r$ denotes the short rate. We can use the Economic Scenario Generator
(ESG) to calculate this value. This was done using the Cash Total Return Index
(CTRI), which at time $t$ is given by
\begin{equation}
  \label{eq:6}
  \CTRI(t) = e^{\int_0^t r_s\,\diff s} = \frac{1}{P(0,t)}.
\end{equation}
The CTRI's output from the ESG are discritized using $\CTRI(t) = \CTRI(t-\Delta
t) e^{r_{t-\Delta t}\Delta t}$, where $\Delta t$ is the time-step and $\CTRI(0)
= 1$. Let $C_i$ for $1\leq i\leq 10,000$ denote the values of $\CTRI(15)$
generated by the ESG. Then for each scenario $i$,
$\exp\bigl(\int_0^{15}r_t\,\diff t\bigr) = P(0,15) = \frac{1}{C_i}$, and using
Equation \eqref{eq:1} the price of the liability was calculated by
\begin{equation}
  \label{eq:2}
  \text{Price of liability} = \frac{\textsterling 500,000}{10,000} \sum_{i=1}^{10,000}\frac{1}{C_i}\approx \textsterling 290,900.
\end{equation}

For comparison, if we assume that the interest rate $i$ remains constant over the
next 15 years, we get the approximate values
\begin{center}
  \begin{tabular}{lcc}
    & $i\equiv 0.01$ & $i\equiv 0.06$\\\midrule
    \text{Price of liability} & \textsterling 430,700 & \textsterling 208,600
  \end{tabular}
\end{center}

\subsection{Determining the annual payments}\label{sec:determ-annu-paym}
Upon retirement the policyholders will have the option to invest the lump sum in
an annuity from the insurance company. The annuity will pay an amount $X$ at the
of end of each year, for 20 years. The payment amount can be calculated using
the formula
\begin{equation}
  \label{eq:3}
  \textsterling 500,000 = X\sum_{t=16}^{35}P(15,t) = X a_{\actuarial{20}}
\end{equation}
where $P(15,t)$ is the price of the zero coupon bond with maturity $t$ at time
15, and $a_{\actuarial{20}}$ is the price of an annuity paying 1 unit
at the end of each year, for 20 years. Noting that the price of the annuity is
given by
\begin{equation}
  \label{eq:4}
  a_{\actuarial{20}} = \frac{1-(1+i)^{-20}}{i}
\end{equation}
where $i$ is the rate of interest per annum, we can calculate the value $X$ that
equates the price of the offered annuity with the lump sum using
\begin{equation}
  \label{eq:5}
  X = \frac{\textsterling 500,000\cdot i}{1-(1+i)^{-20}}
\end{equation}
Now if the interest rate at retirement at retirement is assumed to be constant,
we get the appoximate values of $X$
\begin{center}
  \begin{tabular}{lcc}
  & $i\equiv 0.01$ & $i\equiv 0.06$\\\midrule
  $X$ & \textsterling 27,700 & \textsterling 43,600
  \end{tabular}
\end{center}

\subsection{Pricing the guarantee}\label{sec:pricing-guarantee}
If the interest rates after year 15 were to fall, the lump sum would decrease in
value. For retirees who wish to protect themselves against such low interest
rates, the insurance company offers an annuity at time $t=15$ that pays at least
\textsterling 35,000 at the end of each year, instead of a one-time payment of
\textsterling 500,000. The actual payoff of this annuity will then be $A =
\textsterling 35,000 \sum_{t=16}^{35} P(15,t)$, which depends on the interest
rates observed at time $t=15$.

Now consider the scenario in whch $A = \textsterling 500,000$. In this case the
policyholder will get \textsterling 35,000 each year, and the guarantee will
have a payoff of 0. If $A > \textsterling 500,000$, the annuity will pay more
that \textsterling 35,000 a year, and the insurance company will pay this
difference to the customer, i.e. the payoff of the guarantee will be
$A-\textsterling 500,000$. In this instance, the observed interest rates at year
15 was lower than what was expected when the contract was written. 

In the case where interest rates are such that $A < \textsterling 500,000$, the
retiree will still get an annual payment of \textsterling 35,000, but the
guarantee will pay off 0. Combining these three cases, and the definition of
$A$, we see that the payoff of the guarantee can be written as
\begin{equation}
  \label{eq:8}
  \Bigl(\textsterling 35,000\sum_{t=16}^{35}P(15,t) - \textsterling 500,000\Bigr)^+
\end{equation}

\subsubsection{Calculating the additional premium}
Using the payoff in Equation \eqref{eq:8}, the additional premium the company
should charge is the payoff discounted until $t=0$. That is
\begin{equation}
  \label{eq:7}
  \text{Premium} = \expect_\probQ\Bigl[e^{-\int_0^{15}r_t\,\diff
    t}\Bigl(\textsterling 35,000\sum_{t=16}^{35}P(15,t) - \textsterling
  500,000\Bigr)^+\Bigr]
\end{equation}

To calculate this premium using the ESG we output annuity prices with maturity
20 for year 15. For the discount-factor we will use the CTRI(15). This gives us
10,000 annuity prices $a_i$ and 10,000 CTRI-values $C_i$, $1\leq i\leq
10,000$, which we use to calculate the premium by
\begin{equation}
  \label{eq:9}
  \text{Premium} = \frac{1}{10,000}\sum_{i=1}^{10,000}\frac{(\textsterling
    35,000 a_i - \textsterling 500,000)^+}{d_i} \approx \textsterling 15850
\end{equation}

In the simulation we have used antithetic variates to reduce variance. Using
10,000 trials we have 5,000 antithetic pairs, giving independent identically
distributed pairs of possible payoff values $(Y_i, \tilde{Y}_i)$, $1\leq i\leq
5,000$. The variance is then
\begin{equation}
  \label{eq:10}
  \sigma^2 = \Var\Biggl[\frac{Y_i + \tilde{Y}_i}{2}\Biggr]
\end{equation}
and the 95\% confidence interval has endpoints $\text{Premium} \pm
\Phi\inv(0.025)\frac{\sigma}{\sqrt{5,000}}$. Here $\Phi$ is the standard normal
cumulative distribution function. From the output from the ESG, the confidence
interval is calculated to be
\begin{equation}
  \label{eq:11}
  CI_{95\%} = (15,401\ ,\ 16,288)
\end{equation}

\subsection{Weaknesses}
The time horizon of this valuation is 15 years, which is quite a large period of
time. Looking back, it is quite evident that a lot can happen to an economy over
the course of 15 years, as for instance a large, financial crisis. Although the
ESG was calibrated to historic data, it might still fail to put enough emphasis
on such extreme events.

Another rare, but not impossible event, is that interest rates become negative,
which for example has happened in Denmark quite recently. However, the extended
2 factor Black-Karasinski model we have used in our simulation does not allow
for negative interest rates. Since negative interest rates are quite rare, this
might still make sense, although one has to keep in mind that this could
potentially have a large impact on the premium estimated in Section
\ref{sec:pricing-guarantee}, as compared to the actual payoff.
\nocite{*}
\bibliographystyle{plain}
\bibliography{literature}
\end{document}