﻿#r "/Users/torbonde/Projects/FooBar/packages/FSharp.Data.2.1.1/lib/net40/FSharp.Data.dll"

module Seq =
  let partition p sq =
    let a = sq |> Seq.filter p
    let b = sq |> Seq.filter (p >> not)
    (a,b)

  let apply f sq = sq |> Seq.map (fun x -> (x, f x))

open FSharp.Data

type Scorecard = CsvProvider<"/Users/torbonde/scorecard.csv",";">

let scorecard = Scorecard.GetSample()

type Group =
  {
    Score : float
    Customers : float
    Defaults : float
  }

//////////////////// Try new scorecard ///////////
type RawData = CsvProvider<"""/Users/torbonde/uni/Special Topics/Retail Credit Risk/raw_data.csv""", ";">
let rawdata = RawData.GetSample()

let ftb (r: RawData.Row) =
  let f = r.Ftb
  if f
  then 0.
  else 5.

let inc (r: RawData.Row) =
  let i = r.Inc
  if i = 0
  then 30.
  elif i = 1
  then 16.
  elif i = 2
  then 25.
  elif i = 3
  then 42.
  elif i = 4
  then 50.
  else 55.

let tia (r: RawData.Row) =
  let t = r.Tia
  if t = 0
  then 0.
  elif t = 1
  then 16.
  elif t = 2
  then 26.
  elif t = 3
  then 32.
  else 42.

let n_cust (r: RawData.Row) = r.N_custs |> float
let n_def (r: RawData.Row) = r.N_defaults |> float

let intercept = 310.

let score r = (ftb r) + (inc r) + (tia r) + intercept
let scoreGroups =
  rawdata.Rows
  |> Seq.groupBy score
  |> Seq.map (fun (s, rows) ->
              {
                Score = s
                Customers = rows |> Seq.sumBy n_cust
                Defaults = rows |> Seq.sumBy n_def
              })
  |> Seq.sortBy (fun g -> g.Score)

//////////////////////////////////////////////////

let scoreGroups' =
  scorecard.Rows
  |> Seq.map (fun row ->
              {
                Score = row.Score |> float
                Customers = row.``Number of customers at this score`` |> float
                Defaults = row.``Number of defaults recorded at this score`` |> float
              })

// Hand-slides, page 10
let misclassificationTable t =
  let accepted, notAccepted =
    scoreGroups
    |> Seq.partition (fun g -> g.Score > t)
  let acceptedDefaults = accepted |> Seq.sumBy (fun g -> g.Defaults)
  let acceptedNonDefaults = accepted |> Seq.sumBy (fun g -> g.Customers  - g.Defaults)
  let notAcceptedDefaults = notAccepted |> Seq.sumBy (fun g -> g.Defaults)
  let notAcceptedNonDefaults = notAccepted |> Seq.sumBy (fun g -> g.Customers - g.Defaults)
  (acceptedNonDefaults, acceptedDefaults, notAcceptedNonDefaults, notAcceptedDefaults)

let n t =
  let (a, b, c, d) = misclassificationTable t
  a+b+c+d

let n0 t =
  let (a, _, c, _) = misclassificationTable t
  a+c

let n1 t =
  let (_, b, _, d) = misclassificationTable t
  b+d

let pi0 t = (n0 t)/(n t)

let pi1 t = (n1 t)/(n t)

let (c0, c1) = (200., 10.)

let F0 t =
  let (a, _, c, _) = misclassificationTable t
  a/(a+c)

let F1 t =
  let (_, b, _, d) = misclassificationTable t
  1. - (d/(b+d))

let loss t = c0*(pi0 t)*(1.-(F0 t)) + c1*(pi1 t)*(F1 t)

let scores =
  scoreGroups
  |> Seq.map (fun g -> g.Score)

let losses = scores |> Seq.map loss

// FnuPlot

#r "/Users/torbonde/Projects/FnuPlot/bin/FnuPlot.dll"

open FnuPlot
open System.Drawing

let path = "/usr/local/bin/gnuplot"
let gp = new GnuPlot(path)
gp.Set(output = Output(X11, font="arial"))
//gp.Set(output = Output(Png("/Users/torbonde/ROC.png")))
let pngPath = """/Users/torbonde/uni/Special Topics/Retail Credit Risk/new_"""

let scoresAndLosses = (scores, losses) ||> Seq.zip
let (minLossScore, minLoss) = scoresAndLosses |> Seq.minBy snd

// Plot loss
let plotLoss() =
  seq {
    yield scoresAndLosses |> Series.Lines
    yield seq { yield (365., 3.); yield (365., 10.) } |> Series.Lines
  }
  |> gp.Plot

let roc =
  seq {
    yield (scores |> Seq.min) - 1.
    yield (scores |> Seq.max) + 1.
    yield! scores
  }
  |> Seq.map (fun s -> (F1 s, F0 s))
  |> Seq.sortBy fst

let plotRoc () =
  gp.Set(output = Output(Eps(pngPath + "roc.eps")))
  gp.SendCommand("set key right bottom")
  seq {
    yield Series.Lines(roc, title = "ROC")
    let diag : seq<float*float> =
      [0. .. 0.01 .. 1.] |> Seq.map (fun x -> (x,x))
    yield Series.Lines(diag, title = "Random")
    //let cutoff = Seq.singleton  (F1 365., F0 365.)
    //yield Series.Points(cutoff, lineColor = Color.Blue, weight=2)
  }
  |> gp.Plot
  gp.Set(output = Output(X11, font="arial"))

let diff h f = (fun x -> ((f (x-h)) - (f (x+h)))/(2.*h))

seq { yield scores |> Seq.apply (diff 1. F1) |> Series.Lines; yield scores |> Seq.apply (diff 1. F0) |> Series.Lines } |> gp.Plot

// Quadrature using trapezoidal rule
let quad series =
    series
    |> Seq.pairwise
    |> Seq.sumBy (fun ((a, fa), (b, fb)) -> (b-a)*(fa+fb)/2.)
// AUC using trapizoidal rule
let aucTrap = quad roc
let giniTrap = 2.*aucTrap - 1.

// AUC using Hand's rule
let fs =
  scoreGroups
  |> Seq.map (fun g ->
              (g, (g.Customers - g.Defaults)/g.Customers))
  |> Seq.collect (fun (g, p) ->
                  Seq.init (int (g.Customers - g.Defaults)) (fun _ -> p))

let gs =
  scoreGroups
  |> Seq.map (fun g ->
              (g, g.Defaults/g.Customers))
  |> Seq.collect (fun (g, p) ->
                  Seq.init (int g.Defaults) (fun _ -> p))

let combined =
  let gs' = gs |> Seq.map (fun p -> (p, 1))
  let fs' = fs |> Seq.map (fun p -> (p, 0))
  Seq.append gs' fs'

let ranked =
  combined
  |> Seq.sortBy (fun (p, _) -> p)
  |> Seq.mapi (fun i (p, c) -> (i+1, p, c))

let S0 =
  ranked
  |> Seq.filter (fun (_, _, c) -> c = 0)
  |> Seq.sumBy (fun (rank, _, _) -> rank |> int64)
  |> float

let n0 = scoreGroups |> Seq.sumBy (fun g -> g.Customers - g.Defaults)
let n1 = scoreGroups |> Seq.sumBy (fun g -> g.Defaults)

let auc = (S0 - n0*(n0+1.)/2.)/(n0*n1)
  
let goodToBadRates =
  scores
  |> Seq.map (fun s -> (s,(F0 s)/(F1 s)))

let plotGoodToBadRates() =
  seq {
    yield goodToBadRates |> Series.Lines
    let low = goodToBadRates |> Seq.minBy snd |> snd
    let high = goodToBadRates |> Seq.maxBy snd |> snd
    yield seq { yield (365., low); yield (365., high) } |> Series.Lines
  }
  |> gp.Plot

let defaults = scoreGroups |> Seq.sumBy (fun g -> g.Defaults)
let nondefaults = scoreGroups |> Seq.sumBy (fun g -> g.Customers - g.Defaults)
let acceptedDefaults t =
  scoreGroups
  |> Seq.filter (fun g -> g.Score > t)
  |> Seq.sumBy (fun g -> g.Defaults)
  |> fun d -> d/defaults
let acceptedNonDefaults t =
  scoreGroups
  |> Seq.filter (fun g -> g.Score > t)
  |> Seq.sumBy (fun g -> g.Customers - g.Defaults)
  |> fun n -> n/nondefaults

let minScore = scores |> Seq.min
let maxScore = scores |> Seq.max

let xscores =
  scores
  |> Seq.append (Seq.singleton (minScore-1.))
  |> Seq.append (Seq.singleton (maxScore+1.))
  |> Seq.sort

let plotAcceptRates() =
  let low = xscores |> Seq.min
  gp.Set(output = Output(Eps(pngPath + "rates.eps")), range = RangeX.[low .. ])
  gp.SendCommand("set xlabel 'Cut-off'")
  gp.SendCommand("set ylabel 'Accepted customers within class (%)'")
  gp.SendCommand("set key right top")
  seq {
    let ds = xscores |> Seq.map (fun s -> (s, (acceptedDefaults s)*100.))
    yield Series.Lines(ds, title = "Accepted defaults")
    let nds = xscores |> Seq.map (fun s -> (s, (acceptedNonDefaults s)*100.))
    yield Series.Lines(nds, title = "Accepted non-defaults")
    let cut = seq { yield (365., 0.); yield (365., 100.) }
    yield Series.Lines(cut, title = "Cut-off = 365")
  } |> gp.Plot
  gp.Set(output = Output(X11, font="arial"))
  
let plotAcceptActual() =
  let low = xscores |> Seq.min
  gp.Set(output = Output(Eps(pngPath + "actual.eps")), range = RangeX.[low .. ])
  gp.SendCommand("set key right top")
  gp.SendCommand("set xlabel 'Cut-off'")
  gp.SendCommand("set ylabel 'Accepted customers'")
  seq {
    let nds = xscores |> Seq.map (fun s -> (s, (acceptedNonDefaults s)*nondefaults))
    let nds' = Seq.append (seq { yield (low, 0.) }) nds
    
    let ds = xscores |> Seq.map (fun s -> (s, (acceptedDefaults s)*defaults))
    let ds' =
      ds
      |> Seq.append (seq { yield (low, 0.) })
      |> Seq.zip nds'
      |> Seq.map (fun ((s, n), (_, d)) -> (s,n+d))
    yield Series.FilledCurves(ds', title = "Accepted defaults", lineColor = Color.DarkGray)
    yield Series.FilledCurves(nds', title = "Accepted non-defaults", lineColor = Color.LightGray)
    
    let cut = seq { yield (365., 0.); yield (365., 100000.) }
    yield Series.Lines(cut, title = "Cut-off = 365")
  } |> gp.Plot
  gp.Set(output = Output(X11, font="arial"))

// Profit
let loss = 200.
let profit = 10.
let totalProfit t =
  let acceptedNonDefaults, acceptedDefaults, _, _ =  misclassificationTable t
  acceptedNonDefaults*profit - acceptedDefaults*loss

let plotProfit() =
  gp.Set(output = Output(Eps(pngPath + "profit.eps")))
  gp.SendCommand("set xlabel 'Cut-off'")
  gp.SendCommand("set ylabel 'Profit (GBP)'")
  let profitFunction = scores |> Seq.apply totalProfit
  let maxProfitScore, maxProfit = profitFunction |> Seq.maxBy snd
  seq {
    yield Series.Lines(profitFunction, title = "Profit")
    yield Series.Lines(seq { yield (365., 0.); yield (365., 600000.) }, title = "Cut-off = 365")
    yield Series.Lines(seq { yield (maxProfitScore, 0.); yield (maxProfitScore, 600000.)}, title = sprintf "Cut-off = %i" (int maxProfitScore))
  }
  |> gp.Plot
  gp.Set(output = Output(X11, font="arial"))

let maxProfitScore =
  scores
  |> Seq.apply totalProfit
  |> Seq.maxBy snd
  |> fst

let acceptRate =
  let (a, b, c, d) = misclassificationTable maxProfitScore
  (a+b)/(a+b+c+d)

let defaultRate =
  let (a, b, c, d) = misclassificationTable maxProfitScore
  b/(a+b)

// Accuracy
  
type Bucket =
  | Lower of float
  | Middle of float * float
  | Upper of float

let buckets n =
  let length = maxScore - minScore |> float
  let bucketSize = length/n
  let lowerBucket = (float minScore) + bucketSize |> Lower |> Seq.singleton
  let upperBucket = (float maxScore) - bucketSize |> Upper |> Seq.singleton
  let middleBuckets =
    [((float minScore) + bucketSize)..bucketSize..((float maxScore)-bucketSize)]
    |> Seq.map (fun a -> Middle (a, a+bucketSize))
  upperBucket |> Seq.append middleBuckets |> Seq.append lowerBucket

let getBucket bs s =
  printfn "Score %f" s
  bs
  |> Seq.find (function
               | Lower b -> s < b
               | Middle (a, b) -> a <= s && s < b
               | Upper a -> s >= a)

let groupedActualDefaults =
  let bs = buckets 10.
  scoreGroups
  |> Seq.groupBy (fun g -> getBucket bs g.Score)
  |> Seq.map (fun (bucket, groups) ->
              let prob = groups |> Seq.sumBy (fun g -> g.Defaults/g.Customers)
              (bucket, prob))
  |> Seq.mapi (fun i (_, prob) -> (i+1 |> float, prob))

let badOddsAtScore s = 2.**((250.-s)/20.)
let probabilityOfDefaultFromBadOdds b = b/(1.+b)
let predictedDefaultProbabilities =
  scores |> Seq.apply (badOddsAtScore >> probabilityOfDefaultFromBadOdds)

let groupedPredictedDefaults =
  let bs = buckets 10.
  predictedDefaultProbabilities
  |> Seq.groupBy (fst>>getBucket bs)
  |> Seq.map (fun (b, ps) -> (b, ps |> Seq.sumBy snd))
  |> Seq.mapi (fun i (b,d) -> (i+1 |> float, d))

let plotDefaultGroups() =
  gp.Set(output = Output(Eps(pngPath + "grouped.eps")))
  gp.SendCommand("set xlabel 'Decile'")
  gp.SendCommand("set ylabel 'Default rate (%)'")
  let gad = groupedActualDefaults |> Seq.map (fun (x,y) -> (x, 100.*y))
  let gpd = groupedPredictedDefaults |> Seq.map (fun (x,y) -> (x, 100.*y))
  seq {
    yield Series.Lines(gad, title = "Actual")
    yield Series.Lines(gpd, title = "Predicted")
  }
  |> gp.Plot
  gp.Set(output = Output(X11, font="arial"))

let logGoodOdds =
  groupedActualDefaults
  |> Seq.filter (fun (_, dr) -> dr <> 0.)
  |> Seq.map (fun (i, dr) -> (i, (1.-dr)/dr |> log))

let plotLogGoodOdds() =
  gp.Set(output = Output(Eps(pngPath + "logodds.eps")))
  gp.SendCommand("set xlabel 'Decile'")
  gp.SendCommand("set ylabel 'log good odds'")
  gp.SendCommand("set nokey")
  seq {
    yield Series.Points(logGoodOdds, weight=4)
    yield Series.Lines(logGoodOdds)
  }
  |> gp.Plot
  gp.Set(output = Output(X11, font="arial"))

let bs = buckets 10.
scoreGroups
|> Seq.groupBy (fun g -> getBucket bs g.Score)
|> Seq.map (fun (b, gs) -> (b, gs |> Seq.sumBy (fun g -> g.Defaults)))
|> Seq.toList
//////// Old version

let totalCustomers =
    scorecard.Rows
    |> Seq.sumBy (fun r -> r.``Number of customers at this score``)
    |> float

let totalDefaults =
    scorecard.Rows
    |> Seq.sumBy (fun r -> r.``Number of defaults recorded at this score``)
    |> float

let sensitivity s =
    let numNonDefault = totalCustomers - totalDefaults
    let numNonDefaultAtScore =
        scorecard.Rows
        |> Seq.filter (fun r -> r.Score > s)
        |> Seq.sumBy (fun r -> r.``Number of customers at this score`` - r.``Number of defaults recorded at this score``)
        |> float
    numNonDefaultAtScore/numNonDefault

let specificity s =
    let numRejectScore =
        scorecard.Rows
        |> Seq.filter (fun r -> r.Score <= s)
        |> Seq.sumBy (fun r -> r.``Number of defaults recorded at this score``)
        |> float
    numRejectScore/totalDefaults

let maxScore = 
    scorecard.Rows
    |> Seq.map (fun r -> r.Score)
    |> Seq.max

let minScore = 
    scorecard.Rows
    |> Seq.map (fun r -> r.Score)
    |> Seq.min

let roc =
    let pointBeforeMin = (1. - (specificity (minScore-1)), (sensitivity (minScore-1)))
    let pointAfterMax = (1. - (specificity (maxScore+1)), (sensitivity (maxScore+1)))
    scorecard.Rows
    |> Seq.map (fun r -> (1. - (specificity r.Score), sensitivity r.Score))
    |> Seq.append [pointBeforeMin; pointAfterMax]
    |> Seq.sortBy fst

let rocStep =
  seq { (minScore-1)..(maxScore+1) }
  |> Seq.map (fun score -> (1. - (specificity score), sensitivity score))
  |> Seq.sortBy fst

roc
|> Seq.iter (fun (spec, sens) -> printfn "%f; %f" spec sens)

// question 2.c
let lossAtDefault = 200
let profitAtNonDefault = 10
let scores = scorecard.Rows |> Seq.map (fun row -> row.Score) |> Seq.sort
let profitByCutoff =
    scores
    |> Seq.map (fun s ->
        let acceptedAtScore =
            scorecard.Rows
            |> Seq.filter (fun row -> row.Score >= s)
        let loss =
            let numberOfDefaults = 
                acceptedAtScore 
                |> Seq.sumBy (fun row -> row.``Number of defaults recorded at this score``)
            numberOfDefaults*lossAtDefault
        let profit =
            let numberOfNonDefaults = 
                acceptedAtScore 
                |> Seq.sumBy (fun row -> row.``Number of customers at this score`` - row.``Number of defaults recorded at this score``)
            numberOfNonDefaults*profitAtNonDefault
        (s |> float, profit-loss |> float))

let acceptRate =
    scores
    |> Seq.map (fun s ->
        let acceptedAtScore =
            scorecard.Rows
            |> Seq.filter (fun row -> row.Score >= s)
            |> Seq.sumBy (fun row -> row.``Number of customers at this score``)
            |> float
        (s |> float, acceptedAtScore/totalCustomers))

let defaultRate =
    scores
    |> Seq.map (fun s ->
        let defaultsAtScore =
            scorecard.Rows
            |> Seq.filter (fun row -> row.Score >= s)
            |> Seq.sumBy (fun row -> row.``Number of defaults recorded at this score``)
            |> float
        (s |> float, defaultsAtScore/totalCustomers))

let badOddsAtScore s = 2.**((250.-s)/20.)
let probabilityOfDefaultFromBadOdds b = b/(1.+b)
let predictedDefaultProbabilities =
  scores
  |> Seq.map float
  |> Seq.map (fun s -> (s, s |> badOddsAtScore |> probabilityOfDefaultFromBadOdds))

let bestScore, bestProfit = profitByCutoff |> Seq.maxBy snd
let crossAt x y = 
  [
    Series.XY [0., y; 1., y]
    Series.XY [x, 0.; x, 1.]
  ]

let plotRoc() = 
    let bestSensitivity = sensitivity (int bestScore)
    let bestSpecificity = specificity (int bestScore)
    (Series.XY (roc, title = "ROC", weight = 2))
      ::(crossAt (1.-(specificity 365)) (sensitivity 365))
    |> gp.Plot
    //gp.Plot(Series.XY roc)

let plotRocStep() =
  rocStep |> Series.XY |> gp.Plot

let plotProfitByCutoff() = 
    let minScore = scores |> Seq.min |> float
    let maxScore = scores |> Seq.max |> float
    [
        Series.XY (profitByCutoff, title = "Profit by cutoff", weight = 2)
        Series.XY ([minScore, bestProfit; maxScore, bestProfit], title = "Maximum profit")
    ]
    |> gp.Plot
    //gp.Plot(Series.XY profitByCutoff)

let plotAcceptAndDefaultRates() =
    [
        Series.XY (acceptRate, title = "Accept rate")
        Series.XY (defaultRate, title = "Default rate")
    ]
    |> gp.Plot

let rocStepwise =
  roc
  |> Seq.pairwise
  |> Seq.collect (fun ((x1, y1), (x2, y2)) -> [ (x1, y1); (x2, y1); (x2, y2) ])

let defaultDistribution =
  scorecard.Rows
  |> Seq.map (fun row -> (float row.Score,
                          (float row.``Number of defaults recorded at this score``)/totalDefaults))

let nonDefaultDistribution =
  scorecard.Rows
  |> Seq.map (fun row ->
              (float row.Score,
               (float row.``Number of customers at this score``)/(totalCustomers-totalDefaults)))

let inline plotDist sq =
  let titles = Titles(sq |> Seq.map (fst >> string) |> Seq.toList)
  gp.Set(titles = titles)
  sq
  |> Seq.map snd
  |> Series.Histogram
  |> gp.Plot

let plotDefaultProb () =
  let basis =
    [270..20..650]
    |> Seq.map (fun s ->
                let s' = float s
                let scale = 10./(s'-250.)
                (s', 0.5*scale))
    |> Seq.append [(250., 0.5)]
  let defaultAtScore =
    scorecard.Rows
    |> Seq.map (fun row ->
                (float row.Score, (float row.``Number of defaults recorded at this score``)/(float row.``Number of customers at this score``)))
  [
    Series.XY basis
    Series.XY defaultAtScore
    Series.Function "0.5*exp(-log((x-250)/10))"
  ]
  |> gp.Plot

let plotDefaultProbabilities() =
  predictedDefaultProbabilities
  |> Series.XY
  |> gp.Plot

type Bucket =
  | Lower of float
  | Middle of float * float
  | Upper of float

let buckets n =
  let length = maxScore - minScore |> float
  let bucketSize = length/n
  let lowerBucket = (float minScore) + bucketSize |> Lower |> Seq.singleton
  let upperBucket = (float maxScore) - bucketSize |> Upper |> Seq.singleton
  let middleBuckets =
    [((float minScore) + bucketSize)..bucketSize..((float maxScore)-2.*bucketSize)]
    |> Seq.map (fun a -> Middle (a, a+bucketSize))
  upperBucket |> Seq.append middleBuckets |> Seq.append lowerBucket

let getBucket bs s =
  bs
  |> Seq.find (function
               | Lower b -> s < b
               | Middle (a, b) -> a <= s && s < b
               | Upper a -> s >= a)

let groupedActualDefaults =
  let bs = buckets 10.
  scorecard.Rows
  |> Seq.groupBy (fun row -> row.Score |> float |> getBucket bs)
  |> Seq.map (fun (b, rows) ->
              let defaults =
                rows
                |> Seq.map (fun row ->
                              row.``Number of defaults recorded at this score`` |> float)
              let customers =
                rows
                |> Seq.map (fun row ->
                              row.``Number of customers at this score`` |> float)
              let prob =
                (defaults, customers)
                ||> Seq.zip
                |> Seq.sumBy (fun (d,c) -> d/c)
                
              (b, prob))//defaults/customers))
  |> Seq.mapi (fun i (b, d) -> (i+1 |> float, d))

let groupedPredictedDefaults =
  let bs = buckets 10.
  predictedDefaultProbabilities
  |> Seq.groupBy (fst>>getBucket bs)
  |> Seq.map (fun (b, ps) -> (b, ps |> Seq.sumBy snd))
  |> Seq.mapi (fun i (b,d) -> (i+1 |> float, d))

let plotBucketDefaults () =
  let titles =
    buckets 10.
    |> Seq.map (function
                | Lower b -> sprintf "< %.1f" b
                | Middle (a, b) -> sprintf "[%.1f, %.1f)" a b
                | Upper a -> sprintf ">= %.1f" a)
    |> Seq.toList
  gp.Set(titles = Titles(x = titles, xrotate = -90))
  [
    Series.XY(groupedActualDefaults, title="Actual defaults")
    Series.XY(groupedPredictedDefaults, title="Predicted defaults")
  ]
  |> gp.Plot
