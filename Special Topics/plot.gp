## Script for producing PnL histograms
# Set auto scaling
set autoscale

# Remove legend
unset key

# Set output type
# PNG
set terminal postscript eps size 3.5,2.62 enhanced color solid \
    font 'Helvetica,10' linewidth 1

# Set output
imagefile = "emm_sim.eps"
set output imagefile

# Set input
datafile = "emm_sim.csv"

# Basic statistics
#stats datafile

set xlabel "Time (years)"
set ylabel "Price (GBP)"
plot for [col=1:100] datafile using ($0/1000):col with lines

# Plot the histogram
#plot [] [] datafile using (bin($1,binwidth)):(1/(binwidth*S_records)) smoot freq with boxes, \
     Gauss(x,S_mean,S_stddev) with lines title "Normal distribution"