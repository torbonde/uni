\documentclass[a4paper,twoside]{article}
\input{../preamble}
\usepackage{listings}
\usepackage{upquote}
\usepackage{natbib}
\usepackage[final,notref,notcite]{showkeys}
\usepackage[tableposition=top]{caption}
\bibliographystyle{chicago}
 
\lstdefinelanguage{FSharp}%
  {morekeywords={let, new, match, with, rec, open, module, namespace, type, of,
    member, and, for, while, true, false, in, do, begin, end, fun, function,
    return, yield, try, qmutable, if, then, else, cloud, async, static, use,
    abstract, interface, inherit, finally },
  otherkeywords={ let!, return!, do!, yield!, use!, var, from, select, where,
    order, by },
  keywordstyle=\color{blue},
  sensitive=true,
  basicstyle=\ttfamily,
	breaklines=true,
  xleftmargin=\parindent,
  aboveskip=\bigskipamount,
	tabsize=4,
  morecomment=[l][\color{gray}]{///},
  morecomment=[l][\color{gray}]{//},
  morecomment=[s][\color{gray}]{{(*}{*)}},
  morestring=[b]",
  showstringspaces=false,
  literate={`}{\`}1,
  stringstyle=\color{redstrings},
}
\lstset{
  language=FSharp,
  basicstyle=\ttfamily,
  breaklines=true,
  columns=fullflexible
}
\begin{document}
\title{Equivalent Martingale Measures}
\author{David Tor Bonde}
\date{December 1, 2014}
\maketitle
\tableofcontents

\section{Risk-neutral pricing}
In the following, we will give a short synopsis of the risk-neutral pricing
theory, we will be using in section \ref{sec:an-application}. The model that
will be the main focus in this model does emit a unique \emph{equivalent
  martingale measure}. We will in this section, however, present a model for
which this is true, namely the famed Black-Scholes model.

Let in the following $(\Omega,\calF,\flowF=(\calF_t)_{t\in[0,T]},\probP)$ be a
filtered probability space, where $T>0$. The generalized Black-Scholes model
$\calM^{BS}$ contains $d+1$ traded assets, $S\pex{0},\ldots,S\pex{d}$, where
$S\pex{0}$ given by
\begin{equation}
  \label{eq:22}
  \diff S_t\pex{0} = S_t\pex{0}r_t\,\diff t\quad\text{and}\quad S_0\pex{0} = 1
\end{equation}
is the risk-free asset and $r$ is the effective rate of interest. Furthermore,
for $i=1,\ldots,d$,
\begin{equation}
  \label{eq:23}
  \frac{\diff S_t\pex{i}}{S_t\pex{i}} = b_t\pex{i} +
  \sum_{j=1}^n\sigma^{ij}_t\,\diff W_t^j\quad\text{and}\quad S_0\pex{i} =
  p^i\in(0,\infty)
\end{equation}
are risky assets, where $W = (W_t^1,\ldots,W_t^n)\transp_{t\in[0,T]}$ is an
$n$-dimensional Brownian motion, $b=(b_t^1,\ldots,b_t^n)\transp_{t\in[0,T]}$ is
the vector of appreciation rates, and
$\sigma=(\sigma_t^{ij})_{t\in[0,T]}$ is the $d\times n$ matrix-valued volatility
process.

In the classical Black-Scholes model $n=d=1$ and $r$, $b$ and $\sigma$ are
constant. In our case, we will assume that these are progressively measurable
with respect to $\flowF$ \citep{bingham2004risk}, and that they satisfy
\begin{equation}
  \label{eq:33}
  \int_0^T|r_t| + \norm{b_t} + \norm{\sigma_t}^2\,\diff t < \infty,\text{ a.s.}
\end{equation}

Our goal is to be able to get a ``fair price'' for each contingent claim. This
is done using the Risk-neutral Valuation Formula.
\begin{thm}[Risk-neutral Valuation Formula]\label{thm:risk-neutral}
  The ``fair price'' of any contingent claim $h$ in an arbitrage-free and complete
  market $\calM$ is given by the Risk-neutral Valuation Formula
  \begin{equation}
    \label{eq:35}
    \Pi_h(t) = S_t\pex{0}\expect^{\probP^*}\Biggl(\frac{h}{S_T\pex{0}}\Biggm|\calF_t\Biggr),
  \end{equation}
  where $\probP^*$ is the unique equivalent martingale measure.
\end{thm}

In order to even partially grasp this, we need some introduction. Assume, as in
the model described above, that we are considering a model $\calM$ with $d+1$
assets, the first of which is the risk-less asset, and the remaining $d$ are
risky assets. Let $\phi = (\phi_t\pex{0},\ldots,\phi_t\pex{d})_{t\in[0,T]}$ be a
$d+1$-dimensional, real valued, previsible stochastic process. If $\phi$
satisfies the integrability condition
\begin{equation}
  \label{eq:36}
  \int_0^T\expect[\phi_t\pex{0}]\,\diff t<\infty\quad\text{and}\quad
  \sum_{i=0}^d\int_0^T\expect[(\phi_t\pex{i})^2]\,\diff t < \infty,
\end{equation}
we call $\phi$ a trading strategy. Under a trading strategy, we can define a
wealth process, $V\pex{\phi}$ given by $V_t\pex{\phi} =
\langle\phi_t,S_t\rangle$, where $\langle\cdot,\cdot\rangle$ denotes the typical
inner product. We call $\phi$ a self-financing strategy if the value process at
time $t$ is determined solely by $V_0\pex{\phi}$ and the gains/losses in the
portfolio up until time $t$. I.e. if it doesn't depend on cash withdrawals or
injections.

Now, self-financing strategy $\phi$ is said to be an arbitrage opportunity if
\begin{equation}
  \label{eq:37}
  V_0\pex{\phi} = 0,\quad\probP(V_T\pex{\phi}\geq 0) = 1\quad\text{and}\quad \probP(V_T\pex{\phi}>0)>0.
\end{equation}
A market $\calM$ is then said to be arbitrage-free if there are no such trading
strategies.

Suppose now that $h$ is a contingent claim, and that we can find a trading
strategy $\phi$ such that $V_t\pex{\phi}\geq 0$ for all $t\in[0,T]$ and
\begin{equation}
  \label{eq:38}
  V_T\pex{\phi} = h.
\end{equation}
Then we call $h$ attainable, and $\phi$ the replicating strategy for $h$. A
market where any contingent claim is attainable is called complete.

In order to determine a fair price, we need to have a probability measure
$\probQ$, under which the discounted asset price process $\tilde{S}$ is a local
martingale and such that $\probQ$ is equivalent to $\probP$. Such a measure is
called an equivalent martingale measure.

Now we are ready to state the two fundamental theorems of asset pricing:
\begin{thm}[First Fundamental Theorem of Asset Pricing]
  The market $\calM$ is arbitrage free precisely when there exist an equivalent
  martingale measure $\probP^*$.
\end{thm}

\begin{thm}[Second Fundamental Theorem of Asset Pricing]
  Assume that $\calM$ is arbitrage-free. Then $\calM$ is complete precisely when
  the equivalent martingale measure $\probP^*$ is uniquely determined.
\end{thm}

Now, in order to determine whether a fair price exists in the Black-Scholes
model $\calM^{BS}$, and to be able to find it using the Risk-neutral Valuation
Formula, Theorem \ref{thm:risk-neutral}, we will need to see that there is an
equivalent martingale measure $\probP^*$, and furthermore, that this is in fact
uniquely determined. The details are outside the scope of this paper. However,
the existence-part relies heavily on the following much appraised theorem, which
is used to construct a measure $\probQ$, under which $\tilde{W}$ defined by
\begin{equation}
  \label{eq:39}
  \diff \tilde{W}_t = \diff W_t + \gamma_t\,\diff t
\end{equation}
for some process $\gamma$ is a Brownian motion. One can then show that this is a
probability measure, and that the discounted asset price process $\tilde{S}$ is
a $\probQ$-local martingale, hence an equivalent martingale measure.

\begin{thm}[Girsanov theorem]
  Let $(\Omega,\calF,\flowF = (\calF_t)_{t\in[0,T]},\probP)$ be a filtered
  probability space where $T>0$, and let $W=(W_t^i,\ldots,W_t^i)\transp_{t\in[0,T]}$ be a $d$-dimensional Brownian motion with respect to $\flowF$. Let
  furthermore $b=(b_t^1,\ldots,b_t^d)\transp_{t\in[0,T]}$ be a
  $d$-dimensional process, adapted to $\flowF$ with $\int_0^T(b_t^i)^2\,\diff
  t<\infty$ for each $i=1,\ldots,d$. Define for each $t\in[0,T]$ and
  $i=1,\ldots,d$ by
  \begin{equation}
    \label{eq:34}
    X_t^i = W_t^i + \int_0^tb_s^i\,\diff s
  \end{equation}
  a $d$-dimensional process $X = (X_t^1,\ldots,X_t^d)\transp_{t\in[0,T]}$. Now let
  \begin{equation}
    \label{eq:10}
    \gamma_T = \exp\left(-\sum_{i=1}^d\int_0^Tb_t^i\,\diff W_t^i -
      \frac{1}{2}\sum_{i=1}^d\int_0^T|b_t^i|^2\,\diff t\right)
  \end{equation}
  and define the measure $\probQ$ through its Radon-Nikodym derivative by
  \begin{equation}
    \label{eq:18}
    \od{\probQ}{\probP} = \gamma_T.
  \end{equation}
  Then under $\probQ$, the process $X$ is a Brownian motion with respect to
  $\flowF$.
\end{thm}


\section{The model}
One major drawback of the classical Black-Scholes model is that it assumes the
volatility of the asset price process $S$ to be constant. Empirically, this
assumption will not generally hold. Several alternatives have been studied, and
in the following, we will study the setting where the volatility is governed by
a stochastic process. The particular model we will consider was studied
thoroughly in \citet{hobson2004stochastic}, and is given as follows
\begin{align}
  \diff S_t &= S_t Y_t (\lambda_t\,\diff t + \diff B_t),\\
  \diff Y_t &= \alpha_t\,\diff t + \beta_t\,\diff W_t,\\
  \diff W_t &= \rho_t\,\diff B_t + \bar{\rho}_t\,\diff Z_t.
\end{align}
Here $S$ is the asset price process and $Y$ is the stochastic volatility. These
are assumed to be correlated through the Brownian motions $B$ and $W$ with
correlation coefficient $\rho_t$, and then $Z$ is a Brownian motion independent
of $B$ and $\bar{\rho}_t = \sqrt{1-\rho_t^2}$. The processes $\lambda_t$,
$\alpha_t$ and $\beta_t$ are assumed to be previsible. As in
\citet{hobson2004stochastic} we work under the assumption of zero interest
rates, so that $S$ is in fact the discounted price process.

\section{Choosing measure}

Unlike the Black-Scholes model, the model we will consider is incomplete. So by
the Second Fundamental Theorem of Asset Pricing, there is no unique equivalent
martingale measure. So the question is, how do we choose the optimal one? In the
following, let $M^e(\probP)$ denote the set of equivalent martingale measures,
i.e.
\begin{equation}
  \label{eq:27}
  M^e(\probP) = \{\probQ\sim\probP : \probQ\text{ is a probability measure and
    $S$ is a $\probQ$-local martingale}\}
\end{equation}
In the case of the variance-optimal measure, \citet{delbaen1996variance} suggest
choosing the measure $\probQ\pex{2}\in M^e(\probP)$ minimizing the $L^2(\probP)$-norm
\begin{equation}
  \label{eq:26}
  \norm{\od{\probQ}{\probP}}_{L^2(\probP)} = \left(\int_\Omega \left(\od{\probQ}{\probP}\right)^2\,\diff\probP\right)^{\frac{1}{2}},
\end{equation}
which in some sense denotes a distance to the real-world probability measure $\probP$.

It makes sense to aim for this measure since, assuming such it actually exists,
it is uniquely defined. To see this, we will need the following lemma, which we
will show in the general setting of the $\LqP$-norm, for $q>1$. Recall
that a norm $\norm{\cdot}$ is called \emph{strictly convex} if $\norm{x} =
\norm{y} = \frac{1}{2}\norm{x+y}$ implies $x=y$.

\begin{lem}\label{lem:L2norm-convex}
  The $\LqP$-norm $\norm{\cdot}_{\LqP}$ is strictly convex, for $q>1$.
\end{lem}
\begin{proof}
  Let $f,g\in \LqP$ with $\norm{f}_\LqP = \norm{g}_\LqP =
  \frac{1}{2}\norm{f+g}_\LqP$. By Minkowski's inequality we have
  \begin{equation}
    \label{eq:3}
    \norm{f+g}_\LqP\leq\norm{f}_\LqP+\norm{g}_\LqP
  \end{equation}
  with equality precisely when $f = \alpha g$ for some $\alpha\geq 0$. But by
  assumption, equality must hold.

  If $f = \alpha g$ for $\alpha\geq 0$, then
  \begin{equation}
    \label{eq:4}
    \norm{g}_\LqP = \norm{f}_\LqP = \norm{\alpha g}_\LqP =
    \vert\alpha\vert\norm{g}_\LqP = \alpha \norm{g}_\LqP
  \end{equation}
  which means that $\alpha = 1$, and thus $f = g$, giving the strict convexity.
\end{proof}

Now the proof of our claim follows quite easily. For let us assume that there
exist $\probQ_1,\probQ_2\in M^e(\probP)$ with $\probQ_1\neq\probQ_2$ which
both minimizes $\norm{\od{\probQ}{\probP}}_\LqP$. Then
  \begin{equation}
    \label{eq:5}
    \norm{\od{\probQ_1}{\probP}}_\LqP = \norm{\od{\probQ_2}{\probP}}_\LqP
  \end{equation}
  and by Minkowski's inequality
  \begin{equation}
    \label{eq:6}
    \norm{\od{\probQ_1}{\probP}+\od{\probQ_2}{\probP}}_\LqP \leq
    \norm{\od{\probQ_1}{\probP}}_\LqP + \norm{\od{\probQ_2}{\probP}}_\LqP.
  \end{equation}
  If we have equality in \eqref{eq:6}, then by strict convexity of the
  $\LqP$-norm $\probQ_1=\probQ_2$ a.s., which contradicts our
  assumption. Hence, we must have strict inequality in \eqref{eq:6}.

  However, letting $\probQ = \frac{1}{2}\probQ_1 + \frac{1}{2}\probQ_2$, if
  follows trivially that $\probQ\sim\probP$, just as it does that $\probQ$ is a
  probability measure. Now let $\bigl\{\tau_k\pex{1}\bigr\}_{k\geq 1}$ and
  $\bigl\{\tau_k\pex{2}\bigr\}_{k\geq 1}$ be the localizing sequences of $S$ under
  $\probQ_1$ and $\probQ_2$, respectively, and for each $k\geq 1$ let $\tau_k =
  \tau_k\pex{1}\vee\tau_k\pex{2}$. Then $\{\tau_k\}_{k\geq 1}$ is an increasing
  sequence of stopping times and the stopped process defined by $X_t\pex{n} =
  S_{t\wedge\tau_n}$ is easily seen to be a $\probQ$-martingale. In total,
  $\probQ\in M^e(\probP)$ which gives us a contradiction on the
  minimality-assumption.

  Alas, we have seen that, afterall, we must have $\probQ_1=\probQ_2$ a.s., and
  uniqueness follows. Particularly, it follows by setting $q=2$ that the measure
  $\probQ\pex{2}$ minimizing \eqref{eq:26}, provided that it exists, is unique.

\subsection{The $q$-optimal measure}
Following \citet[Section 2]{hobson2004stochastic} let $V$ be a local martingale with $V_0$,
so that $V_tS_t$ is a positive local martingale whenever
\begin{equation}
  \label{eq:1}
  V_t = \dolExp(-\lambda\cdot B - \psi\cdot Z)_t,
\end{equation}
and define a local martingale $\probQ$ through
\begin{equation}
  \label{eq:2}
  \od{\probQ}{\probP}\Biggm|_{\calF_t} = V_T = \dolExp(-\lambda\cdot B -
  \psi\cdot Z)_T.
\end{equation}

Now note that if $\int_0^T\lambda_t^2\,\diff t$ and $\int_0^T\psi_t^2\,\diff t$
are finite, then $V_T$ is strictly positive. Furhtermore, if $V$ is a true
martingale, then $\probQ$ is equivalent to $\probP$. This is still not enough to
say that $\probQ\in M^e(\probP)$. Note though that under such $\probQ$, by the
Girsanov Theorem, the processes $B^{\probQ}$ and $Z^{\probQ}$ given by
\begin{equation}
  \label{eq:30}
  \diff B_t^\probQ = \diff B_t + \lambda_t\,\diff t\quad\text{and}\quad\diff
  Z_t^\probQ = \diff Z_t + \psi_t\,\diff t
\end{equation}
are Brownian motions, and the under $\probQ$, the stochastic differentials of
$S$ and $Y$ becomes
\begin{equation}\label{eq:31}
  \begin{split}
    \diff S_t &= S_tY_t\,\diff B_t^{\probQ},\\
    \diff Y_t &= (\alpha_t - \beta_t\rho_t\lambda_t -
    \beta_t\bar{\rho}_t\psi_t)\,\diff t + \beta_t\bigl(\rho_t\,\diff B_t^{\probQ}
    + \bar{\rho}_t\,\diff Z_t^{\probQ}\bigr)
  \end{split}
\end{equation}

Under some further assumptions \citet{hobson2004stochastic} shows that there is
a measure of this form \eqref{eq:2}, which minimizes the $\LqP$-norm of $V_T$
and is indeed in $M^e(\probP)$. In order to do so, he defines for $q>1$ and
$\probQ$ in the shape of \eqref{eq:2}
\begin{equation}
  \label{eq:7}
  H_q(\probP,\probQ) =
  \begin{cases}
    \expect[(V_T)^q] & \probQ\ll\probP\\
    \infty & \text{otherwise}
  \end{cases},
\end{equation}
which in the spirit of \citet{delbaen1996variance} is sought optimized. The
measure $\probQ\pex{q}$ which optimizes $H_q(\probP,\probQ)$ is called the
$q$-optimal measure, given that it exists. Under the aforementioned assumptions,
this quantity can be calculated explicitly, which we will do here.

Let $K_t = \int_0^t\lambda_u^2\,\diff u$ and write for a fixed $q>1$
\begin{equation}
  \label{eq:12}
  \frac{q}{2}K_T = M_T - \frac{q-1}{2} [M]_T + L_T + \frac{1}{2}[L]_T + c
\end{equation}
where $c$ is a constant depending on $q$ and
\begin{equation}
  \label{eq:13}
  M_t = \int_0^t \eta_u(\diff B_u + q\lambda_u\,\diff
  u)\quad\text{and}\quad\int_0^t\xi_i\,\diff Z_u
\end{equation}
for some previsible processes $\eta$ and $\xi$, depending on $q$. Note that
\begin{equation}
  \label{eq:28}
  [M]_t = \int_0^t\eta_u^2\,\diff u\quad\text{and}\quad [L]_t =
  \int_0^t\xi_u^2\,\diff u.
\end{equation}
Equation \eqref{eq:12} is called the fundamental representation equation. We are
now ready to state the assumptions of \citet[Theorem 3.1]{hobson2004stochastic},
under which we will be working:

\begin{assm}
  Let $q>1$. Suppose we have a solution for \eqref{eq:12} such that $K_T$,
  $[M]_T$, $[L]_T$ and $c$ are finite and define local
  martingales $V\pex{q}$ and $\tilde{V}\pex{q}$ through
  \begin{equation}
    \label{eq:29}
    V_t\pex{q} = \dolExp(-\lambda\cdot B - \xi\cdot
    Z)_t\quad\text{and}\quad\tilde{V}_t\pex{q} =
    \dolExp(((q-1)\eta-q\lambda)\cdot B-\xi \cdot Z)_t.
  \end{equation}
  Finally, we will assume that $V\pex{q}$ and $\tilde{V}\pex{q}$ are in fact
  martingales.
\end{assm}

Under these assumptions, \citet{hobson2004stochastic} shows that the measure
$\probQ\pex{q}$ given by
\begin{equation}
  \label{eq:8}
  \od{\probQ\pex{q}}{\probP} = V_T\pex{q} = \dolExp(-\lambda\cdot B - \xi\cdot Z)_T
\end{equation}
is the $q$-optimal measure. The ``distance'' from $\probQ\pex{q}$ to $\probP$ is
given by
\begin{equation}
  \label{eq:9}
  H_q(\probQ\pex{q},\probP) = \expect\left[\left(V_T\pex{q}\right)^q\right] = \expect^{\probQ\pex{q}}\left[\left(V_T\pex{q}\right)^{q-1}\right]
\end{equation}

Now by the definition of $K_t$, the Dol\'eans exponential and \eqref{eq:30}
\begin{equation}\label{eq:32}
  \ln V_T\pex{q} = -\int_0^T \lambda_t\,\diff B_t^{\probQ\pex{q}} + \frac{1}{2}K_T -
  \int_0^T\xi_t\,\diff Z_t - \frac{1}{2}\int_0^T\xi_t^2\,\diff t.
\end{equation}
Writing $\frac{1}{2}K_T = \frac{q}{2}K_T - \frac{q-1}{2}K_T$, and restating
\eqref{eq:12} in terms of $B_t^{\probQ\pex{q}}$ and $Z_t^{\probQ\pex{q}}$, we
get that
\begin{equation}
  (q-1)\ln V_T\pex{q} = (q-1)\int_0^T\eta_t-\lambda_t\,\diff B_t^{\probQ\pex{q}} -
  \frac{(q-1)^2}{2}\int_0^T (\eta_t-\lambda_t)^2\,\diff t + c,
\end{equation}
and so $H_q(\probQ\pex{q},\probP) =
e^{(q-1)c}\expect\biggl[\dolExp((q-1)(\eta-\lambda)\cdot B^{\probQ\pex{q}})_T
V_T\pex{q}\biggr]$ by \eqref{eq:9}. Now first rewriting $\ln V_T\pex{q}$ by
\eqref{eq:30} in terms of $\diff B_t^{\probQ\pex{q}}$ and next going the other
way (to writing $\diff B_t$), one gets
\begin{align}
  \MoveEqLeft[3] \ln(\dolExp((q-1)(\eta-\lambda)\cdot
  B^{\probQ\pex{q}})_TV_T\pex{q})\\
  = {} & \int_0^T(q-1)(\eta_t-\lambda_t)\,\diff B_t^{\probQ\pex{q}} -
  \frac{(q-1)^2}{2}
  \int_0^T(\eta_t-\lambda_t)^2\,\diff t + \ln V_T\pex{q}\\
  = {} & \int_0^T(q-1)\eta_t - q\lambda_t\, \diff B_t - \int_0^T\xi_t\, \diff
  Z_t - \frac{1}{2}\int_0^T\xi_r^2\, \diff t\\
  &-  \frac{1}{2}\int_0^T (q-1)^2 (\eta_t-\lambda_t)^2 -\lambda_t^2 - 2((q-1)\eta_t-q\lambda_t)\lambda_t\,\diff t,
\end{align}
Finally, looking further at the integrand in the last term,
\begin{equation}
  (q-1)^2 (\eta_t-\lambda_t)^2 -\lambda_t^2 -
  2((q-1)\eta_t-q\lambda_t)\lambda_t = ((q-1)\eta_t-q\lambda_t)^2,
\end{equation}
we can see that
\begin{equation}
  \label{eq:11}
  H_q(\probQ\pex{q},\probP) =
  e^{(q-1)c}\expect[\dolExp(((q-1)\eta-q\lambda)\cdot B - \xi\cdot Z)_T] = e^{(q-1)c}
\end{equation}
by noting that the expression inside the expectation is exactly
$\tilde{V}_T\pex{q}$, and using that this is a true martingale, hence
$\expect[\tilde{V}_T\pex{q}]=1$.

\section{An application}\label{sec:an-application}
In this section, we will give an application of the results obtained in \citet{hobson2004stochastic}, to
pricing European options under the assumption of $\lambda$ being a deterministic
function of time, $\lambda_t = \lambda(t)$.

First, let's recall the fundamental representation equation \eqref{eq:12}:
\begin{equation}
  \label{eq:14}
  \frac{q}{2}K_T = M_T -\frac{q-1}{2}[M]_T + L_T + \frac{1}{2}[L]_T + c,
\end{equation}
Since $\lambda$ is a deterministic function of time, the left hand side of
\eqref{eq:14} can not be stochastic, and so, neither can the right hand
side. But we can not choose $\eta$ and $\xi$ such that the stochastic integrals
$M_T$ and $L_T$ cancels out, since $B$ and $Z$ are independent. Thus, we must
have $\eta \equiv 0 \equiv \xi$, and $c = \frac{q}{2}\int_0^T\lambda(t)^2\,\diff
t$. Conclusively, since $\lambda$ is deterministic, $K_T$, $c$, $[M]_T$ and
$[L]_T$ are all finite, and thus satisfies the conditions of Theorem 3.1 in
\citet{hobson2004stochastic}.

Now $V_t\pex{q}$ and, for $q>1$, $\tilde{V}_t\pex{q}$ are given by
\begin{equation}
  \label{eq:19}
  V_t\pex{q} = \dolExp(-\lambda\cdot B)_t\quad\text{and}\quad\tilde{V}_t\pex{q}
  = \dolExp(q\lambda\cdot B)_t.
\end{equation}
These are supermartingales with $V_0\pex{q} = \tilde{V}_0\pex{q} = 1$, so to see
that these are in fact martingales, it suffices to show that they have
expectation $1$ at time $T$ \citep[p.~541]{hobson2004stochastic}. So observe that
\begin{equation}
  \label{eq:20}
  \expect[V_T\pex{q}] = \expect\bigl[e^{-\int_0^T\lambda(t)\,\diff B_t -
    \frac{1}{2}\int_0^T\lambda(t)^2\,\diff t}\bigr] =
  e^{-\frac{1}{2}\int_0^T\lambda(t)^2\,\diff
    t}\expect\bigl[e^{-\int_0^T\lambda(t)\,\diff B_t}\bigr],
\end{equation}
and $e^{-\int_0^T\lambda(t)\,\diff B_t}\sim\ln
N\bigl(0,\int_0^T\lambda(t)^2\,\diff t\bigr)$. It follows that
$\expect[V_T\pex{q}] = 1$, since
$\expect\bigl[e^{-\int_0^T\lambda(t)\,\diff t}\bigr] =
e^{\frac{1}{2}\int_0^T\lambda(t)^2\,\diff t}$ . That $\tilde{V}_t\pex{q}$ is a
martingale follows by an analogue argument.

Now the $q$-optimal measure $\probQ\pex{q}$ is given by
\begin{equation}
  \label{eq:21}
  \od{\probQ\pex{q}}{\probP}\Biggm|_{\calF_t} = V_T\pex{q} =
  \dolExp(-\lambda\cdot B).
\end{equation}
Note that, in the setting of $\lambda$ being deterministic, the $q$-optimal
measure is independent of the choice of $q$.

\subsection{Simulation basics}
In this section, we wish to describe the methods we use for pricing European
option.

First, recall that we work under the assumption of zero interest
rates. Therefore, for an attainable contingent claim $h$, the arbitrage price
process is given by
\begin{equation}
  \label{eq:15}
  \Pi_h(t) = \expect^{\probQ\pex{q}}(h|\calF_t).
\end{equation}
Our best bet for the price of $h$ is now given by
\begin{equation}
  \label{eq:16}
  \expect^{\probQ\pex{q}}\bigl[\Pi_h(0)\bigr] =
  \expect^{\probQ\pex{q}}\bigl[\expect^{\probQ\pex{q}}(h|\calF_0)\bigr] = \expect^{\probQ\pex{q}}[h],
\end{equation}
which as in the nature of Monte Carlo methods, we will imitate by averaging
over a large number of simulations of $S_T$.

For our specific application, we wish to simulate $S$ given by equations
\eqref{eq:31} under the further condition of $Y$ being a mean-reverting
process. To obtain this, we take
\begin{align}
  \alpha(t,y) &= \theta(\mu-y)\\
  \beta(t,y) &= \sigma y
\end{align}
where $\theta > 0$, $\sigma > 0$ and $\mu\in\reals$ are constants. That is, in
our case, equations \eqref{eq:31} are given by
\begin{equation}
  \label{eq:17}
  \begin{split}
    \diff S_t &= S_tY_t\,\diff B_t^{\probQ\pex{q}},\\
    \diff Y_t &= (\theta(\mu-Y_t) - \sigma Y_t\rho_t\lambda(t))\,\diff t +
    \sigma Y_t\bigl(\rho_t\,\diff B_t^{\probQ\pex{q}} + \bar{\rho}_t\,\diff
    Z_t^{\probQ\pex{q}}\bigr)
  \end{split}
\end{equation}

\begin{figure}[t]
  \centering
  \includegraphics{emm_sim.eps}
  \caption{100 simulated trajectories $S$ over a time-period of 3 months.}
  \label{fig:sims}
\end{figure}

Since $S$ is given by equations \eqref{eq:17}, for which we don't have a closed
form solution, we will need to apply a discretization scheme, to obtain a value
for $S_T$. Our method of choice is called the Euler scheme
\citep[see][]{glasserman2004monte}, which, applied to equations \eqref{eq:17}
will yield the following equations
\begin{align}
  \hat{S}_{(k+1)h} ={}& \hat{S}_{kh} +
  \hat{S}_{kh}\hat{Y}_{kh}\sqrt{h}\tilde{B}_k\label{eq:24}\\
  \hat{Y}_{(k+1)h} ={}& \hat{Y}_{kh} + (\theta(\mu-\hat{Y}_{kh}) -
  \sigma\hat{Y}_{kh}\rho_{kh}\lambda(kh))h\\
  &+ \sigma\hat{Y}_{kh}(\rho_{kh}\sqrt{h}\tilde{B}_{k+1} +
  \bar{\rho}_{kh}\sqrt{h}\tilde{Z}_{k+1}).\label{eq:25}
\end{align}
Here the $\tilde{B}_k$'s are independent, identically distributed
$N(0,1)$-variables, the $\tilde{Z}_k$'s are independent, identically distributed
$N(0,1)$-variables, and for each $k$ $\tilde{Z}_k$ is independent of
$\tilde{B}_k$.

Notice that \eqref{eq:24} contains $\tilde{B}_k$, but \eqref{eq:25} contains
$\tilde{B}_{k+1}$. This is to enforce the assumed correlation between $S$ and $Y$.

\subsection{Results}

In this section we will present the prices of European options, simulated under
the model described above, as well as the parameters used in the
simulation. Figure \ref{fig:sims} shows 100 simulated trajectories of $S$
simulated under this given model. The code used for these simulations is listed
in Appendix \ref{sec:code}. The parameters used are stated in Table \ref{tab:params}.

\begin{table}[h]
  \centering
  \caption{Model parameters}
  \begin{tabular}{ccccc}
  $\lambda(t)$   & $\rho_t$    & $\theta$ & $\mu$ & $\sigma$ \\\midrule
  $0.05|\sin t|$ & $0.9\sin t$ & $1$      & $0.2$ & $0.3$
\end{tabular}
  \label{tab:params}
\end{table}

We chose to simulate $S$ over a time period of 3 months, with 250 time steps,
$Y_0 = 0.2$ and $S_0 = \textsterling 50$. For the European options, we have
chosen the strike prices $K_{call}=\textsterling 40$ and $K_{put}=\textsterling
60$. Under these conditions, we obtained the prices stated in Table
\ref{tab:prices}, after $N$ simulations.
\begin{table}[h]
  \centering
  \caption{Simulated prices (\textsterling) of Euorpean options}
  \begin{tabular}{rcc}
      $N$ & Call price & Put price \\\midrule     
$10^1$    & 10.1096    & 10.8168   \\
$10^2$    & 9.6893     & 9.8150    \\
$10^3$    & 10.1210    & 10.1556   \\
$10^4$    & 10.1064    & 10.0877   \\
$10^5$    & 10.1109    & 10.0848   \\
$10^6$    & 10.1020    & 10.1061
  \end{tabular}
  \label{tab:prices}
\end{table}

\appendix
\section{Code}\label{sec:code}
The following code was used for doing the Monte Carlo simulations. It is written
in F\#, and depends on the NuGet packages \emph{Streams} and
\emph{MathNet.Numerics}.
\begin{lstlisting}
namespace Simulation

open Nessos.Streams
open MathNet.Numerics.Distributions

module Payout =
    /// Call payout function
    let call K S = max (S-K) 0.0

    /// Put payout function
    let put K S = max (K-S) 0.0

module Hobson =
    /// Random number generator
    let N01 =
        let N = Normal()
        let r = System.Random(1)
        N.RandomSource <- r
        N
       
    /// Next Gaussian random number
    let rnd () = N01.Sample()

    /// State carried through calculations
    type State =
        {
            t : float;
            S : float;
            Y : float;
        }
        static member getPrice state = state.S

    /// Parameters specifying model
    type Model =
        {
            lambda : float -> float;
            rho : float -> float;
            theta : float;
            mu : float;
            sigma : float;
        }

    /// Function to do a single simulation
    let private simulate m T model payout initState =
        let rho' t = 1. - (model.rho t)**2. |> sqrt
        let h = T/(float m)
        [|1..m|]
        |> Stream.ofArray
        |> Stream.fold (fun s i ->
            let B = rnd()
            let Z = rnd()
            let Y' = 
                if i = 1 
                then s.Y
                else s.Y + (model.theta * (model.mu - s.Y)
                    - model.sigma * s.Y * (model.rho s.t)) * h
                    + model.sigma * s.Y * (sqrt h)
                        * ((model.rho s.t) * B + (rho' s.t) * Z)
            let S' = s.S + (sqrt h) * s.S * Y' * B
            { t = s.t + h; S = S'; Y = Y'})
            initState
        |> State.getPrice
        |> payout

    /// Function to do N simulations
    let valuate N m T model payout initState =
        let sum =
            [|1..N|]
            |> Stream.ofArray
            |> Stream.map (fun _ -> simulate m T model payout initState)
            |> Stream.sum
        sum/(float N)
\end{lstlisting}
\bibliography{emm}
\end{document}