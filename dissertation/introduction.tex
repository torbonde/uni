\chapter{Introduction}\label{cha:background}
The purpose of this chapter is to introduce the topic of discussion in this
project. In Section~\ref{sec:model} the scene is set by describing the model and
in Section~\ref{sec:barrier-options} some background information about barrier
options is given, including some known pricing formulas.

\section{The model}\label{sec:model}
Let $(\Omega,\Prob,\calF, \flowF = (\calF_t)_{t\geq 0})$ be a filtered
probability space, where $\calF_t = \sigma(W_s : 0\leq s\leq t)$ is the
filtration generated by the Wiener process $W = (W_t)_{t\geq 0}$. The model
under consideration in this paper will be the standard one-dimensional
Black-Scholes model without dividends. That is, the market is assumed to consist
of a riskless asset $B = (B_t)_{t\geq 0}$ given by
\begin{equation}
  \label{eq:16}
  \diff B_t = r B_t\,\diff t,\quad B_0 = 1,
\end{equation}
with the solution $B = \e^{rt}$ and a single risky asset $S = (S_t)_{t\geq 0}$ given by
\begin{equation}
  \label{eq:17}
  \diff S_t = \mu S_t\,\diff t + \sigma S_t\, \diff W_t.
\end{equation}
Here the risk-free rate of return $r\geq 0$, the drift $\mu\in\reals$, the
volatility $\sigma>0$ and the spot price $S_0\geq 0$ are fixed constants. For
the sake of simplicity assume further that $\Prob$ is the risk-neutral measure,
such that $\mu = r$. Then the solution to \eqref{eq:17} is $S_t =
S_0\e^{\pbrac*{r-\half\sigma^2}t + \sigma W_t}$ and $\expect S_t = S_0 \e^{rt}$,
i.e. the stock price is expected to grow at the risk-free rate.

The price of a European type option with payoff
$h\pbrac[\big]{(S_t)_{t\in[0,T]}}$ at time $t$ is given by
\begin{equation}
  \label{eq:42}
  \e^{-r(T-t)}\expect\pbrac[\big]{h\pbrac[\big]{(S_t)_{t\in[0,T]}} \bigm| \calF_t}.
\end{equation}
Note that the payoff may depend on the entire path of $S$. This kind of option
is known as a path-dependent.

\section{Barrier options}\label{sec:barrier-options}
Barrier options are a type of path dependent option, where the payoff is
conditioned on the movement of the underlying during the life time of the
option. The two types of barrier options that will be considered in this paper
are single barrier options and double barrier options. Each of these types can
be further classified as either a knock-out or a knock-in option. This
classification determines how the payoff is conditioned on the path of the
underlying.

In the case of a single barrier knock-out option with a barrier $B>0$, the
option is nullified if the underlying reaches the barrier $B$. Likewise, for a
knock-in option, the option only comes into existence if the underlying reaches
the level $B$. In each case, there are two interesting possibilities; $B > S_0$
and $B < S_0$. Obviously, the uninteresting case when $B = S_0$ yields either a
worthless option or a European type option, depending on whether the option is a
knock-out or a knock-in type option.

\begin{table}[tbp]
  \centering
  \begin{tabular}{l l l}
    Type & Barrier & Payoff\\\midrule
    Up-and-out & $B > S_0$ & $h(S_T) \ind_{\Set{S_t < B, \text{ for
          all } t\in[0,T)}}$\\
    Up-and-in & $B > S_0$ & $h(S_T) \ind_{\Set{S_t \geq B, \text{ for
          some } t\in[0,T)}}$\\
    Down-and-out & $B < S_0$ & $h(S_T) \ind_{\Set{S_t > B, \text{ for
          all } t\in[0,T)}}$\\
    Down-and-in & $B < S_0$ & $h(S_T) \ind_{\Set{S_t \leq B, \text{ for
          some } t\in[0,T)}}$
  \end{tabular}
  \caption{Payoffs for single barrier options, where $h(x)$ is the
    payoff. E.g. $h(x) = (x-K)^+$ for call options, and $h(x) = (K-x)^+$ for put
    options.}
  \label{tab:single-barrier-payoff}
\end{table}

When $B > S_0$, the knock-out and knock-in options are for simplicity called
up-and-out and up-and-in options, respectively. For $B < S_0$ they
are called down-and-out and down-and-in options. The payoffs of
these different types of barrier options are described in Table~\ref{tab:single-barrier-payoff}.

Where single barrier options have only one barrier, double barrier options have,
a bit unsurprisingly perhaps, two barriers; one lower and one upper, $L < S_0 <
U$. As before, these can be classified into knock-out options and knock-in
options. A double barrier option is hence either knocked out or knocked in, if
the underlying reaches one of the barriers before maturity. The payoffs of
double barrier options are listed in Table~\ref{tab:double-barrier-payoff}.

\begin{table}[htbp]
  \centering
  \begin{tabular}{l l}
    Type & Payoff\\\midrule
    Knock-out & $h(S_T) \ind_{\Set{L < S_t < U, \text{ for
          all } t\in[0,T)}}$\\
    Knock-in & $h(S_T) \ind_{\Set{S_t \leq L \text{ or } S_t \geq U, \text{ for
          some } t\in[0,T)}}$
  \end{tabular}
  \caption{Payoffs for double barrier options, where $h(x)$ is the
    payoff. E.g. $h(x) = (x-K)^+$ for call options, and $h(x) = (K-x)^+$ for put
    options.}
  \label{tab:double-barrier-payoff}
\end{table}

All barrier options can be further classified by on how they are monitored;
discretely or continuously. Both are traded on the exchanges, although
discretely monitored barrier options are more common. There are several ways of
pricing such options. In some cases, discretely monitored options are more easy
to deal with, while for others continously monitored options a more easy; see
Chapters~\ref{cha:sequ-monte-carlo} and \ref{cha:pde-approach}, respectively. In
the same way, some methods deal more easily with single barrier options, whereas
some are more well suited for double barrier options; see again
Chapters~\ref{cha:sequ-monte-carlo} and \ref{cha:pde-approach}. These features
and more will be discussed later in this paper.

\subsection{Option pricing formulas}\label{sec:opti-pric-form}
As previously stated, this paper will focus on different methods for pricing
barrier optioins. Before going in depth with these, it is of course worth
mentioning some well known analytical formulas for pricing options.

Below are formulas for continuously monitored barrier call options in a number
of cases. Formulas for put options will not be stated, since the pricing of
these are outside the scope of this paper. The formulas for the single barrier
options are taken from \citet{hull2009options}, and the formula for the double
barrier option is from \citet{kunitomo1992pricing}. Note that in each case the
knock-in and knock-out options are related through the in-out parity
\begin{equation}
  \label{eq:24}
  c = c_{\text{i}} + c_{\text{o}},
\end{equation}
where $c_{\text{i}}$ is the price of an in call option, and $c_{\text{o}}$ is
the price of the corresponding out call option. This parity holds of course for
all other payoffs $h$ as well, since it relies only on
\begin{equation}
  \label{eq:48}
  \expect h(S_T) = \expect[h(S_T);\text{barrier breached}] +
  \expect[h(S_T);\text{barrier not breached}].
\end{equation}

The present value of a down-and-in call is given by
\begin{equation}
  \label{eq:20}
  c_{\text{di}} = S_0\pbrac*{\frac{B}{S_0}}^{2\lambda}\Phi(x) - K \e^{-rT}\pbrac*{\frac{B}{S_0}}^{2\lambda-2}\Phi(x-\sigma\sqrt{T})
\end{equation}
where $K$ is the strike price, $B$ is the barrier and
\begin{equation}
  \label{eq:21}
  \lambda = \frac{r}{\sigma^2} + \half \qquad\text{and}\qquad x =
  \frac{\log\pbrac*{B^2/(S_0K)}}{\sigma\sqrt{T}} + \lambda\sigma\sqrt{T},
\end{equation}
when $K \geq B$. The down-and-out call price is given through the in-out parity
\eqref{eq:24}. In the case when $K \leq B$ the down-and-out call price is given
by
\begin{equation}
  \label{eq:22}
  c_{\text{do}} = S_0\Phi(y_1) - K\e^{-rT}\Phi(y_1-\sigma\sqrt{T}) - S_0
  \pbrac*{\frac{B}{S_0}}^{2\lambda}\Phi(y_2) + K \e^{-rT}\pbrac*{\frac{B}{S_0}}^{2\lambda-2}\Phi(y_2-\sigma\sqrt{T}),
\end{equation}
where
\begin{equation}
  \label{eq:23}
  y_1 = \frac{\log(S_0/B)}{\sigma\sqrt{T}} +
  \lambda\sigma\sqrt{T}\qquad\text{and}\qquad y_2 =
  \frac{\log(B/S_0)}{\sigma\sqrt{T}}
  + \lambda\sigma\sqrt{T}
\end{equation}
and the down-and-in call price is given through \eqref{eq:24}. In each of the above
two cases one can of course also consider the up-options. However, notice that
when $K \geq B$ the up-and-out call is worthless, and hence the up-and-in call
has the same value as a regular call option. In the case when $K \leq B$ the
up-and-in price is
\begin{multline}
  \label{eq:25}
  c_{\text{ui}} = S_0\Phi(y_1) - K\e^{-rT}\Phi(y_1-\sigma\sqrt{T})
  -S_0\pbrac*{\frac{B}{S_0}}^{2\lambda}(\Phi(-x) - \Phi(-y_2))\\
  + K\e^{-rT}\pbrac*{\frac{B}{S_0}}^{2\lambda-2}(\Phi(-x+\sigma\sqrt{T}) -
  \Phi(-y_2+\sigma\sqrt{T}))
\end{multline}
and the corresponding out-option is given by \eqref{eq:24}.

The price of the double barrier knock-out call option is given by
\begin{multline}
  c_{\text{ko}} =
  S_0\sum_{n=-\infty}^\infty\pbrac*{\pbrac[\Bigg]{\frac{U^n}{L^n}}^{2\lambda}
    \pbrac{\Phi(d_{1,n}) - \Phi(d_{2,n})} -
    \pbrac[\Bigg]{\frac{L^{n+1}}{S_0U^n}}^{2\lambda} (\Phi(d_{3,n}) -
    \Phi(d_{4,n}))}\\
   - K\e^{-rT}
  \sum_{n=-\infty}^\infty\left(\pbrac[\Bigg]{\frac{U^n}{L^n}}^{2\lambda -
      2}(\Phi(d_{1,n} - \sigma\sqrt{T}) - \Phi(d_{2,n} - \sigma\sqrt{T}))\right.\\
    - \left. \pbrac[\Bigg]{\frac{L^{n+1}}{S_0U^n}}^{2\lambda-2}(\Phi(d_{3,n} -
    \sigma\sqrt{T}) - \Phi(d_{4,n} - \sigma\sqrt{T}))\right),
\end{multline}
where
\begin{alignat}{2}
  d_{1,n} &= \frac{\log(S_0U^{2n}/(K L^{2n}))}{\sigma\sqrt{T}} +
  \lambda\sigma\sqrt{T},&\quad
  d_{2,n} &= \frac{\log(S_0U^{2n}/(K L^{2n}))}{\sigma\sqrt{T}} +
  \lambda\sigma\sqrt{T}\\
  d_{3,n} &= \frac{\log(L^{2n+2}/(S_0 K U^{2n}))}{\sigma\sqrt{T}} +
  \lambda\sigma\sqrt{T},&\quad
  d_{4,n} &= \frac{\log(L^{2n+2}/(S_0 U^{2n+1}))}{\sigma\sqrt{T}} +
  \lambda\sigma\sqrt{T}.
\end{alignat}
Even though $c_{\text{ko}}$ is given as an infinite sum, in practice only quite
few terms are necessary.

\begin{rem}
  The formulas in this section gives the value of continuously monitored barrier
  options. Formulas for discrete barrier options does not exist, however, one
  may use the continuity correction introduced by \citet{broadie1997continuity}
  to fit these to the discrete case.
\end{rem}