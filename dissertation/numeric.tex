\chapter{Numerical results}\label{cha:numerical-results}
In this chapter some numerical results obtained from the implementations in
Appendix~\ref{cha:code} are presented. The underlying is in all cases assumed to
follow \eqref{eq:17} with $\sigma = 0.2$ and $r = 0.05$. When the option
considered has only one barrier, this is taken to be $B = 60$. When there are
two barriers, these are $L = 40$ and $U = 60$. In both cases the strike price is
$K = 50$ and the time to maturity $T = 1$.

\section{Finite differences}
Figure~\ref{fig:single-barrier-pde} shows the relative errors of a number of
single barrier option prices, evaluated using finite differences on the two
different partial differential equations given in
Chapter~\ref{cha:pde-approach}. These are plotted against computation time,
measured in seconds. Each of the points corresponds to a combination of $M$ and
$N$, both taking values in $\{50, 100, 500, 1500, 2000, 3000\}$. Here $M$ is the
number of steps in the space dimension, and $N$ is the number of steps in the
time dimension. Points which are connected corresponds to a fixed value of $N$,
and varying values of $M$. The plots show quite clearly the trade off between
accuracy and efficiency. 

Furthermore, one can see that in most cases, the biggest gain in accuracy comes
by increasing $M$ rather than $N$. This is not true for the Black-Scholes
equation when the spot price is is close to 0, where the biggest gain comes from
increasing $M$, i.e. refining the space dimension, rather than the time
dimension. Notice that in the case where $S_0=10$, the actual price is of the
order of $10^{-15}$, which might explain the very high relative errors for this
case, giving for instance round-off errors a much greater impact.

In this example, the finite differences method on the Black-Scholes equation
generally performs less than a factor of ten better than the log transformed
equation, in terms of relative error. It is also worth noting that when $S_0 =
55$, the log transformed equation does not actually give the smallest errors at
the largest computation times.

\begin{figure}[tbh]
  \centering
  \tikzsetnextfilename{singlebarrierpde}
  \input{figures/singleloglog.tex}
  \caption{Relative errors of single barrier call option prices for three
    different spot prices.}
  \label{fig:single-barrier-pde}
\end{figure}

Similarly to Figure~\ref{fig:single-barrier-pde},
Figure~\ref{fig:double-barrier-pde} shows the relative errors of some double
barrier options, evaluated using finite differences. Again, each point
corresponds to a pair of values $M, N$, each of which takes values in $\{50,
100, 500, 1500, 2000, 3000\}$, with $M$ determining the space-discretization and
$N$ the time-discretization. Connected points corresponds to the same value of
$N$ and varying values of $M$.

In the case of the Black-Scholes equation, the errors generally decreases as a
function of computation time. For the log transformed equation, however, there
is no such trend. In particular, one can see that the errors do not decrease
strictly as the computation time grows, and in fact, one can notice that for
each $N$, the method generally performs better before reaching the maximum
computation time. Furthermore, the solution of the log transformed equation is
generally more accurate than the solution of the Black-Scholes equation, up to
around a factor of 10. This is contrary to the situation above.

These plots shows that both methods do converge to the actual value, in both
cases. However, they also show that there is no silver bullet, even when
considering just finite differences methods. The optimal method and how the
paramters should be chosen very much
depends on the problem at hand.

\begin{figure}[tbh]
  \centering
  \tikzsetnextfilename{doublebarrierpde}
  \input{figures/doubleloglog.tex}
  \caption{Relative errors of double barrier call option prices for three
    different spot prices.}
  \label{fig:double-barrier-pde}
\end{figure}

Figures~\ref{fig:single-barrier-pde-ms-convergence},
\ref{fig:single-barrier-pde-ns-convergence},
\ref{fig:double-barrier-pde-ms-convergence} and
\ref{fig:double-barrier-pde-ns-convergence} more directly show the convergence
of these schemes to the actual prices. By looking at these graphs, it will
appear that the finite differences method applied to the log transformed
Black-Scholes partial differential equation has faster convergence, than when
applied to the classic Black-Scholes equation. To make this more precise, let
$u_{M,N}(t,x)$ be a finite differences approximation to $u$ at a point
$(t,x)\in[0,T)\times (L,U)$ for some interval $(L,U)$, where the space step-size
is $(U-L)/M$ and the time step-size is $T/N$. If there are constants $C_N$ and
and $q_N$ for a fixed $M$ such that
\begin{equation}
  \label{eq:32}
  \abs{u_{M,N}(t,x) - u(t,x)} \leq C_N N^{-q_N} \text{ for all } N,
\end{equation}
then $u_{M,N}$ has order of convergence $q_N$ in $N$. Note that, despite the
subscript, $q_N$ and $C_N$ might depend on $M$, but not $N$.

Assuming that the inequality in \eqref{eq:32} holds with approximate equality,
then by taking logarithms
\begin{equation}
  \label{eq:45}
  \log\abs{u_{M,N}(t,x) - u(t,x)} \approx \log C_N - q_N\log N\text{ for all } N.
\end{equation}
Hence the order of convergence can be approximated by simple linear
regression. Table~\ref{tab:rate-of-conv} shows approximated orders of convergence
in $M$ and $N$ for both single and double barrier options. The orders of
convergence in $N$ was estimated for $M = 3000$, and likewise the orders in $M$
were estimated for $N = 3000$. The table 

\begin{figure}[tb]
  \centering
  \tikzsetnextfilename{singlebarrierMspdeconvergence}
  \input{figures/pde-single-Ms-convergence.tex}
  \caption{Single barrier prices calculated by finite differences for both
    partial differential equations. Each line corresponds to a fixed value of
    $N$. The dashed line is the actual price.}
  \label{fig:single-barrier-pde-ms-convergence}
\end{figure}

\begin{figure}[tb]
  \centering
  \tikzsetnextfilename{singlebarrierNspdeconvergence}
  \input{figures/pde-single-Ns-convergence.tex}
  \caption{Single barrier prices calculated by finite differences for both
    partial differential equations. Each line corresponds to a fixed value of
    $M$. The dashed line is the actual price.}
  \label{fig:single-barrier-pde-ns-convergence}
\end{figure}

\begin{figure}[tb]
  \centering
  \tikzsetnextfilename{doublebarrierMspdeconvergence}
  \input{figures/pde-double-Ms-convergence.tex}
  \caption{Double barrier prices calculated by finite differences for both
    partial differential equations. Each line corresponds to a fixed value of
    $N$. The dashed line is the actual price.}
  \label{fig:double-barrier-pde-ms-convergence}
\end{figure}

\begin{figure}[tb]
  \centering
  \tikzsetnextfilename{doublebarrierNspdeconvergence}
  \input{figures/pde-double-Ns-convergence.tex}
  \caption{Double barrier prices calculated by finite differences for both
    partial differential equations. Each line corresponds to a fixed value of
    $M$. The dashed line is the actual price.}
  \label{fig:double-barrier-pde-ns-convergence}
\end{figure}

\begin{table}[tb]
  \centering
  \begin{tabular}[tbh]{rl*{6}{l}}
    & & \multicolumn{3}{c}{Single barrier} &
    \multicolumn{3}{c}{Double barrier}\\
    \cmidrule(lr){3-5}\cmidrule(lr){6-8}
    Convergence in & PDE & $S_0 = 10$ & $S_0 = 30$ & $S_0 = 55$ & $S_0 = 45$ &
    $S_0 = 50$ & $S_0 = 55$\\
    \midrule
    $M$ & B-S &    1.8978 &   1.0165  &  0.9096 &   0.9238  &  0.8397 &   0.7938\\
    $N$ & B-S &    1.9742 &   0.8039  &  0.7733 &   0.6045  &  0.8475 &   0.8922\\
    $M$ & log &    2.0587 &   1.0851  &  1.3171 &   1.2229  &  1.1941 &   0.9602\\
    $N$ & log &    1.7194 &   0.3836  &  0.4196 &   1.3521  &  1.1087 &   1.0743
  \end{tabular}
  \caption{Orders of convergence of the finite difference-solutions to the
    Black-Scholes (B-S) equation and the log transformed Black-Scholes equation.}
  \label{tab:rate-of-conv}
\end{table}
