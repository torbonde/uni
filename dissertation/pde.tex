\chapter{Finite differences}\label{cha:pde-approach}
This chapter will focus on finite differences, which is a method for numerically
solving partial differential equations. Two partial differential equations will
be considered for each of the case of single and double barrier options. First,
a result on the existence of a unique solution to the partial differential
equation \eqref{eq:62}, and a result on the relationship between this solution
and the stochastic differential equation \eqref{eq:66}.

Let in the following $\Diff_tu(t,x) \defby \pd{t}u(t,x)$, $\Diff_xu(t,x) \defby
\pd{x}u(t,x)$ and $\Diff_{xx} \defby \pd[^2]{x^2}u(t,x)$ for $u\in C^{1,2}$ and
let the elliptic operator $\Lop$ be given by
\begin{equation}
  \label{eq:28}
  \Lop u(t,x) \defby b(t,x)\Diff_x u(t,x) + \half a(t,x)\Diff_{xx}u(t,x) +
  c(t,x)u(t,x)
\end{equation}
with $a(t,x) = \sigma^2(t,x)$ on $\bar{H}$, where $H = [0,T) \times Q$, $Q$ is
an open interval in $\reals$, and $\sigma$, $b$ and $c$ are Lipschitz
functions. Consider the following partial differential equation
\begin{subequations}
  \label{eq:62}
  \begin{empheq}{alignat=3}
    \Diff_t u(t,x) + \Lop u(t,x) & = f(t,x) \qquad &\text{for } (t,x)&\in
    H \label{eq:63}\noeqref{eq:63} \\
    u(t,x)                       & = g(t,x) \qquad &\text{for } (t,x)&\in
    B \label{eq:64}\noeqref{eq:64} \\
    u(T,x)                       & = h(x)   \qquad &\text{for } x    &\in
    Q \label{eq:65}\noeqref{eq:65} 
  \end{empheq}
\end{subequations}
for some functions $f$, $g$ and $h$, where $B = [0,T)\times\partial Q$. The
following result is taken from \citet{friedman2006stochastic}.

\begin{thm}\label{thm:ex-uniq-friedman}
  Assume that $a$ is bounded away from zero; i.e. there is a fixed $\epsilon >
  0$ such that $a(t,x) \geq \epsilon$ for all $(t,x) \in H$, and assume
  furthermore that $a$ and $b$ are uniformly Lipschitz continuous on $\bar{H}$
  and that $c$ and $f$ are uniformly H\"older continuous on $\bar{H}$. Let now
  $g$ and $h$ be functions such that $g$ is continuous on $\bar{B}$, $h$ is
  continuous on $\bar{Q}$ and
  \begin{equation}
    \label{eq:50}
    g(T,x) = h(x) \quad \text{for all } x\in\partial Q.
  \end{equation}
  Then there exist a unique solution $u\in C^{1,2}([0,T)\times Q)$ to the
  partial differential equation \eqref{eq:62}.
\end{thm}

Let now $t\in[0,T]$, $x\in\reals$ and consider the stochastic differential
equation
\begin{equation}
  \label{eq:66}
  \diff S_s = b(s,S_s)\,\diff s + \sigma(s,S_s)\,\diff W_s,\qquad
  s\in[t,T],\qquad S_t = x.
\end{equation}
This has a unique solution, which in the following will be denoted by $S^{t,x} =
(S_s^{t,x})_{s\in[t,T]}$. Below, this superscript might sometimes be dropped
when the process is started at $t = 0$ and the initial value is not
important. As the following theorem will show, the partial differential equation
\eqref{eq:62} is very closely related to $S^{t,x}$.

\begin{thm}\label{thm:pde-sde-relationship}
  Assume that the conditions on $a$, $b$, $c$, $f$, $g$ and $h$ from
  Theorem~\ref{thm:ex-uniq-friedman} hold. Let $\tau^{t,x} \defby
  \inf\Set[\big]{s\geq t : S_s^{t,x}\notin Q}$ be the first exit time of the
  process $S^{t,x}$ from the interval $Q$ and let furthermore $Z_s^{t,x}\defby
  \e^{\int_t^sc(q,S_q^{t,x})\,\diff q}$. Then
  \begin{equation}
    \label{eq:67}
    u(t,x) = \expect\sbrac[\big]{Z_{\tau^{t,x}}^{t,x}g(\tau^{t,x},S_{\tau^{t,x}}^{t,x})
      \ind_{\tau^{t,x}<T}} +\expect\sbrac[\big]{Z_T^{t,x}h(S_T^{t,x})
      \ind_{\tau^{t,x}\geq T}} -
    \expect\sbrac[\Big]{\int_t^{\tau^{t,x}\wedge T}Z_s^{t,x}f(s,S_s^{t,x}) \,\diff s}
  \end{equation}
  is the unique solution to \eqref{eq:62}.
\end{thm}
\begin{proof}
  Let $u\in C^{1,2}([0,T)\times Q)$ be the unique solution to \eqref{eq:62} and
  take $s\in[t,T]$ and observe that $\diff Z_s^{t,x} =
  c(s,S_s^{t,x})Z_s^{t,x}\,\diff s$. Then by It\^o's formula
  \begin{align}
    \diff \pbrac[\big]{Z_s^{t,x}u(s,S_s^{t,x})} ={}& Z_s^{t,x}\,\diff
    u(s,S_s^{t,x}) +
    u(s,S_s^{t,x})\,\diff Z_s^{t,x} + \diff Z_s^{t,x}\,\diff u(s,S_s^{t,x})\\
    ={}& Z_s^{t,x}\pbrac[\big]{\Diff_tu(s,S_s^{t,x}) +
      b(s,S_s^{t,x})\,\Diff_xu(s,S_s^{t,x}) +
      \half\sigma^2(s,S_s^{t,x})\,\Diff_{xx}u(s,S_s^{t,x})}\,\diff s\\
    &+ Z_s^{t,x}\sigma(s,S_s^{t,x})\,\Diff_xu(s,S_s^{t,x})\,\diff W_s +
    Z_s^{t,x} c(s,S_s^{t,x})u(s,S_s^{t,x})\,\diff s\\
    ={}& Z_s^{t,x}(\Diff_t + L)u(s,S_s^{t,x})\,\diff s +
    Z_s^{t,x}\sigma(s,S_s^{t,x})\,\Diff_x u(s,S_s^{t,x})\,\diff W_s,
  \end{align}
  which can be written equivalently in its integral form
  \begin{equation}
    \label{eq:68}
    \begin{split}
      Z_s^{t,x}u(s,S_s^{t,x}) &= u(t,x) + \int_t^s Z_q^{t,x} (\Diff_t +
      L)u(q,S_q^{t,x})\,\diff q + \int_t^s
      Z_q^{t,x}\sigma(q,S_q^{t,x})\Diff_xu(q,S_q^{t,x})\,\diff W_q\\
      &= u(t,x) + \int_t^s Z_q^{t,x} f(q,S_q^{t,x})\,\diff q + m_s,
    \end{split}
  \end{equation}
  where $m_s\defby \int_t^s Z_q^{t,x}\sigma(q,S_q^{t,x})\Diff_x
  u(q,S_q^{t,x})\,\diff W_q$ for $s\in[t,T]$ and the last equality follows from
  \eqref{eq:63}. Let now $\rho^{t,x} \defby
  \tau^{t,x}\wedge T$ and notice that the stopped process
  $(m_{s\wedge\rho^{t,x}})_{s\in[t,T]}$ is a martingale, and hence
  $\expect m_{s\wedge \rho^{t,x}} = 0$ for any $s\in[t,T]$. Replacing $s$ with
  $\rho^{t,x}$ in \eqref{eq:68} and taking expectations, one gets
  \begin{align}
    u(t,x) ={}&
    \expect\sbrac{Z_{\rho^{t,x}}^{t,x}u(\rho^{t,x},S_{\rho^{t,x}}^{t,x})} -
    \expect\sbrac[\Big]{\int_t^{\rho^{t,x}}Z_s^{t,x}f(s,S_s^{t,x})\,\diff s}\\
    ={}&
    \expect\sbrac{Z_{\rho^{t,x}}^{t,x}
      u(\rho^{t,x},S_{\rho^{t,x}}^{t,x})\ind_{\tau^{t,x}<T}}
    + \expect\sbrac{Z_{\rho^{t,x}}^{t,x}
      u(\rho^{t,x},S_{\rho^{t,x}}^{t,x})\ind_{\tau^{t,x}\geq T}} -
    \expect\sbrac[\Big]{\int_t^{\rho^{t,x}} Z_s^{t,x}f(s,S_s^{t,x})\,\diff s}\\
    ={}&
    \expect\sbrac[\big]{Z_{\tau^{t,x}}^{t,x}
      g(\tau^{t,x},S_{\tau^{t,x}}^{t,x})\ind_{\tau^{t,x}<T}} 
    + \expect\sbrac[\big]{Z_T^{t,x}h(S_t^{t,x})\ind_{\tau^{t,x}\geq T}} -
    \expect\sbrac[\Big]{\int_t^{\tau^{t,x}\wedge T} Z_s^{t,x}f(s,S_s^{t,x})\,\diff s}.
  \end{align}
  The third equality follows by \eqref{eq:64} and \eqref{eq:65}.
\end{proof}

Under the classic Black-Scholes model \eqref{eq:17} considered in this paper,
$b(t,x) = r x$, $a(t,x) = \sigma^2 x^2$ and $c \equiv -r$, where $\sigma$ is a
constant, and $\Lop$ is hence given by
\begin{equation}
  \label{eq:26}
  \Lop u(t,x) = r x \Diff_xu(t,x) + \half\sigma^2x^2\Diff_{xx}u(t,x) - ru(t,x).
\end{equation}
When $Q = (L,U)$ with $0 < L < U$, $f \equiv 0$ and $g\equiv 0$, $u$ is the
price of the double barrier knock-out option with payoff $h(S_T)$ and
\eqref{eq:62} becomes
\begin{subequations}
  \label{eq:39}
  \begin{empheq}{alignat=3}
    \Diff_t u(t,x) + \Lop u(t,x) &= 0\qquad  & \text{for }
    (t,x)&\in[0,T)\times(L,U) \label{eq:40}\noeqref{eq:40}\\
    u(t,L) = u(t,U) &= 0     \qquad  & \text{for }
    t&\in[0,T)             \label{eq:41}\noeqref{eq:41}\\ 
    u(T,x) &= h(x)  \qquad  & \text{for }
    x&\in[L,U]             \label{eq:43}\noeqref{eq:43} 
  \end{empheq}
\end{subequations}
Note that if $h$ is continuous on $[L,U]$ with $h(L) = h(U) = 0$, then all the
assumptions of Theorem~\ref{thm:ex-uniq-friedman} hold, hence \eqref{eq:39} has
a unique solution and Theorem~\ref{thm:pde-sde-relationship} reduces to the
following result.

\begin{thm}\label{thm:double-barrier-pde}
  Let $\Lop$ be given as in \eqref{eq:26} and let $h$ be any continuous function
  on $[L,U]$ such that $h(L) = h(U) = 0$. Let $\tau^{t,x} = \inf\cbrac*{s\geq t
    : S_s^{t,x}\notin(L,U)}$ be the first exit time of the process $S^{t,x}$
  from $(L,U)$. Then
  \begin{equation}
    \label{eq:44}
    u(t,x) = e^{-r(T-t)}\expect\sbrac[\big]{h(S_T^{t,x})\ind_{\tau^{t,x}>T}}
  \end{equation}
  is the unique solution to \eqref{eq:39}.
\end{thm}

If $h(x) = (x-K)^+$ with $K\in(L,U)$ then the continuity condition \eqref{eq:50}
of Theorem~\ref{thm:ex-uniq-friedman} fails due to $h(U) = U-K \neq 0$. It can
be shown that the result still holds in this case, however it has not been
possible to find a reference, since the consulted texts on financial mathematics
seems to neglect such details. For an intuitive explanation of why the result
must still hold, one should realize that the underlying process \eqref{eq:17}
will only reach the problematic point $(T,U)$ with probability zero.

Proving this is outside the scope of this paper, although it will be shown, at
least experimentally, that the result of the finite differences scheme presented
below converges to the solution of the partial differential
equation. Alternatively, consider the case when $h$ is replaced by the function
\begin{equation}
  \label{eq:56}
  h^\epsilon(x) \defby
  \begin{cases}
    \frac{U-x}{\epsilon}(U-\epsilon-K) & \text{for } x\in[U-\epsilon,U]\\
    (x-K)^+ & \text{else}
  \end{cases}
\end{equation}
for some $\epsilon>0$. Then clearly all conditions of
Theorem~\ref{thm:ex-uniq-friedman} are satisfied, and by decreasing $\epsilon$
the solution $u^\epsilon$ to the partial differential equation \eqref{eq:39} can
approximate the price $u$ arbitrarily well, since
$\Prob(S_T\in[U-\epsilon,U])\to 0$ as $\epsilon\to 0$. Hence, since $u^\epsilon$
can be approximated arbitrarily well using finite differences, so can the price
$u$.

Note that when $\Lop$ is given as in \eqref{eq:26} the partial differential
equation \eqref{eq:39} might degenerate if the lower boundary $L$ becomes too
small, due to the $x$ and $x^2$ coefficients on $\Diff_x$ and $\Diff_{xx}$,
respectively. In practice, this will in particular cause trouble if these
coefficients fall below machine precision. To account for this, one might apply
the log transform which will give rise to a new and somewhat nicer partial
differential equation with constant coefficients: Let $u$ be the unique solution
to \eqref{eq:39} and define $v(t,y)\defby u(t,\e^y)$, or equivalently $u(t,x) =
v(t,\log x)$ for $x>0$. Then clearly
\begin{align}
  \Diff_t u(t,x) &= \Diff_t v(t,x),\\
  \Diff_x u(t,x) &= \frac{1}{x} \Diff_y v(t,\log x),\\
  \Diff_{xx} u(t,x) &= -\frac{1}{x^2}\Diff_y v(t,\log x) +
  \frac{1}{x^2}\Diff_{yy} v(t,\log x)
\end{align}
and so $v$ must solve the partial differential equation
\begin{equation}
  \label{eq:60}
  \begin{alignedat}{3}
    \Diff_t v(t,y) + \tfrac{1}{2}\sigma^2\Diff_{yy} v(t,y) + \theta \Diff_y v(t,y)
  - r
  v(t,y) & = 0\qquad & \text{for } (t,y)&\in [0,T) \times (\log L, \log U) \\
  v(t,\log L) = v(t,\log U)  & = 0 \qquad & \text{for } t&\in [0,T)\\
  v(T,y) & = h(\e^y) \qquad & \text{for } y&\in (\log L, \log U)
  \end{alignedat}
\end{equation}
where $\theta = r-\half\sigma^2$. Both of these methods have been implemented
for comparison. The code can be found in Appendix~\ref{cha:code}.

\section{The single barrier case}
In this section the case of a single barrier option will be considered. Denote
by $u(t,x)$ the price of a up-and-out barrier option on $S$ with barrier $B$ and
payoff $h(x) = (x-K)^+$ for some $K < B$, where $S_t = x$. That is
\begin{equation}
  \label{eq:81}
  u(t,x) =
  \e^{-rT}\expect\sbrac[\big]{h(S_T^{t,x})
    \ind_{\tau^{t,x}>T}}\quad\text{where}\quad\tau^{t,x}\defby
  \inf\{s\geq t : S_s^{t,x} \geq B\}.
\end{equation}
Then by pde theory, and by \citet{shreve2004stochastic}, $u$ is the unique
solution to the Black-Scholes partial differential equation
\begin{equation}
   \label{eq:82}
  \begin{alignedat}{3}
    \Diff_t u(t,x) + \Lop u(t,x) & = 0\qquad      & \text{for } (t,x) & \in[0,T)\times(0,B) \\
    u(t,0) = u(t,B)              & = 0  \qquad    & \text{for } t     & \in[0,T)\\ 
    u(T,x)                       & = h(x)  \qquad & \text{for } x     & \in[0,B] 
  \end{alignedat}
\end{equation}
where $\Lop$ is given as in \eqref{eq:26}. Note that the lower boundary being 0
makes sense, since if the process $S$ is started at 0 it will remain 0, and
$h(0) = 0$.

As an alternative, consider the log transformed partial differential
equation. Naturally, one can not apply the log transform to \eqref{eq:82}, due
to the lower boundary being 0. If one were to na\"ively apply the log transform,
the domain of the resulting partial differential equation would be unbounded. In
practice the domain would then need to be truncated. Consider therefore instead
directly the truncated partial differential equation
\begin{equation}
  \label{eq:18}
  \begin{alignedat}{3}
    \Diff_t v_R(t,y) + \tfrac{1}{2}\sigma^2\Diff_{yy} v_R(t,y) + \theta \Diff_y
    v_R(t,y) - r
    v_R(t,y) & = 0\qquad & \text{for } (t,y)&\in [0,T) \times (-R, \log B) \\
    v_R(t,-R) = v_R(t,\log B)  & = 0 \qquad & \text{for } t&\in [0,T)\\
    v_R(T,y) & = h(\e^y) \qquad & \text{for } y&\in (-R, \log B)
  \end{alignedat}
\end{equation}
with $R > 0$. This corresponds to \eqref{eq:60} with $U = B$ and $L = \e^{-R}$
and hence $v_R(t,\log x)$ is the price of a double barrier knock-out option with
these barriers;
\begin{equation}
  \label{eq:19}
  v_R(t,\log x) =
  \e^{-r(T-t)}\expect\sbrac[\big]{h(S_T^{t,x})\ind_{\tau_R^{t,x}>T}}\quad\text{where}
  \quad\tau_R^{t,x} \defby\inf\{s \geq t : S_s^{t,x}\notin(\e^{-R},B)\}.
\end{equation}

Truncating the domain naturally introduces an extra source of error, which
depends on $R$. The question is then how big this truncation error is. To
estimate this, let first $\rho_R^{t,x} \defby \inf\{s\geq t : \log x + (r -
\half\sigma^2) (s-t) + \sigma W_{(s-t)} \leq - R\}$ so that $\tau_R^{t,x} =
\tau^{t,x}\wedge\rho_R^{t,x}$ and in particular
\begin{equation}
  \label{eq:30}
  \ind_{\tau_R^{t,x}>T} = \ind_{\tau^{t,x}>T} \ind_{\rho_R^{t,x} > T}.
\end{equation}
In the following it will be assumed for simplicity that $t=0$, i.e. that the
price of interest is $u(0,S_0)$. To avoid clutter the superscripts will be
dropped. Then by \eqref{eq:81}, \eqref{eq:19} and the Cauchy-Schwartz inequality
the error can be bounded from above by
\begin{align}
  \epsilon_R &= \abs{v_R(0, \log S_0) - u(0, S_0)} \leq
  \e^{-rT}\expect\sbrac[\Big]{\abs[\big]{h(S_T)\ind_{\tau>T}(\ind_{\rho_R>T} -
      1)}}\\
  &= \e^{-rT}\expect\sbrac[\Big]{\abs[\big]{h(S_T)\ind_{\tau>T}}\ind_{\rho_R\leq
      T}} \leq \e^{-rT}\sqrt{\expect\sbrac[\big]{h^2(S_T)\ind_{\tau>T}}}
  \sqrt{\Prob\pbrac[\big]{\rho_R \leq T}}\\
  & \leq C_1 \sqrt{\Prob\pbrac{\rho_R\leq T}},
\end{align}
for some constant $C_1$, where the last inequality follows since $h$ is bounded
on $(\e^{-R},B)$. Now let $\alpha,\beta\in(-R,\log B)$ with $\alpha < \beta$, and
assume that $\log S_0\in (\alpha,\beta)$. Then there is a constant $\nu
\in(0,1)$, independent of $S_0$ such that $\abs{\log S_0} \leq \nu R$ and hence
$-\abs{R+\log S_0} \geq -R(\nu + 1)$. Now one can calculate, see for instance
\citet{gyongy2015findiff} or \citet{krylov2002introduction},
\begin{equation}
  \label{eq:33}
  \Prob(\rho_R\leq T) = \Prob\pbrac[\Bigg]{\inf_{t\in[0,T]}\cbrac[\Big]{\pbrac[\Big]{\frac{r}{\sigma} -
  \frac{\sigma}{2}}t + W_t} \leq \frac{-(R+\log S_0)}{\sigma}} \leq C_2\e^{- C_3
(R+\log S_0)^2/\sigma^2} \leq C_2 \e^{- C_4 R^2},
\end{equation}
where $C_2$ and $C_3$ are constants depending only on $\abs{r-\half\sigma^2}$,
$\sigma^2$ and $T$, and $C_4 = C_3(\nu + 1)^2/\sigma^2$. Combining these
calculations, one gets the upper bound on the truncation error $\epsilon_R \leq
C_1 \sqrt{C_2}\e^{-\half C_4 R^2}$.

\section{Finite differences}
Theorem~\ref{thm:double-barrier-pde} verifies that one can find the price of a
double barrier option by solving the partial differential
equation~\eqref{eq:39}. Doing this explicitly is not generally possible. Rather,
this section will focus on solving the partial differential equation
numerically, by applying a finite differences scheme.

A finite differences scheme works by replacing differentials by finite
differences. For instance, $\od{x}\phi(x)$ might be replaced by $(\phi(x + h) -
\phi(x))/h$, where $h>0$ is the grid size. This is a forward difference estimate
and converges to $\od{x}\phi(x)$ as $h\to 0$. Instead of this one could have
chosen the backward difference $(\phi(x) - \phi(x-h))/h$ or the centered
difference $(\phi(x+h)-\phi(x-h))/(2h)$.

Let in the following $M$ and $N$ be positive integers, and let $h = (U-L)/M$ and
$\tau = T/N$. Consider the following difference operators
\begin{alignat}{2}
  \delta_{x,h}\phi(t,x) &\defby \frac{\phi(t,x+h) - \phi(t,x)}{h},&\quad
  \delta_{x,-h}\phi(t,x) &\defby \frac{\phi(t,x) - \phi(t,x-h)}{h},\\
  \delta_{t,\tau}(t,x) &\defby \frac{\phi(t+\tau,x)-\phi(t,x)}{\tau},&
  \delta_{t,-\tau}(t,x) &\defby \frac{\phi(t,x) - \phi(t-\tau)}{\tau}.\label{eq:52}
\end{alignat}
Here $\delta_{x,h}$ is a forward operator and $\delta_{x,-h}$ is a backward
operator. From the perspective of the partial differential equation
\eqref{eq:39}, $\delta_{t,\tau}$ is in fact also a backward operator and
$\delta_{t,-\tau}$ is a forward operator. The first two of these may be used to
estimate $\Diff_x\phi(t,x)$, while the last two may be used for estimating
$\Diff_t\phi(t,x)$. When estimating $\Diff_{xx}\phi(t,x)$ it is sensible to use
a centered operator, combining the forward and backward operators;
\begin{equation}
  \label{eq:51}
  \delta_{x,h}\delta_{x,-h}\phi(t,x) = \frac{\phi(t,x+h) - 2\phi(t,x) +
    \phi(t,x-h)}{h^2}.
\end{equation}

When one wishes to use finite differences on $b \od{x} \phi(x)$ for some
constant $b$, the upwind scheme suggests using the forward operator for $b > 0$,
and the backwards scheme for $b < 0$. This method is used in the implementation
solving the log transformed partial differential equation \eqref{eq:60}. In the
implementation solving \eqref{eq:39} the following centered difference operator
is used:
\begin{equation}
  \label{eq:29}
  \half(\delta_{x,h} + \delta_{x,-h})\phi(t,x) = \frac{\phi(t,x+h) -
    \phi(t,x-h)}{2h}.
\end{equation}

For estimating the time derivative, two operators were given in \eqref{eq:52},
both of which have their advantages and disadvantages. Suppose $\Lop^h$ is an
approximation of $\Lop$ by some finite differences method, and let $t_i = i\tau$
for $i=0,\ldots,N$ and $x_j = L+jh$ for $j=0,\ldots,M$. For the sake of brevity
let also $u_{i,j} = u(t_i,x_j)$. Then, to solve \eqref{eq:39} numerically using
finite differences, observe first that $u_{N,j} = h(x_j)$ for all $j=0,\ldots,M$
and $u_{i,0} = u_{i,M} = 0$ for all $i=0,\ldots,N-1$. Next, see that by
replacing $\Diff_t$ by the forward operator $\delta_{t,-\tau}$, to estimate the
solution $u$ one must solve
\begin{equation}
  \label{eq:53}
  \frac{u_{i+1,j} - u_{i,j}}{\tau} + \Lop^hu_{i+1,j} = 0
\end{equation}
for all $i = 0,\ldots,N-1$ and $j = 1,\ldots,M-1$, which can be done recursively
in $i$ using
\begin{equation}
  \label{eq:54}
  u_{i,j} = u_{i+1,j} + \tau \Lop^hu_{i+1,j}.
\end{equation}
This can be done very efficiently which gives this method, called explicit
finite differences, a clear advantage. However, it can be shown that it places
certain restrictions on the relationship between $\tau$ and $h$ for the scheme
to converge \citep{heath2001scientific}. For this reason, more complicated
schemes are often used.

Another approach is called the implicit finite differences scheme, in which one
uses the backward operator $\delta_{t,\tau}$. This scheme has the advantage of
not putting any restrictions on $\tau$ and $h$, and guarantees
convergence. However, instead of \eqref{eq:53} one gets for $i = 0,\ldots,N-1$
and $j = 1,\ldots,M-1$
\begin{equation}
  \label{eq:55}
  \frac{u_{i+1,j} - u_{i,j}}{\tau} + \Lop^hu_{i,j} = 0,
\end{equation}
which leads to a system of $M-1$ equations which must be solved at each step
$i$. Suppose for instance that $\Diff_x$ and $\Diff_{xx}$ are replaced by the
centered operators in \eqref{eq:29} and \eqref{eq:51}, respectively, then
the equation to solve is
\begin{equation}
  \label{eq:27}
  \frac{u_{i+1,j} - u_{i,j}}{\tau} + r x_j \frac{u_{i,j+1} -
  u_{i,j-1}}{2h} + \half\sigma^2x_j^2\frac{u_{i,j+1} - 2u_{i,j} +
  u_{i,j-1}}{h^2} - r u_{i,j} = 0,
\end{equation}
or equivalently
\begin{equation}
  \label{eq:57}
  a_ju_{i,j-1} + b_ju_{i,j} + c_ju_{i,j+1} = u_{i+1,j}
\end{equation}
where $a_1 = 0$, $c_{M-1}=0$ and in all other cases
\begin{equation}
  \label{eq:58}
  a_j = \frac{\tau}{2h}rx_j-\half\frac{\tau}{h^2}\sigma^2x_j^2, \quad b_j = 1 +
  \frac{\tau}{h^2}\sigma^2x_j^2 + \tau r\quad\text{and}\quad c_j =
  -\frac{\tau}{2 h}r
  x_j - \half\frac{\tau}{h^2}\sigma^2x_j^2.
\end{equation}
In the implementation this is solved by stating the problem in its matrix form
\begin{equation}
  \label{eq:59}
  \begin{bmatrix*}[l]
    b_1 & c_1    &                      &                       \\
    a_2 & \ddots &                      &                       \\
        &        & \ddots               & \hspace{-.2em}c_{M-2} \\
        &        & \hspace{-1em}a_{M-1} & \hspace{-.2em}b_{M-1}
  \end{bmatrix*}
  \begin{bmatrix}
    u_{i,1} \\ \vdots\\ \vdots \\ u_{i,M-1}
  \end{bmatrix}
  =
  \begin{bmatrix}
    u_{i+1,1} \\ \vdots\\ \vdots \\ u_{i+1,M-1}
  \end{bmatrix}
\end{equation}
and using the \Matlab backslash-operator. To obtain higher precision using any
finite differences scheme $M$ and $N$ must be increased, which in the implicit
case gives more and larger systems of the type above to solve. This is far less
efficient than the explicit scheme.

\section{Remarks}
The methods introduced in this chapter are fairly general in their nature. Due
to Theorem~\ref{thm:ex-uniq-friedman} and Theorem~\ref{thm:pde-sde-relationship}
finite differences schemes can be applied to a very large class of
models. Moreover, the same methods can be applied to cases where the barriers
are not constant, but functions of time.

One case where this approach does not perform well is when the option is to be
monitorred at discrete times only. In this case the barriers are discontinuous
and the solution $u(t,x)$ jumps at the monitoring times. Since the schemes
presented in this chapter assumes continuity of the solution, they may lead to
unwanted features such as spikes in the approximation in this setting. A finite
difference scheme allowing such jumps is proposed in \citet{duffy2013finite}.
