\chapter{Fourier approach}
In this chapter the focus is on a direct approach presented by
\citet{borovkov2002new}. Suppose one wishes to calculate the expectations $P_1
\defby \expect (\e^X - K)^+$ and $P_2\defby\expect[(\e^X - K)^+; Z\leq B]$ for
some random variables $X$ and $Z$, and real numbers $K$ and $B$, where for any
set $A\subset\Omega$, $\expect[X ; A]\defby\expect[X \ind_A]$. In order to use the
following method, the moment generating functions $\phi$ and $\psi$ of $X$ and
$(X, Z)$ are required to be known;
\begin{equation}
  \label{eq:1}
  \phi(u) = \expect \e^{uX}\qquad \text{and} \qquad \psi(u,v) = \expect \e^{uX + vZ}.
\end{equation}
Section~\ref{sec:moment-gener-funct} presents analytical expressions for $\phi$
and $\psi$ under the model described in Section~\ref{sec:model}.

The following theorem was presented in \citet{borovkov2002new}.

\begin{thm}\label{thm:borovkov-novikov} Let $K>0$ be any positive, real number,
  and let $a > 0$ be such that
  \begin{equation}
    \label{eq:9}
    \phi(1+a) < \infty.
  \end{equation}
  The expectation $P_1$ is given by
  \begin{equation}
    \label{eq:2}
    P_1 = \frac{1}{2\pi}\int_{-\infty}^\infty \phi(1+a-\i s)g(s)\,\diff s
  \end{equation}
  where $g(s) = \frac{\e^{\i sb - ab}}{(a-\i s)(1+a-\i s)}$ and $b = \log K$.

  Let now $B$ be any real number. The expectation $P_2$ is then given by $P_2 = P_1
  W(B)$, where $W$ is the distribution function determined by the characteristic
  function
  \begin{equation}
    \label{eq:3}
    w(v) = \frac{1}{2\pi P_1}\int_{-\infty}^\infty \psi(1+a-\i s,\i v) g(s)\,\diff s.
  \end{equation}
\end{thm}
\begin{proof}
  In order to prove the first part of the theorem, observe first that for any
  real numbers $a$ and $x$
  \begin{equation}
    \label{eq:4}
    (\e^x-K)^+ = \e^{(1+a)x}G(x)\quad\text{with}\quad G(x) =
    \e^{-ax}H(x)\quad\text{and}\quad H(x) = (1-\e^{b-x})^+.
  \end{equation}
  Next, see that for any complex $s$ with $\Im s \geq 0$
  \begin{equation}
    \label{eq:5}
    \int_{-\infty}^\infty \e^{\i s x}H'(x)\,\diff x = \frac{\e^b}{\i s -
      1}\sbrac*{\e^{(\i s-1)x}}_b^\infty = \frac{\e^{\i s b}}{1-\i
      s},\qquad H'(x) = \begin{cases} 0 & x < b \\ \e^{b-x} &
      \text{else} \end{cases}
  \end{equation}
  where $H'$ is the derivative of $H$. Thus, taking now $s$ to be real and $a>0$,
  \begin{equation}
    \label{eq:6}
    \int_{-\infty}^\infty \e^{\i(s+\i a)x}H'(x)\,\diff x = \frac{\e^{\i(s+\i
        a)b}}{1-\i(s+\i a)} = \frac{\e^{\i s b - a b}}{1 + a - \i s}.
  \end{equation}
  However, integrating the left-hand side by parts, one also notices that
  \begin{align}
    \int_{-\infty}^\infty \e^{\i(s+\i a)x} H'(x) &= \sbrac[\big]{\e^{\i(s+\i
        a)x}H(x)}_{-\infty}^\infty - \i(s+\i a)\int_{-\infty}^\infty \e^{\i(s+\i
      a)x}H(x)\,\diff x\\
    &= \sbrac[\big]{\e^{\i(s+\i a)x}(1-\e^{b-x})}_b^\infty - \i(s+\i
    a)\int_{-\infty}^\infty \e^{\i s x}G(x)\,\diff x
  \end{align}
  wherein the first summand vanishes (since $a>0$) and the integral is the
  Fourier transform of $G$, hence
  \begin{equation}
    \label{eq:7}
    g(s)=\int_{-\infty}^\infty \e^{\i s x}G(x)\,\diff x = \frac{\e^{\i sb -
        ab}}{(a-\i s)(1+a-\i s)}
  \end{equation}
  
  Obviously $g$ is integrable on $\reals$, and so $G$ can be represented as the
  inverse Fourier transform of $g$, giving finally an expression for $P_1$;
  \begin{equation}
    \label{eq:8}
    P_1  = \expect\sbrac[\big]{\e^{(1+a)X}G(X)} =
    \frac{1}{2\pi}\expect\int_{-\infty}^\infty \e^{(1+a-\i s)X}g(s)\,\diff s =
    \frac{1}{2\pi}\int_{-\infty}^\infty\phi(1+a-\i s)g(s)\,\diff s.
  \end{equation}
  Note that the last equality follows by Fubini's theorem, which can be applied
  due to the assumption \eqref{eq:9} giving absolute integrability.

  For the second part of the theorem see that
  \begin{align}
    \int_{-\infty}^\infty\int_{-\infty}^\infty \frac{(\e^x-K)^+}{P_1}
    p_{X,Z}(x,z)\,\diff x \diff z &= \frac{1}{P_1}\int_{-\infty}^\infty
    (\e^x-K)^+p_X(x)\int_{-\infty}^\infty p_{Z | X = x}(z)\,\diff z\, \diff x\\
    &= \frac{1}{P_1}\int_{-\infty}^\infty (\e^x-K)^+ p_X(x)\,\diff x = 1,
  \end{align}
  where $p_{X,Z}$, $p_X$ and $p_{Z|X}$ are densities for $(X,Z)$, $X$ and $Z|X$
  under $\Prob$, respectively. Hence $x\mapsto (\e^x-K)^+/P_1$ is a density with
  respect to the original distribution of $(X, Z)$, and
  \begin{equation}
    \label{eq:10}
    P_2 = \int_{-\infty}^\infty\int_{-\infty}^\infty(\e^x-K)^+\ind_{\Set{z\leq
        B}} p_{X,Z}(x,z)\,\diff x\,\diff z = P_1 \Prob^\ast(Z\leq B)
  \end{equation}
  where $\Prob^\ast$ is the probability measure with this density. Letting
  $p_Z^\ast$ be the density for $Z$ under $\Prob^\ast$, see finally that
  \begin{align}
    w(v) ={}& \int_{-\infty}^\infty \e^{\i v z}p_Z^\ast(z)\,\diff z =
    \frac{1}{P_1}\int_{-\infty}^\infty\int_{-\infty}^\infty \e^{\i v
      z}(\e^x-K)^+p_{X,Z}(x,z)\,\diff x\, \diff z\\
    ={}& \frac{1}{2\pi P_1} \int_{-\infty}^\infty\int_{-\infty}^\infty \e^{\i v
      z}p_{X,Z}(x,z) \int_{-\infty}^\infty \e^{(1+a-\i s)x}g(s)\,\diff
    s\,\diff x\,\diff z\\
    ={}&\frac{1}{2\pi P_1} \int_{-\infty}^\infty \psi(1+a-\i s, \i v)
    g(s)\,\diff s,
  \end{align}
  which is the characteristic function of $Z$ under $\Prob^\ast$, and so $W(B) =
  \Prob^\ast(Z\leq B)$. The third equality comes from \eqref{eq:4} and that $G$
  is the Fourier transform of $g$; $G(x) = \frac{1}{2\pi}\int_{-\infty}^\infty
  \e^{-\i s x}g(x)\,\diff x$.
\end{proof}

The parameter $a$ in the proof can according to \citet{borovkov2002new} be
chosen arbitrarily. In the implementation given in Appendix~\ref{cha:code} this
is chosen to be 9.

\begin{rem}\label{rem:borovkov-novikov-barrier}
  The method introduced in the above theorem and its proof can easily be
  extended to the case when there are two barriers. Suppose for instance that
  one wishes to calculate the expectation $P_3=\expect\sbrac{(\e^X-K)^+; Y\leq
    B_-, Z\leq B_+}$, and that the moment generating function $\chi$ of
  $(X,Y,Z)$ is known. By virtually the same arguments as in the second part of
  the proof, this expectation is given by $P_3 = P_1\bar{W}(B_-,B_+)$ where
  $\bar{W}$ is the distribution function with the characteristic function
  \begin{equation}
    \label{eq:11}
    \bar{w}(u,v) = \frac{1}{2\pi P_1} \int_{-\infty}^\infty \chi(1+a-\i s, \i u,
    \i v)g(s)\,\diff s.
  \end{equation}
\end{rem}

Under the current model, and for the applications to vanilla European, single
barrier up-and-out and double barrier knock-out call options, one obvious way of
defining the random variables $X$, $Y$ and $Z$ is by
\begin{align}
  X= X_T,\quad Y= -\min_{t\in[0,T]}X_t,\quad Z= \max_{t\in[0,T]}X_t
\end{align}
where $X_t= \pbrac*{r - \half\sigma^2}t + \sigma W_t$ as before. Suppose the
strike price of the option is $K'$, the barrier for the up-and-out option is
$B'$ and the lower and upper barriers for the knock-out option are $L$ and
$U$. Then, taking $K \defby K'/S_0$, $B \defby \log(B'/S_0)$,
$B_-\defby\log(S_0/L)$ and $B_+\defby\log(U/S_0)$, the price of the European
call option is $c = \e^{-rT}S_0P_1$, the price of the up-and-out call option is
$c_{\text{uo}} = \e^{-rT}S_0P_2$ and the proce of the knock-out call option is
$c_{\text{ko}} = \e^{-rT}S_0P_3$.

\section{Moment generating functions}\label{sec:moment-gener-funct}
In order to apply Theorem~\ref{thm:borovkov-novikov} and
Remark~\ref{rem:borovkov-novikov-barrier}, it is necessary to first arrive at
expressions for the moment generating functions $\phi$, $\psi$ and $\chi$.  As a
first step towards obtaining these, define $\theta\defby
\frac{r}{\sigma}-\frac{\sigma}{2}$ and $W_t^\ast \defby W_t + \theta t$, and
observe that by the Girsanov theorem $(W_t^\ast)_{t\in[0,T]}$ is a Wiener
martingale with respect to the history of $W$, under some probability measure
$\Prob^\ast$. Note that this is \emph{not} the same probability measure as in
the proof of Theorem~\ref{thm:borovkov-novikov}. Letting now $m_T =
\min_{t\in[0,T]}W_t^\ast$ and $M_T = \max_{t\in[0,T]}W_t^\ast$ one can see that
\begin{equation}
  \label{eq:13}
  \begin{split}
    \phi(u) &= \e^{-\half\theta^2T}\bar{\phi}(u\sigma + \theta),\\
    \psi(u,v) &= \e^{-\half\theta^2T}\bar{\psi}(u\sigma + \theta, v\sigma),\\
    \chi(u,v,w) &= \e^{-\half\theta^2T}\bar{\chi}(u\sigma + \theta, v\sigma,
    w\sigma),
  \end{split}
\end{equation}
where $\bar{\phi}$, $\bar{\psi}$ and $\bar{\chi}$ are the moment generating
functions of $W_T^\ast$, $(W_T^\ast, M_T)$ and $(W_T^\ast, -m_T, M_T)$ under
$\Prob^\ast$, respectively.

It can be seen quite easily that $\bar{\phi}(u) = \e^{\half T u^2}$ and hence
\begin{equation}
  \label{eq:12}
  \phi(u) = \e^{-\half\theta^2T}\e^{\half T (u\sigma + \theta)^2} =
  \e^{\pbrac[\big]{r-\half\sigma^2}u + \half T\sigma^2 u^2}.
\end{equation}
Of course, this could have been seen directly, by noting that $X_t\sim
N\pbrac*{r-\half\sigma^2, t\sigma^2}$ and using the well known formula for the
moment generating function of a normally distributed random
variable.

When it comes to the calculation of $\psi$ and $\chi$, however, the
representation in \eqref{eq:13} is really rather useful. The density function of
the pair $(W_T^\ast,M_T)$ is given by
\begin{equation}
  \label{eq:14}
  p(x,z) =
  \begin{cases}
    \sqrt{\frac{2}{\pi}} \frac{2z-x}{T^{3/2}} \e^{-\frac{(2z-x)^2}{2T}} &
    \text{when } x\leq y, y\geq 0\\
    0 & \text{else}
  \end{cases},
\end{equation}
and can be found in \citet{ioannis1991brownian}. From this one can arrive at the
following expression for $\bar{\psi}$;
\begin{equation}
  \label{eq:15}
  \bar{\psi}(u,v) = 2\Phi\pbrac[\big]{\sqrt{T}(u+v)}\e^{\half
    T(u+v)^2}\frac{u+v}{2u+v} +
  \Phi\pbrac[\Big]{\half\sqrt{T}v}\e^{\frac{1}{8}v^2}
  \pbrac*{\frac{2u\e^{\frac{1}{4}Tu(2u+v)}}{2u+v} - 1}.
\end{equation}
Doing so is a matter of will power more than a matter of brain power, although a
little of both is advisible. In lack of either, one can use instead a computer
algebra system, which is what was done here.

As might perhaps be expected, obtaining $\bar{\chi}$ is less trivial, and even
the density of $(W_T^\ast, -m_T,M_T)$ is not easy to come by. It can be found,
however, from the following expression given in \citet{geman1996pricing};
\begin{align}
  \MoveEqLeft\Prob\pbrac[\big]{W_T^\ast\in\diff x, y\leq m_T\leq
    M_T\leq z}\\
  &=\frac{1}{\sqrt{2\pi T}}\sum_{k=-\infty}^\infty
  \sbrac[\Bigg]{\exp\pbrac[\Big]{-\frac{1}{2T}(x-2k(z-y))^2} - \exp\pbrac[\Big]{-\frac{1}{2T}((x-2z) +
      2k(z-a))^2}}\,\diff x.
\end{align}
Differentiating with respect to $y$ and $z$, and substituting $-y$ for $y$, one
obtains the density $\bar{p}$ of the triple $(W_T^\ast,-m_T,M_T)$
\begin{multline}
  \bar{p}(x,y,z) = \frac{1}{\sqrt{2\pi T}}
  \sum_{k=-\infty}^\infty\frac{4k}{2T}\Biggl[k(T-(2k(z+y)+x)^2)
  \exp\pbrac*{-\frac{1}{2T}(x+2k(z+y))^2}\\
  + (k-1)(((x-2z)+2k(z+y))^2-T)\exp
  \pbrac*{-\frac{1}{2T}((x-2z)+2k(z+y))^2}\Biggr].
\end{multline}
Finally $\bar{\chi}$ is given by the three-dimensional integral
\begin{equation}
  \label{eq:36}
  \bar{\chi}(u,v,w) =
  \int_0^\infty\int_{-\infty}^0\int_y^z\e^{ux+vy+wz}\bar{p}(x,y,z)\,\diff
  x\,\diff y\,\diff z.
\end{equation}

\section{Remarks}
The method discussed in this chapter is of a fairly general kind. In fact, all
it requires is the knowledge of certain moment generating functions. This
requirement does not put any immediate restrictions on the model, although
\citet{borovkov2002new} does require the underlying process to be of L\'evy
type. It will appear, however, that this restriction is merely necessary due to
the example in the same paper, in which they apply a result that requires the
process to have independent increments.

This generality does in theory open up for applications to a wide range of
models, apart from the beloved Black-Scholes model. One of the reasons for the
well-likedness of this model is the ease with which one can calculate so many
things; e.g. barrier option prices, as was seen in
Section~\ref{sec:opti-pric-form}. However, even in this relatively simple model,
the expressions for the moment generating function needed for pricing barrier
options given in Section~\ref{sec:moment-gener-funct} are not very nice to work
with.

There is another important point to make, when it comes to the application of
the second part of Theorem~\ref{thm:borovkov-novikov} and
Remark~\ref{rem:borovkov-novikov-barrier}. When pricing a single barrier option,
one needs to invert the characteristic function $w$ in \eqref{eq:3}, in order to
evaluate the distribution function $W$. To do so,
\citet{shephard1991characteristic} suggest the following formula
\begin{equation}
  \label{eq:37}
  W(x) = \half - \frac{1}{2\pi}\int_0^\infty\Del_u\sbrac*{\frac{1}{\i u}
    w(u)\e^{-\i u x}}\,\diff u
\end{equation}
where the operator $\Del$ is given by $\Del_u f(u) = f(u) + f(-u)$. For
double barrier options the situation is similar, but more complicated, since the
characteristic function which needs to be inverted is the two-dimensional
$\bar{w}$ in \eqref{eq:11}. This can be done by the recursive relation
\citep[Section 4]{shephard1991characteristic}
\begin{equation}
  \label{eq:38}
  2 \bar{W}(x,y) - W_1(x) - W_2(y) + \half = -\frac{1}{\pi^2}\int_0^\infty\int_0^\infty
  \Del_v\Re\sbrac*{\frac{1}{u v} \bar{w}(u,v)\e^{-\i u x - \i v y}}\,\diff
  u\,\diff v,
\end{equation}
where $W_1$ is the distribution function with characteristic function
$\bar{w}(\cdot, 0)$ and $W_1$ is the distribution function with
characteristic function $\bar{w}(0, \cdot)$. Note that $W_1$ is in fact $W$
as given by \eqref{eq:37}, and that $W_2$ can be expressed in a similar
fashion. 

Evaluating \eqref{eq:37} requires estimating a two-dimensional integral where
the integrand is described in terms of the standard normal distribution function
$\Phi$. Performing the two-dimensional quadrature was manageable, however one
problem was introduced when evaluating $\Phi$ at complex numbers, which \Matlab
does not support natively. This problem was sought solved using the
\texttt{erfc} function of the \texttt{Faddeeva}
package\footnote{\url{http://ab-initio.mit.edu/Faddeeva}}, which unfortunately
produces not-a-number and infinite values for some inputs. For this reason, the
method has not been succesfully applied.

Obviously, evaluating $\bar{W}$ is more complicated than evaluating $W$, since
it requires evaluating two integrals of the type \eqref{eq:37} along with the
double integral in \eqref{eq:38}. Due to \eqref{eq:11}, $\bar{w}$ is itself an
integral and its integrand involves $\bar{\chi}$. By \eqref{eq:36} $\bar{\chi}$
is a three-dimensional integral with its integrand given by an infinite
series. Putting all of this together, evaluating $\bar{W}$ requires calculating
a six-dimensional integral of an infinite series over an infinite
domain. Numerical evaluation of multi-dimensional integrals are not generally
handled well by standard quadrature schemes, and it is often beneficial to
estimate the integral using Monte Carlo methods
\citep{heath2001scientific}. This has not been investigated further.

Despite the problems described above, the method has a certain beauty in its own
right. \citet{borovkov2002new} gives an example wherein they price a discretely
monitored lookback option under two different models. One model is the same as
in this paper, and the other is the variance-gamma model where the risky asset
is described by
\begin{equation}
  \label{eq:49}
  S_t = S_0\e^{\pbrac{r-\half\sigma^2}t + \sigma W_{\gamma_t}},
\end{equation}
where $\gamma$ is a gamma process independent of $W$ with $\expect\gamma_t = t$
and $\var(\gamma_t) = t$. This is only possible because of how generic the method
is.

A function for pricing a European call option under the current model has been
implemented in \texttt{bn\_eu\_call.m}, using the methods described in this
chapter. In this case the method performs reasonably well.
