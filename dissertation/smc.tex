\chapter{Sequential Monte Carlo}\label{cha:sequ-monte-carlo}
This chapter will introduce two estimators for each of the discretely monitored
and the continuously monitored barrier options; a standard Monte Carlo estimator
and a sequential Monte Carlo estimator. The sequential Monte Carlo estimator
was introduced in \citet{shevchenko2014valuation}, in which the option prices
are expressed in terms of so-called Feynman-Kac expectations.

\section{Feynman-Kac expectations}
Consider for simplicity first the discretely monitored double barrier knock-out
option with barriers $L$ and $U$, and suppose it is to be monitored at times $0
= t_0 < t_1 < \cdots < t_N = T$, where $N\in\nats$ and $t_i \defby iT/N$. For
the sake of ease, let $S_i = S_{t_i}$ and let furthermore $\tilde{\tau}$ be the
first time $t_i$ such that $S_i\notin(L,U)$, i.e. $\tilde{\tau} \defby \inf\{t_i
: S_{t_i}\notin(L,U)\}$, and let $h$ denote the payoff function. Then the price
of the option is given by
\begin{equation}
  \label{eq:69}
  d_{\text{ko}} = \e^{-rT}\expect\sbrac[\big]{h(S_T)\ind_{\tilde{\tau}>t_N}} =
  \e^{-rT}\expect\sbrac[\Big]{h(S_T)\prod_{n=0}^{N-1}\ind_{(L,U)}(S_{n+1})}.
\end{equation}

The price of the continuously monited double barrier option can be written in a
similar form. To see this let for $s,t\in[0,T]$ with $s<t$ $f(S_s,S_t)$ be the
probability of not hitting the barrier between times $s$ and $t$, conditioned on
$S$ being within the barriers at the end-points; i.e.
\begin{equation}
  \label{eq:70}
  f(S_s,S_t) \defby \Prob\pbrac[\big]{S_q \in (L,U) \text{ for all } q\in(s,t)
    \bigm\vert S_s,S_t\in(L,U)}.
\end{equation}
Taking $s\in(0,T)$ and using the tower property and the definition of $f$,
\begin{align*}
  \expect\sbrac[\big]{h(S_T); S_t\in(L,U)\text{ for all } t\in(0,T]} &=
  \expect\sbrac[\big]{h(S_T); S_s,S_T\in(L,U), S_t\in(L,U)\text{ for all } t
    \in (0,s)\cup(s,T)}\\
  &= \expect\bigl[ h(S_T) f(S_s,S_T); S_s,S_T\in(L,U), S_t\in(L,U)\text{ for all
  } t\in(0,s)\bigr]\\
  &= \expect\sbrac[\big]{h(S_T) f(S_0,S_s) f(S_s,S_T) ; S_s,
    S_T\in(L,U)}\\
  &= \expect\sbrac[\big]{h(S_T) \pbrac[\big]{\ind_{(L,U)}(S_s) f(S_0,S_s)}
    \pbrac[\big]{\ind_{(L,U)}(S_T) f(S_s,S_T)}},
\end{align*}
where $\tau \defby \inf\{t\geq 0 : S_t \notin(L,U)\}$. Repeating this steps, one
obtains the following formula for the price of the continuously monitored double
barrier knock-out option
\begin{equation}
  \label{eq:71}
  c_{\text{ko}} = \e^{-rT}\expect\sbrac[\big]{h(S_T)\ind_{\tau > T}} =
  \e^{-rT}\expect\sbrac[\Big]{h(S_T)\prod_{n=0}^{N-1}\pbrac[\big]{
    \ind_{(L,U)}(S_{n+1})f(S_n,S_{n+1})}}.
\end{equation}
Let in the following $X_n\defby (S_n,S_{n+1})$, which forms a Markov chain due
to the Markov property of $S$ \citep{oksendal2003stochastic}. By
defining $G_n(X_n)\defby\ind_{(L,U)}(S_{n+1})f(S_n,S_{n+1})$ and $H(X_n) \defby
h(S_n)$, one can write \eqref{eq:71} as a telescoping product
\begin{align}
  c_{\text{ko}} &= \e^{-rT} \expect\sbrac[\Big]{H(X_N) \prod_{n=0}^{N-1}
    G_n(X_n)}\label{eq:72}\\
  &= \e^{-rT}\frac{\expect\sbrac[\big]{H(X_N) \prod_{n=0}^{N-1}
      G_n(X_n)}}{\expect\sbrac[\big]{\prod_{n=0}^{N-1}
      G_n(X_n)}}\cdots\frac{\expect\sbrac[\big]{G_1(X_1)
      G_0(X_0)}}{\expect\sbrac[\big]{G_0(X_0)}} \expect\sbrac[\big]
  {G_0(X_0)}\\
  &= \e^{-rT}\eta_N(H) \prod_{n=0}^{N-1} \eta_n(G_n)
\end{align}
where $\eta_n(F) \defby\expect\sbrac[\big]{F(X_n)\prod_{\ell =
    0}^{n-1}G_\ell(X_\ell)}/\expect\sbrac[\big]{\prod_{\ell =
    0}^{n-1}G_\ell(X_\ell)}$ under the convention
$\prod_{\ell\in\emptyset}G_\ell(X_\ell) = 1$. In the setting of particle methods
the expectation in \eqref{eq:72} is commonly referred to as a Feynman-Kac
expectation, the functions $G_n$ are called potentials and the random variables
$X_n$ are called particles. Thus, introducing $X_n$ does not only help to avoid
clutter, but also makes it easier to connect the formulas to a more general
theory. For an introduction to particle methods and their financial applications
the reader is referred to \citet{carmona2012introduction}.

Now define the random variables $\xi_n \defby \sbrac[\big]{\prod_{\ell =
    0}^{n-1}G_\ell(X_\ell)}/\expect\sbrac[\big]{\prod_{\ell =
    0}^{n-1}G_\ell(X_\ell)}$ and see that $\xi_n \geq 0$ and $\expect\xi_n =
1$. Then one can define a series of measures $\ProbQ_n$ by $\diff\ProbQ_n =
\xi_n\,\diff\Prob$ and so $\eta_n(F) = \expect\sbrac{F(X_n)\xi_n} =
\expect_{\ProbQ_n}F(X_n)$. Hence, by sampling $M$ values of $X_n$,
$X_n\pex{1},\ldots,X_n\pex{M}$, according to $\ProbQ_n$, one gets the estimator
$\frac{1}{M}\sum_{m=1}^MF(X_n\pex{m})$ of $\eta_n(F)$. This is essentially was
is done in the algorithm introduced below. However, one can not sample these
directly under $\ProbQ_n$. Instead the particles $X_n\pex{1},\ldots,X_n\pex{M}$
are sampled from an approximation to the density of $X_n$ under $\ProbQ_n$,
using an importance sampling technique called sequential importance
resampling. This technique plays an important role in some sequential Monte
Carlo methods, such as the bootstrap filter \citep{doucet2001sequential}.

\begin{rem}
  In the formulas for $d_{\text{ko}}$ and $c_{\text{ko}}$, one could of course
  instead of the interval $(L,U)$ have taken $(-\infty, B)$ or $(B,\infty)$, $B
  > 0$, getting formulas for up-and-out or down-and-out options, respectively.
\end{rem}

\begin{rem}
  These formulas make it easy to deal with the case when the barriers, interest
  rate and volatility are only piece-wise constant, and when the time steps are
  not equal in size. However, this setting will not be discussed further in this
  paper.
\end{rem}

\section{Monte Carlo estimators}
This section will present the crude Monte Carlo estimator and the sequential
Monte Carlo estimator for the option prices $d_{\text{ko}}$ and $c_{\text{ko}}$
given in~\eqref{eq:69} and \eqref{eq:71}. Both methods will rely on sampling
paths of $S$, $\hat{S} = (\hat{S}_0,\ldots,\hat{S}_N)$. This can be done either
using
\begin{equation}
  \label{eq:79}
  \hat{S}_n = \hat{S}_{n-1} \e^{\pbrac*{r-\half\sigma^2}\Delta t + \sigma\Delta W_n}
\end{equation}
where $\Delta t = T/N$ and $\Delta W_n = W_{t_{n+1}} - W_{t_n}\sim N(0,\Delta
t)$, or by using a discritization scheme on the stochastic differential equation
\eqref{eq:17} such as the Euler-Maruyama scheme,
\begin{equation}
  \label{eq:80}
  \hat{S}_n = \hat{S}_{n-1} + r\hat{S}_{n-1}\Delta t + \sigma\hat{S}_{n-1}\Delta W_n.
\end{equation}
The Euler-Maruyama scheme can be applied to more general models than the one
under consideration here, however, this comes at the cost of an extra error
term. In the implementation \eqref{eq:79} will be used instead, since in fact
$\hat{S}_n\disteq S_{t_n}^{t_{n-1},\hat{S}_{n-1}}$ where $\disteq$ means equal
in distribution.

\subsection{Crude Monte Carlo}
The unbiased crude Monte Carlo estimators $\hat{d}_{\text{ko}}^{\text{MC}}$ and
$\hat{c}_{\text{ko}}^{\text{MC}}$ are obtained directly from \eqref{eq:69} and
\eqref{eq:71} by sampling $M$ paths $S\pex{m} = (S_1\pex{m},\ldots,S_N\pex{m})$,
$m=1,\ldots,M$ and approximating the expectations by averages;
\begin{align}
  \hat{d}_{\text{ko}}^{\text{MC}} &= \e^{-rT}\frac{1}{M}\sum_{m=1}^M\pbrac[\Bigg]{
  h\pbrac[\big]{S_N\pex{m}}
  \prod_{n=0}^{N-1}\ind_{(L,U)}\pbrac[\big]{S_{n+1}\pex{m}}},\\
  \hat{c}_{\text{ko}}^{\text{MC}} &= \e^{-rT}\frac{1}{M}\sum_{m=1}^M\pbrac[\Bigg]{
  h\pbrac[\big]{S_N\pex{m}}
  \prod_{n=0}^{N-1}\pbrac[\Big]{\ind_{(L,U)}\pbrac[\big]{S_{n+1}\pex{m}} 
    f\pbrac[\big]{S_n\pex{m},S_{n+1}\pex{m}}}}.
\end{align}

\begin{figure}[t]
  \centering
  \tikzsetnextfilename{barrierhits}
%  \input{figures/barrierhits.tex}
  \caption{Number of barrier hits out of 200 sampled trajectories of the process
    $S$, with $S_0 = 50$, $r = 0.05$, $\sigma = 0.2$ over a period of length $T
    = 1$, in four different cases. The barriers are $50 \pm B$ for $B =
    5,10,15,20$.}
  \label{fig:barrier-hits}
\end{figure}

These estimators are very simple to implement. However, it is easy to realize
that in order to get a good estimate, one will need to take a very high number
of sample paths. This is due to the fact that if a barrier is breached for a
sampled trajectory, the payoff will be zero for that trajectory. As can be seen
in Figure~\ref{fig:barrier-hits}, the number of trajectories for which the
payoff is zero increases as the barriers get closer to each other.

When designing a Monte Carlo estimator it is not uncommon to want to change the
measure, in order to get more samples in a certain area, e.g. for estimating
$\expect(S_T - K)^+$ when $K \gg S_0$. This technique is known as importance
sampling, and it is in fact a variant of this method called sequential
importance resampling which will be the key step in the sequential Monte Carlo
algorithm.

\subsection{Sequential Monte Carlo}
In this section the sequential Monte Carlo algorithm developed in
\citet{shevchenko2014valuation} and the resulting estimators are presented. It
will be useful to consider particles $X_n\pex{m}$ with $X_0\pex{m} =
(S_0\pex{m},S_1\pex{m})$, which can be sampled using \eqref{eq:79}. As mentioned
above, the crucial step in the algorithm is the sequential importance resampling
step, which itself consists of two steps: the acceptance-rejection step, and the
resampling step.

In the acceptance-rejection step, each particle $X_n\pex{m}$ is either accepted
or rejected, with probability determined by $G_n(X_n\pex{m})$. In particular, a
particle $X_n\pex{m}$ will be accepted with probability $G_n(X_n\pex{m})$, and
rejected with probability $1-G_n(X_n\pex{m})$. This means that particles
$X_n\pex{m} = (S_n\pex{m}, S_{n+1}\pex{m})$ with a high probability of having
crossed the boundary over in the period $t_n$ to $t_{n+1}$ are more likely to be
rejected. Note that if $S_{n+1}\pex{m}\notin(L,U)$ then $G_n(X_n\pex{m}) = 0$,
and so the particle will be rejected.

In the resampling step the rejected particles are resampled from the discrete
distribution with density function $\sum_{m=1}^M w_n\pex{m}\delta_{X_n\pex{m}}$ where
the weights $w_n\pex{m}$ are given by
\begin{equation}
  \label{eq:61}
  w_n\pex{m} = \frac{G_n(X_n\pex{m})}{\sum_{k=1}^M G_n(X_n\pex{k})}.
\end{equation}
These newly sampled particles will be denoted by $\hat{X}_n\pex{m} =
(\hat{S}_n\pex{m},\hat{S}_{n+1}\pex{m})$ with $\hat{X}_n\pex{m} = X_n\pex{m}$
when $X_n\pex{m}$ was not rejected. Note again that if
$S_{n+1}\pex{m}\notin(L,U)$, then $X_n\pex{m}$ can not be picked from the
discrete distribution, to replace some other particle.

\begin{figure}[t]
  \centering
  \tikzsetnextfilename{resample}
  \input{figures/resample.tex}
  \caption{The evolution of six particles showing the sequential importance
    resampling step in action.}
  \label{fig:resample}
\end{figure}

Following these steps, $S_{n+2}\pex{m}$ is sampled using \eqref{eq:79} and one
takes $X_{n+1}\pex{m} = (\hat{S}_{n+1}\pex{m},S_{n+2}\pex{m})$. The sequential
importance resampling is illustrated in Figure~\ref{fig:resample} and the full
sequential Monte Carlo algorithm is presented in Algorithm~\ref{alg:smc}. By
taking $\tilde{G}_n(X_n) = \ind_{(L,U)}(S_{n+1})$ \eqref{eq:69} can be written
as a Feynman-Kac expectation
\begin{equation}
  \label{eq:73}
  d_{\text{ko}} = \e^{-rT}\expect\sbrac[\Big]{H(X_N)\prod_{n=0}^{N-1}\tilde{G}_n(X_n)}
\end{equation}
and the estimators $\hat{d}_{\text{ko}}^{\text{SMC}}$ and
$\hat{c}_{\text{ko}}^{\text{SMC}}$ are given by
\begin{align}
  \hat{d}_{\text{ko}}^{\text{SMC}} &= \e^{-rT}\prod_{n=0}^{N-1}
\pbrac[\Bigg]{\frac{1}{M}\sum_{m=1}^M\tilde{G}_n(X_n\pex{m})} \frac{1}{M}\sum_{m=1}^M
  H(X_N\pex{m})\\
  \hat{c}_{\text{ko}}^{\text{SMC}} &= \e^{-rT}\prod_{n=0}^{N-1}
\pbrac[\Bigg]{\frac{1}{M}\sum_{m=1}^MG_n(X_n\pex{m})} \frac{1}{M}\sum_{m=1}^M
  H(X_N\pex{m}).
\end{align}

Note that line~\ref{smc:line:weight} in Algorithm~\ref{alg:smc} implicitly assumes that not all of the
simulated paths cross the barriers at the same time. In this case, all of the
$w_m$'s will be zero, in which case it is reasonable to take the estimator to be
zero as well.

In line~\ref{smc:line:sample-discrete} one needs to sample from the probability
distribution mentioned above. This can be done easily by simulating a random
variable $Y\sim U[0,1]$ and taking $\hat{X}_n\pex{m} = X_n\pex{k}$, where $k$ is
the smallest number $1\leq k\leq M$ such that $Y\leq\sum_{\ell = 1}^k
w_\ell$. Alternatively \citep{shevchenko2014valuation} proposes a method for
simulating several of such random variables efficiently. This is the method used
in the code provided in Appendix~\ref{cha:code}.

\begin{rem}
  Algorithm~\ref{alg:smc} is generic enough to contain both types of barrier
  options, in both the discretely and the continuously monitored cases. For
  example, in the case of a discretely monitored up-and-out call option with
  barrier $B$, the algorithm can be adapted by taking $L = -\infty$, $U = B$ and
  $G_n = \tilde{G}_n$. The algorithm can even be adapted to the case of a
  European-type option, by taking $G_n \equiv 1$. In this case however, the
  algorithm just reduces to the standard Monte Carlo method.
\end{rem}

\begin{algorithm}
  \begin{algorithmic}[1]
    \Function{smc}{$M,N$}
    \State $\hat{S}_0\pex{m}\gets S_0$ for $m\gets 1,\ldots,M$.
    \For{$n \gets 0,\ldots,N-1$}
        \For{$m\gets 1,\ldots,M$}
            \State Sample $S_{n+1}\pex{m}$ using \eqref{eq:79}
            \State $X_n\pex{m} \gets (\hat{S}_n\pex{m}, S_{n+1}\pex{m})$
            \State Sample $U_n\pex{m}\sim U[0,1]$
            \State $G_n\pex{m} \gets G_n(X_n\pex{m})$\label{smc:line:barrier-breach}
        \EndFor
        \State $w_n\pex{m}\gets \frac{G_n\pex{m}}{\sum_{k=1}^M G_n\pex{k}}$ for
        $m\gets 1,\ldots,M$\label{smc:line:weight}
        \For{$m \gets 1,\ldots,M$}
            \If{$G_n\pex{m} < U_n\pex{m}$}
                \State Sample $\hat{X}_n\pex{m}$ from the discrete distribution
                with density
                $\sum_{k=1}^Mw_n\pex{k}\delta_{X_n\pex{k}}$ 
                \label{smc:line:sample-discrete}
            \Else
                \State $\hat{X}_n\pex{m} \gets X_n\pex{m}$
            \EndIf
        \EndFor
        \State $E_n \gets \frac{1}{M} \sum_{m=1}^M G_n\pex{m}$
    \EndFor
    \State $\hat{h} \gets \frac{1}{M} \sum_{m=1}^M
    H\pbrac[\big]{\hat{X}_N\pex{m}}$
    \State \Return{$\e^{-rT}\hat{h}\prod_{n=1}^NE_n$}
    \EndFunction
  \end{algorithmic}
  \caption{Sequential Monte Carlo algorithm}
  \label{alg:smc}
\end{algorithm}

\subsubsection*{Unbiasedness of the SMC estimator}
The goal of this section is to give a sketch of the proof that the sequential
Monte Carlo estimator is unbiased. First, define for $n\geq 0$ and $M\geq 1$ the
estimators
\begin{equation}
  \label{eq:74}
  \eta_n^M(H)\defby\frac{1}{M}\sum_{m=1}^MH(X_n\pex{m}),\quad \gamma_n^M(1) =
  \prod_{\ell = 0}^{n-1}\eta_\ell^M(G_\ell)\quad\text{and}\quad \gamma_n^M(H) =
  \gamma_n^M(1)\eta_n^M(H).
\end{equation}
Hence one can write $\hat{c} = \e^{-rT}\gamma_N^M(H)$, and so the goal is to
show that $\expect\gamma_N^M(H) = \gamma_N(H)$ where
\begin{equation}
  \label{eq:31}
  \gamma_n(H) = \expect\sbrac[\Big]{H(X_n)\prod_{\ell=0}^{n-1}G_\ell(X_\ell)},
\end{equation}
which will be shown by an induction argument. By convention
\begin{equation}
  \label{eq:76}
  \gamma_0^M(H) = \gamma_0^M(1)\eta_0^M(H) = \eta_0^M(H)
\end{equation}
and so $\expect \gamma_0^M(H) = \expect \eta_0^M(H)$. Hurthermore $\eta_0(H) =
\expect H(X_0)$ and $\expect\eta_0^M(H) = \expect H(X_0)$, hence
\begin{equation}
  \label{eq:77}
  \expect\gamma_0^M(H) = \expect\eta_0^M(H) = \eta_0(H) = \gamma_0(H).
\end{equation}
Assume now that $\expect\gamma_{n-1}^M(H) = \gamma_{n-1}(H)$ for some $n\geq 1$. Notice
that due to the resampling step in the algorithm,
\begin{align}
  \expect\pbrac[\big]{\eta_n^M(H) \bigm|
    \pbrac[\big]{X_0\pex{m},\ldots,X_{n-1}\pex{m}}_{m=1,\ldots,M}} &=
  \expect\pbrac[\big]{H(X_n) \bigm|
    \pbrac[\big]{X_0\pex{m},\ldots,X_{n-1}\pex{m}}_{m=1,\ldots,M}}\\
  &= \sum_{m=1}^M w_n\pex{m} \expect\pbrac[\big]{H(X_n) \bigm| X_{n-1} =
    X_{n-1}\pex{m}}
\end{align}
where the weights are given by
\begin{equation}
  \label{eq:75}
  w_n\pex{m} \defby
  \frac{G_{n-1}(X_{n-1}\pex{m})}{\sum_{k=1}^MG_{n-1}(X_{n-1}\pex{k})}.
\end{equation}
These will be 0 in case of a barrier breach. Observing that $w_n\pex{m} =
\frac{1}{M}\frac{1}{\eta_{n-1}(G_{n-1})}G_{n-1}(X_{n-1}\pex{m})$ and since
$\expect\pbrac[\big]{\gamma_n^M(1)\bigm|
  \pbrac[\big]{X_0\pex{m},\ldots,X_{n-1}\pex{m}}_{m=1,\ldots,M}} =
\gamma_n^M(1)$,
\begin{align}
  \MoveEqLeft\expect\pbrac[\big]{\gamma_n^M(H) \bigm|
    \pbrac[\big]{X_0\pex{m},\ldots,X_{n-1}\pex{m}}_{m=1,\ldots,M}}
  = \gamma_n^M(1) \sum_{m=1}^Mw_n\pex{m}\expect\pbrac[\big]{H(X_n)\bigm|
    X_{n-1} =
    X_{n-1}\pex{m}}\\
  &= \pbrac[\Bigg]{\prod_{\ell =
      0}^{n-1}\eta_\ell^M(G_\ell)}\frac{1}{M}\frac{1}{\eta_{n-1}(G_{n-1})}\sum_{m=1}^M
  G_{n-1}\pbrac[\big]{X_{n-1}\pex{m}} \expect\pbrac[\big]{H(X_n)\bigm| X_{n-1} =
    X_{n-1}\pex{m}}\\
  &= \pbrac[\Bigg]{\prod_{\ell = 0}^{n-2}\eta_\ell^M(G_\ell)} \frac{1}{M} \sum_{m=1}^M
  G_{n-1}\pbrac[\big]{X_{n-1}\pex{m}} \expect\pbrac[\big]{H(X_n)\bigm| X_{n-1} =
    X_{n-1}\pex{m}}\\
  &= \gamma_{n-1}^M(1) \eta_{n-1}(Q_n(H)) = \gamma_{n-1}^M(Q_n(H)),
\end{align}
where $Q_n(H)(x) = G_{n-1}(x)\expect\pbrac[\big]{H(X_n) \bigm| X_{n-1} =
  x}$. From the induction hypothesis it follows that
\begin{equation}
  \label{eq:78}
  \expect\gamma_n^M(H) =
  \expect\sbrac[\big]{\expect\pbrac[\big]{\gamma_n^M(H)
      \bigm|\pbrac[\big]{X_0\pex{m},\ldots,X_{n-1}\pex{m}}_{m=1,\ldots,M}}} 
  = \expect\gamma_{n-1}^M(Q_n(H)) = \gamma_{n-1}(Q_n(H)).
\end{equation}
Now all that is left to complete the proof is to show that $\gamma_n(H) =
\gamma_{n-1}(Q_n(H))$, which follows quite easily;
\begin{align}
  \gamma_n(H) &= \expect\sbrac[\Bigg]{H(X_n) \prod_{\ell =
      0}^{n-1}G_\ell(X_\ell)} =
  \expect\sbrac[\Bigg]{\expect\pbrac[\Bigg]{H(X_n)\prod_{\ell =
        0}^{n-1}G_\ell(X_\ell)\Bigm| \pbrac{X_0,\ldots,X_{n-1}}}}\\
  &= \expect\sbrac[\Bigg]{\expect\pbrac[\big]{H(X_n) \bigm|
      \pbrac{X_0,\ldots,X_{n-1}}}\prod_{\ell = 0}^{n-1} G_\ell(X_\ell)}\\
  &= \expect\sbrac[\Bigg]{G_{n-1}(X_{n-1})\expect\pbrac[\big]{H(X_n)\bigm|
      X_{n-1}}\prod_{\ell = 0}^{n-2}G_\ell(X_\ell)}\\
  &= \expect\sbrac[\Bigg]{Q_n(H)(X_{n-1})\prod_{\ell = 0}^{n-2}G_\ell(X_\ell)} =
  \gamma_{n-1}(Q_n(H)).
\end{align}

\section{Boundary crossing probabilities}
In order to be able to use Algorithm~\ref{alg:smc} it is of course necessary to
have a way of calculating the probability of crossing the boundary between to
points in time, as stated in \eqref{eq:70}. So consider $s$ and
$t$ with $0\leq s<t\leq T$ and assume that $S_s = x_1$ and $S_t = x_2$ are both
within the boundaries. In the case with only a single (upper or lower) barrier
$B$, this is given by
\begin{equation}
  \label{eq:34}
  f(x_1,x_2) = 1 -
  \exp\pbrac*{-2\frac{\log\pbrac{x_1/B}\log\pbrac{x_2/B}}{\sigma^2(t-s)}}.
\end{equation}
When there are two boundaries $L$ and $U$, the expression becomes more
complicated;
\begin{equation}
  \label{eq:35}
  f(x_1,x_2) = 1 - \sum_{k=1}^\infty \pbrac*{R(y, q k - a) + R(y, - q k + b) - R(y,
  q k) - R(y, -q k)}
\end{equation}
where $y = \log(x_2/x_1)$, $q = \log(U/L)$, $a = \log(x_1/L)$, $b = \log(U/x_1)$
and $R(y,z) = \exp\pbrac*{-2\frac{z(z-y)}{\sigma^2(t-s)}}$. These expressions
can be found in \citet{shevchenko2014valuation}.

\section{Remarks}
The method described in this chapter is very flexible and can be applied to a
great many cases. For instance, by dividing the interval $[0,T]$ into several
pieces, the interest rate, volatility and boundaries can be taken to be only
piecewise constant. By using the boundary crossing probabilities given in
\citet{kunitomo1992pricing}, it is even possible to price options with curved
boundaries.

Some of this flexibility is provided by the restatement of the price in terms of
Feynman-Kac expectations. In this way, it is even possible to have stochastic
interest rate, by including the interest rate in the potential $G$. For more
information see \citet{carmona2012introduction}.

The sequential Monte Carlo method is especially efficient for discretely
monitored options, when the boundary crossing probabilities does not have to be
calculated at each step, which adds an extra layer of complexity. Under other
models than the current, the boundary crossing probabilities might not have
analytical expressions. However, since they are not needed in the discrete case,
the method is easily adapted to a broader range of models, in which case one can
sample paths using for instance the Euler-Maruyama scheme \eqref{eq:80}.
