\documentclass[oneside,a4paper]{memoir}

%%% IMPORTANT %%%
%% Before final compilation:
% - Switch fixme to 'final'
% - If necessary remove '\listoffixmes'
% - Comment out showkeys
% - Check margins are correct
% - Make sure to write abstract

%%% PACKAGES %%%
%% utf8 encoding
\usepackage[utf8]{inputenc}

%% babel for hyphernation
\usepackage[english]{babel}

%% fontencoding
\usepackage[T1]{fontenc}

%% fix bugs in LaTeX2e
\usepackage{fixltx2e}

%% xcolor for colors
\usepackage{xcolor}

%% fixme for comments
\usepackage[draft]{fixme}
\fxsetup{layout=footnote,marginclue} % Write comments as footnotes

%% showkeys for label- and cite-keys
%\usepackage[notref, notcite]{showkeys}

%% lipsum for generating dummy-text
\usepackage{lipsum}

%% fancyref for smarter references
\let\fref\relax % relax memoir's \fref-command
\usepackage{fancyref}

%% geometry for setting margins
\usepackage{geometry}

%% graphicx for including images
\usepackage{graphicx}

%% adjustbox for vertically aligning graphics
\usepackage[export]{adjustbox}

%% mathpazo for palantino font
%\usepackage{mathpazo}

%% fourier for fourier font
\usepackage{fourier}

%% soul for text with more spacing
\usepackage{soul}

%% the natbib bibliography style
\usepackage{natbib}

%% essential AMS packages
\usepackage{amsmath, amssymb}

%% mathtools gives e.g. nice :=
\usepackage{mathtools}

%% fix bug involving mathtools and ntheorem with thmmarks, showing weird
%% equation numbers. See mathtools manual for info
\usepackage[overload,ntheorem]{empheq}

%% use ntheorem for theorem constructions
\usepackage[amsmath,thmmarks]{ntheorem}

%% blackboard bold font
\usepackage{bbm}

%% for cool figures
\usepackage{tikz, pgfplots}
\usepgfplotslibrary{groupplots}
\usepgfplotslibrary{external}  
%\tikzexternalize[prefix=/]

%% for creating inline-files with given content
\usepackage{filecontents}

%% for writing pseudo-code
\usepackage[chapter]{algorithm}
\usepackage{algpseudocode}

%% xspace helps writing "name"-macros
\usepackage{xspace}

%% for referencing and formatting URLs
\usepackage{url}

%% for individually numbered cases
\usepackage{cases}

%% for pretty-printing MATLAB code
\usepackage[numbered]{matlab-prettifier}
\lstset{%
  style=Matlab-Pyglike,
  basicstyle=\mlttfamily}

%% internal links. MUST BE LOADED LASTLY
\usepackage{hyperref}

%% for including external pdfs
\usepackage{pdfpages}

%%% MATH %%%
%% operators
\newcommand{\expect}{\ensuremath{\mathrm{\mathbf{E}}}}
%\DeclareMathOperator{\expect}{E} % Expectation
%\DeclareMathOperator{\var}{Var}  % Variance
\newcommand{\var}{\ensuremath{\mathrm{\mathbf{Var}}}}
\newcommand{\diff}[1]{\ensuremath{\mathrm{d}#1}} % differential operator
\newcommand{\Diff}[1]{\ensuremath{\mathrm{D}#1}} % capital letter diff operator
\newcommand{\Lop}[1]{\ensuremath{\mathrm{L}#1}}  % pde operator L
\newcommand{\Mop}[1]{\ensuremath{\mathrm{M}#1}}  % pde operator M
\DeclareMathOperator*{\Del}{\Delta}
\let\Re\relax
\DeclareMathOperator{\Re}{Re} % real part of complex number
\let\Im\relax
\DeclareMathOperator{\Im}{Im} % imaginary part of complex number

%% paired delimiters
% declared using the mathtools-package. This gives three versions:
%    \abs         :     the regular version
%    \abs*        :     preceded with \left and \right
%    \abs[\Bigg]  :     manually sets the size of the delimiters
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\sbrac}{[}{]}   % [ ]
\DeclarePairedDelimiter{\pbrac}{(}{)}   % ( )
\DeclarePairedDelimiter{\cbrac}{\{}{\}} % { }
% the following allows for \Set*{x \in X \given \frac{1}{x} < y}
\providecommand\given{}
\newcommand\SetSymbol[1][]{%
  \nonscript\:#1\vert
  \allowbreak
  \nonscript\:
  \mathopen{}}
\DeclarePairedDelimiterX\Set[1]\{\}{%
  \renewcommand\given{\SetSymbol[\delimsize]}
  #1
}

%% use nicer versions of some greek letters
\newcommand{\oldphi}{\phi}
\renewcommand{\phi}{\varphi}
\newcommand{\oldepsilon}{\epsilon}
\renewcommand{\epsilon}{\varepsilon}

%% environments
% Theorem
\newtheorem{thm}{Theorem}[chapter]

% Remark
\theoremstyle{nonumberplain}
\theorembodyfont{\normalfont}
\theoremsymbol{\ensuremath{\bullet}}
\theoremseparator{.}
\newtheorem{rem}{Remark}

% Proof
\theoremstyle{nonumberplain}
\theoremheaderfont{\normalfont\bfseries}
\theorembodyfont{\normalfont}
\theoremsymbol{\ensuremath{\square}}
\theoremseparator{.}
\newtheorem{proof}{Proof}

%% give := a new name
\newcommand{\defby}{\coloneqq}

%% equal in distribution
\newcommand{\disteq}{\stackrel{\scriptscriptstyle\smash{d}}{=}}

%% differentiation (ordinary and partial)
\newcommand{\od}[2][]{\frac{\diff{#1}}{\diff{#2}}}
\newcommand{\pd}[2][]{\frac{\partial #1}{\partial #2}}

%% sets
\newcommand{\reals}{\mathbbm{R}}
\newcommand{\nats}{\mathbbm{N}}

%% special functions
\newcommand{\ind}{\mathbbm{1}}
\newcommand{\Prob}{\ensuremath{\mathrm{\mathbf{P}}}}
\newcommand{\ProbQ}{\ensuremath{\mathrm{\mathbf{Q}}}}

%% Imaginary unit
\renewcommand{\i}{\ensuremath{\mathrm{i}}}

%% e = exp
\newcommand{\e}{\ensuremath{\mathrm{e}}}

%% one half
\newcommand{\half}{\frac{1}{2}}

%% caligraphic letters
\newcommand{\calF}{\mathcal{F}}

%% flow of information
\newcommand{\flowF}{\mathbbm{F}}

%% parenthesised exponent
\newcommand{\pex}[1]{^{(#1)}}

%%% OTHER COMMANDS %%%
%% Matlab
\newcommand{\Matlab}{\textsc{Matlab}\xspace}

%%% LAYOUT %%%
%% chapter style
\setlength\midchapskip{10pt}
\makechapterstyle{VZ23}{
  \renewcommand\chapternamenum{}
  \renewcommand\printchaptername{}
  \renewcommand\chapnumfont{\Huge\bfseries\centering}
  \renewcommand\chaptitlefont{\Huge\scshape\bfseries\centering}
  \renewcommand\afterchapternum{%
    \par\nobreak\vskip\midchapskip\hrule\vskip\midchapskip}
  \renewcommand\printchapternonum{\chaptitlefont}
}
\chapterstyle{VZ23}

%% page style
\copypagestyle{dissertation}{headings}
\makeevenhead{dissertation}{\leftmark}{}{}
\makeoddhead{dissertation}{}{}{\rightmark}
\makeevenfoot{dissertation}{}{\thepage}{}
\makeoddfoot{dissertation}{}{\thepage}{}
\makepsmarks{dissertation}{
  \createmark{chapter}{left}{shownumber}{}{. \ }
}
\nouppercaseheads
\pagestyle{dissertation}

%% only number chapters, sections and subsections
\setsecnumdepth{subsection}
\maxsecnumdepth{subsection}

%% only include chapters and sections in ToC
\settocdepth{section}

%% change color of refkeys to a dark green
\definecolor{refkey}{rgb}{0.0, 0.5, 0.0}
\definecolor{labelkey}{rgb}{0.0, 0.5, 0.0}

%% set minimum margins according to the program guide
%\geometry{left=3cm, top=1.5cm, right=1.5cm, bottom=2cm}

%% define soul-macro with specific text spacing for text on frontpage
\sodef\an{}{0.2em}{.9em plus.6em}{1em plus.1em minus.1em}
\newcommand\stext[1]{\an{\scshape#1}}

%% include only...
%\includeonly{background, fourier, smc, pde}
%\includeonly{pde}

%% only show referenced formulas
\mathtoolsset{showonlyrefs, showmanualtags}

\begin{document}

\include{frontpage}

\cleardoublepage

\frontmatter

\include{colophon}

\include{abstract}

\cleardoublepage

\include{acknowledgements}

\includepdf[pages=-]{own_work_declaration.pdf}

\cleardoublepage

\tableofcontents* % * for not including ToC in the ToC

%\listoffigures

%\listoftables

%\listoffixmes

% List of symbols/notation?

\mainmatter

\include{introduction} % Introduction
\include{fourier}      % Fourier approach               
\include{pde}          % PDE/Finite differences approach
\include{smc}          % Sequential Monte Carlo
\include{numeric}      % Numerical results

\appendix

\include{code}

\nocite{*}
\bibliographystyle{chicago}
\bibliography{bib}
% \printindex
\end{document}
