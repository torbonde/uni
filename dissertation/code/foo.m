function y = foo(x)
eps = 0.1;
U = 100;
K = 95;
y = zeros(size(x));
y = (U-eps-K)*(U-x)/eps;
I = x < U-eps;
y(I) = max(x(I)-K,0);