function y = normcdfc(x)
y = 1/2 + erf_(x/sqrt(2))/2;