% Samples from the weighted pdf given by Eq. (13) with weights p at x.
function Y = sampleweighted(n,p,x)
E = exprnd(1,n+1,1);
T = cumsum(E);
V = T./T(end);
k = 1;
r = 1;
[~, m] = size(x);
Y = zeros(n,m);
while r <= n
    while V(r) < sum(p(1:k)) && r <= n % Add last condition to ensure only n rows is returned
       Y(r,:) = x(k,:);
       r = r+1;
    end
    k = k+1;
end