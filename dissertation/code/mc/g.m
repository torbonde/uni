function p = g(Snm1,Sn,U,L,sigma,dt,M)

x = log(Sn./Snm1);
alpha = 2*log(U/L);
beta = 2*log(U./Snm1);
gamma = 2*log(Snm1./L);
R = @(z,x) exp(-z.*(z-2*x)/(2*sigma^2*dt));
q = 0;
for m = 1:M
    q = q + R(alpha*m-gamma,x) + R(-alpha*m+beta, x) - (R(alpha*m,x) + R(-alpha*m,x));
end
p = 1-q;