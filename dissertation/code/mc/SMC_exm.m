function Qcall = SMC_exm()
% Parameters
L = 90;
U = 110;
K = 100;
T = 0.5;
S0 = 100;
r = 0.1;
sigma = 0.3;
Ns = 16;%2.^(0:6);

M = 1000;

Qcall = zeros(length(Ns),1);
B = exp(-r*T);
hcall = @(x) max(x-K,0);

for i = 1:length(Ns)
    N = Ns(i);
    %fprintf('N: %f\n', N);
    dt = T/N;
    % Function g in Eq. (7)
    f = @(s1, s2) g(s1, s2, U, L, sigma, dt, 3);
    
    % Initialize
    dW = randn(M,N);
    G = zeros(M,N);
    
    % Initial step
    Snm1 = S0*ones(M,1);
    Sn = Snm1.*exp((r-sigma^2/2)*dt + sigma*sqrt(dt)*dW(:,1));
    X = [Snm1, Sn];
    
    % Accept/reject
    G(:,1) = (L < Sn & Sn < U).*f(Snm1, Sn);
    Uni = rand(M,1);
    reject = G(:,1) < Uni;
    
    % Recycle
    p = G(:,1)/sum(G(:,1));
    Y = sampleweighted(sum(reject),p,X);
    X(reject,:) = Y;
    
    for n = 2:N
        % Proposition
        Snm1 = X(:,2);
        Sn = Snm1.*exp((r-sigma^2/2)*dt + sigma*sqrt(dt)*dW(:,n));
        X = [Snm1, Sn];
        
        % Accept/reject
        G(:,n) = (L < Sn & Sn < U).*f(Snm1, Sn);
        Uni = rand(M,1);
        reject = G(:,n) < Uni;
        
        % Recycle
        p = G(:,n)/sum(G(:,n));
        Y = sampleweighted(sum(reject),p,X);
        X(reject,:) = Y;
    end
    
    G_avg = sum(G,1)/M;
    P = prod(G_avg);
    Scall = sum(hcall(X(:,2)))/M;
    Qcall(i) = B*P*Scall;
end