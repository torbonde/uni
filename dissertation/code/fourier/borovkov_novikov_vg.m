clear; clc;
% This script tests the results in the paper "On a new approach to
% calculating expectations for option pricing" by Borovkov and Novikov.

% Parameters
r = 0.05;
S0 = 100;
sigma = 0.2;
T = 0.5;
n = 13;
strikes = [100; 105; 110];

% Diffusion-model and variance gamma-model parameters
a_vg = 3;

% Functions
g = @(s,K,a) exp(1i*s*log(K)-a*log(K))./((a-1i*s).*(1+a-1i*s));
% normcdf_ = @(x) 0.5*(1+Faddeeva_erf(x/sqrt(2)));

% v = @(y,t) -mu*t./(sigma*sqrt(y));
% 
% integ1 = @(z,y,t) exp(z*mu*t)*exp(sigma^2*y*z^2/2).*normcdf_(sigma*z*sqrt(y)-v(y,t));
% integ2 = @(y,t) normcdf_(v(y,t));
% integ_vg = @(y,s,t) (integ1(s,y,t) + integ2(y,t)).*gampdf(y,t,1);
% integ_vgp = @(y,s,t) integ_vg(tan(y),s,t).*sec(y).^2;
% vg_mgf = @(z,t) arrayfun(@(s) quadcc(@(y) integ_vgp(y,s,t),0,pi/2,1e-6),z);
% 
% integ = @(z,y,t) (normcdf_(v(y,t)) ...
%     + exp(z*mu*t + 1/2*z^2*sigma^2*y).*normcdf_(z*sigma*sqrt(y)-v(y,t))).*gampdf(y,t,1);

mu = r+log(1-sigma^2/2);

x = @(y,t) y - mu*t;
d = @(t) -mu*t/sigma;

h = @(y,t) 2/(sigma*sqrt(2*pi)*gamma(t))*(x(y,t).^2/(2*sigma^2)).^(t/2-1/4).*besselk(t-1/2,x(y,t)*sqrt(2)/sigma);
vg_mgf = @(z,t) arrayfun(@(s) quadcc(@(y) (exp(s*(mu*t + sigma*y)).*(y > d(t)) + (y <= d(t))).*h(y,t),-Inf,Inf,1e-6),z);
%vg_mgf = @(z,t) arrayfun(@(s) quadgk(@(y) integ(s,y,t),0,Inf),z);

phi = spitzer_phi(n,vg_mgf,T);
P1 = zeros(3,1);
% Valuation
for i = 1:3
    tic;
    integrandp = @(s) phi(1+a_vg-1i*s).*g(s,strikes(i)/S0,a_vg);
    integrand = @(s) integrandp(tan(s)).*sec(s).^2;
    P1(i) = real(S0*exp(-r*T)*quadcc(integrand,-pi/2, pi/2,1e-6)/(2*pi)),
    toc,
end