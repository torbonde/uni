clear; clc;
% This script prices a single barrier up-and-out call option using the
% method described by Borovkov and Novikov.

% Parameters
U = 1.1;
K = 1;
T = 0.5;
%S0 = 100;
r = 0.1;
sigma = 0.3;

a = 9;

% Functions
g = @(s) exp(1i*s*log(K)-a*log(K))./((a-1i*s).*(1+a-1i*s));

% Standard normal cdf which accepts complex numbers
normcdf_ = @(x) 0.5*(Faddeeva_erfc(-x/sqrt(2)));

% mgf of (W_T, max W_t)
eta = @(u,v) 2*normcdf_(sqrt(T)*(u+v)).*exp(1/2*T*(u+v).^2).*(u+v)./(2*u+v) ...
    + normcdf_(sqrt(T)*v/2).*exp(1/8*T*v.^2).*(2*u.*exp(1/4*T*u.*(2*u+v))./(2*u+v) - 1);
% eta = @(u,v) normcdf_(sqrt(T)*(u+v)).*(sqrt(2*pi*T)*T*exp(1/2*T*(u+v).^2) ...
%         - u.*exp(T*u.^2/2)*2.*exp(1/2*T*v.*(2*u+v))./(2*u+v)) ...
%     + normcdf_(sqrt(T)*v/2).*(2*u.*exp(T*u.^2/2).*exp(1/8*T*v.*(2*u+v))./(2*u+v) ...
%         - T*sqrt(pi/2)*sqrt(T)*exp(1/8*T*v.^2));
   
% Needed mgf
theta = r/sigma - sigma/2;
psi = @(u,v) exp(-1/2*theta^2)*eta(u*sigma + theta, v*sigma);

P1 = borovkov_novikov_call(r, sigma, T, K, a);

% Distribution function from theorem
h = @(x) -x./(x.^2-1);
hp = @(x) (x.^2+1)./(x.^2-1).^2;
integrand = @(x,t,s) 1./t.*imag(exp(-1i*t*x).*psi(1+a-1i*s, 1i*t).*g(s));
W = @(x) 1/2 - 1/(2*pi^2*P1)*integral2(@(t,s) integrand(x,t,s),0,Inf,-Inf,Inf);
%W = @(x) 1/2 - 1/(2*pi^2*P1)*integral2(@(t,s) integrand(x,h(t),h(s)).*hp(t).*hp(s),0,1,-1,1);
P2 = P1*W(log(U));
osx_notify(sprintf('Result: %f', P2));