clear; clc;
% This script prices a double barrier knock-out call option using the
% method described by Borovkov and Novikov.

% Parameters
L = 90;
U = 110;
K = 100;
T = 0.5;
S0 = 100;
r = 0.1;
sigma = 0.3;

a = 9;

% Functions
g = @(s,K,a) exp(1i*s*log(K)-a*log(K))./((a-1i*s).*(1+a-1i*s));

