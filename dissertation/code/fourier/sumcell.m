function s = sumcell(c)

s = 0;
for i = 1:length(c)
   s = s + c{i}; 
end