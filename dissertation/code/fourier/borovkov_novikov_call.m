% This does not take underlying or discounting into account!!!
function P = borovkov_novikov_call(r, sigma, T, K, a)

g = @(s,K,a) exp(1i*s*log(K)-a*log(K))./((a-1i*s).*(1+a-1i*s));
phi = @(z) exp(z*(r-sigma^2/2)*T + z.^2*T*sigma^2/2);
integrandp = @(s) phi(1+a-1i*s).*g(s,K,a);
integrand = @(s) integrandp(tan(s)).*sec(s).^2;
P = real(quadcc(integrand,-pi/2, pi/2,1e-6)/(2*pi));