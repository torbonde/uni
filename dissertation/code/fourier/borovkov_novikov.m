clear; clc;
% This script tests the results in the paper "On a new approach to
% calculating expectations for option pricing" by Borovkov and Novikov.

% Parameters
r = 0.05;
S0 = 100;
sigma = 0.2;
T = 0.5;
n = 13;
strikes = [100; 105; 110];

% Diffusion-model and variance gamma-model parameters
a_diff = 9;
a_vg = 3;

% Functions
g = @(s,K,a) exp(1i*s*log(K)-a*log(K))./((a-1i*s).*(1+a-1i*s));
v = sigma/2 - r/sigma;

normcdf_ = @(x) 0.5*(Faddeeva_erfc(-x/sqrt(2)));

diff_mgf = @(z,t) normcdf_(v*sqrt(t)) ...
    + exp(-z*t.*(sigma^2/2 - r - z*sigma^2/2)).*normcdf_((z*sigma-v)*sqrt(t));

diff = @(z,t) exp(z*(r-sigma^2/2)*t)./(1-z.^2*sigma^2/2).^t;

phi = spitzer_phi(n,diff_mgf,T);

% Valuation
for i = 1:3
    tic;
    integrandp = @(s) phi(1+a_diff-1i*s).*g(s,strikes(i)/S0,a_diff);
    integrand = @(s) integrandp(tan(s)).*sec(s).^2;
    P1(i) = real(S0*exp(-r*T)*quadcc(integrand,-pi/2, pi/2,1e-6)/(2*pi)),
    toc,
end