function phi = spitzer_phi(n, mgf, T)

ts = (0:n)*T/n;
phis = cell(n+1,1);
phis{1} = @(z) ones(size(z));
for m = 1:n
    phis{m+1} = @(z) sumcell(arrayfun(@(k) ...
        phis{k}(z).*mgf(z,ts(m+2-k)),1:m,'UniformOutput',false))/m;
end
phi = phis{end};