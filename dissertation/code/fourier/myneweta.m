function e = myneweta(u,v)
T = 1;
normcdf_ = @(x) 0.5*(Faddeeva_erfc(-x/sqrt(2)));

Phi1 = normcdf_(sqrt(T)*(u+v));
Phi2 = normcdf_(sqrt(T)*v/2);

a = sqrt(2*pi*T)*T*exp(1/2*T*(u+v).^2);
b = u.*exp(T*u.^2/2)*2.*exp(1/2*T*v.*(2*u+v))./(2*u+v);
c = 2*u.*exp(T*u.^2/2).*exp(1/8*T*v.*(2*u+v))./(2*u+v);
d = T*sqrt(pi/2)*sqrt(T)*exp(1/8*T*v.^2);
IA = (a-b) == 0;
A = Phi1.*(a-b);
A(IA) = 0;
IB = (c-d) == 0;
B = Phi2.*(c-d);
B(IB) = 0;
e = A+B;