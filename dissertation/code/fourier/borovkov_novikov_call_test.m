% This script tests the results in the paper "On a new approach to
% calculating expectations for option pricing" by Borovkov and Novikov.

% Parameters
r = 0.1;
S0 = 100;
sigma = 0.3;
T = 0.5;
n = 13;
strikes = [100; 105; 110];

% Diffusion-model and variance gamma-model parameters
a_diff = 9;

% Functions
g = @(s,K,a) exp(1i*s*log(K)-a*log(K))./((a-1i*s).*(1+a-1i*s));
v = sigma/2 - r/sigma;
phi = @(z) exp(z*(r-sigma^2/2)*T + z.^2*T*sigma^2/2);
d1 = @(K) (log(S0/K) + (r+sigma^2/2)*T)/(sigma*sqrt(T));
d2 = @(K) d1(K) - sigma*sqrt(T);

P1 = zeros(3,1);
P2 = zeros(3,1);
% Valuation
for i = 1:3
    tic;
    integrandp = @(s) phi(1+a_diff-1i*s).*g(s,strikes(i)/S0,a_diff);
    integrand = @(s) integrandp(tan(s)).*sec(s).^2;
    P1(i) = real(S0*exp(-r*T)*quadcc(integrand,-pi/2, pi/2,1e-6)/(2*pi));
    toc,
    K = strikes(i);
    P2(i) = normcdf(d1(K))*S0 - normcdf(d2(K))*K*exp(-r*T);
    fprintf('BN: %d\t Actual: %d\n', P1(i), P2(i));
end
