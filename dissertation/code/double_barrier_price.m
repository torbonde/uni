function p = double_barrier_price(model, N)
c1 = 2*model.r/model.sigma^2 + 1;
d1 = @(n) (log(model.S0*model.U.^(2*n)./(model.K*model.L.^(2*n))) + (model.r + model.sigma^2/2)*model.T)/(model.sigma*sqrt(model.T));
d2 = @(n) (log(model.S0*model.U.^(2*n-1)./(model.L.^(2*n))) + (model.r + model.sigma^2/2)*model.T)/(model.sigma*sqrt(model.T));
d3 = @(n) (log(model.L.^(2*n+2)./(model.K*model.S0*model.U.^(2*n))) + (model.r + model.sigma^2/2)*model.T)/(model.sigma*sqrt(model.T));
d4 = @(n) (log(model.L.^(2*n+2)./(model.S0*model.U.^(2*n+1))) + (model.r + model.sigma^2/2)*model.T)/(model.sigma*sqrt(model.T));
c = @(n) model.S0*((model.U.^n./model.L.^n).^c1.*(normcdf(d1(n)) - normcdf(d2(n))) ...
    - (model.L.^(n+1)./(model.U.^n*model.S0)).^c1.*(normcdf(d3(n)) - normcdf(d4(n)))) ...
    - model.K*exp(-model.r*model.T)*((model.U.^n./model.L.^n).^(c1-2).*(normcdf(d1(n) - model.sigma*sqrt(model.T)) - normcdf(d2(n) - model.sigma*sqrt(model.T))) ...
    - (model.L.^(n+1)./(model.U.^n*model.S0)).^(c1-2).*(normcdf(d3(n) - model.sigma*sqrt(model.T)) - normcdf(d4(n) - model.sigma*sqrt(model.T))));
p = sum(c(-N:N));
    