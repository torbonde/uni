#r @"packages/Meta.Numerics/lib/Meta.Numerics.dll"

open Meta.Numerics
open Meta.Numerics.Analysis

// Parameters
let r = 0.05
let S0 = 100.
let sigma = 0.2
let T = 0.5
let n = 13.
let strikes = [100; 105; 110]

let a = 3.;

// Functions
let g (s: float) (K: float) (a: float) =
  let i = ComplexMath.I
  (ComplexMath.Exp i*s*(log K)) - a*(log K)
