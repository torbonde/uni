% Parameters
L = 90;
U = 110;
K = 100;
T = 0.5;
S0 = 100;
r = 0.1;
sigma = 0.3;

% Payoff
h = @(x) max(x-K,0);

% Method specific
n = 10; % time dimension
m = 10; %ceil((U-L)*sqrt(n)/sqrt(T));
%m = 1000; % space dimension

dt = T/n;
dx = (U-L)/m;

xs = L:dx:U; % x values
ts = 0:dt:T; % t values

a = @(j) dt*(r*xs(j)/(2*dx) - 1/2*sigma^2*xs(j).^2/dx^2);
b = @(j) 1 + r*dt + sigma^2*xs(j).^2*dt/dx^2;
c = @(j) -dt*(r*xs(j)/(2*dx) + 1/2*sigma^2*xs(j).^2/dx^2);

% Initialize solution matrix
V = zeros(m+1,n+1);
V(2:end-1, end) = h(xs(2:end-1));

D = diag(b(1:m-1));
A = diag(a(2:m-1));
C = diag(c(1:m-2));

D(2:m-1,1:m-2) = D(2:m-1,1:m-2) + A;
D(1:m-2,2:m-1) = D(1:m-2,2:m-1) + C;

for i = n:-1:1
    % Solve system of equations
    V(2:end-1,i) = D\V(2:end-1,i+1);
end
osx_notify('Done');