function [P, V, xs, ts] = barrier_log_pde_solve(model, N, M)

dt = model.T/N;
dx = (log(model.U) - log(model.L))/M;

xs = log(model.L):dx:log(model.U);
ts = 0:dt:model.T;

theta = model.r - 1/2*model.sigma^2;

a = dt*(theta - abs(theta))/(2*dx) - model.sigma^2*dt/(2*dx^2);
b = 1 + model.sigma^2*dt/dx^2 + abs(theta)*dt/dx + model.r*dt;
c = -dt*(theta + abs(theta))/(2*dx) - model.sigma^2*dt/(2*dx^2);

V = zeros(M+1,N+1);
V(2:end-1, end) = model.payoff(exp(xs(2:end-1)));

D = zeros(M-1);
D(2:M:end) = a;
D(1:M:end) = b;
D(M:M:end) = c;

for n = N:-1:1
    V(2:end-1, n) = D\V(2:end-1, n+1);
end

P = interp1(xs, V(:, 1), log(model.S0));