function [P, V, xs, ts] = barrier_pde_solve(model, N, M)

dt = model.T/N;
dx = (model.U - model.L)/M;

xs = model.L:dx:model.U;
ts = 0:dt:model.T;

a = @(x) dt*(model.r*x/(2*dx) - model.sigma^2*x.^2/(2*dx^2));
b = @(x) 1 + model.r*dt + model.sigma^2*x.^2*dt/dx^2;
c = @(x) -dt*(model.r*x/(2*dx) + model.sigma^2*x.^2/(2*dx^2));

V = zeros(M+1,N+1);
V(2:end-1, end) = model.payoff(xs(2:end-1));

D = zeros(M-1);
D(2:M:end) = a(xs(2:M-1));
D(1:M:end) = b(xs(1:M-1));
D(M:M:end) = c(xs(1:M-2));

for n = N:-1:1
    % Solve systems of equations
    V(2:end-1, n) = D\V(2:end-1, n+1);
end

P = interp1(xs, V(:,1), model.S0);