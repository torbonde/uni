% Simulate a CIR process X using the drift-implicit Milstein scheme
function X = milstein_scheme(alpha, mu, sigma, X0, varargin)
% The varargin-argument is assumed to either contain the a simulated
% Brownian motion W, or M and N determining the number of trajectories of X
% and the number of points in which X is simulated.
if length(varargin) == 1
    W = varargin{1};
    [~, pts] = size(W);
    N = log2(pts-1); % pts == 2^N+1
else
    M = varargin{1};
    N = varargin{2};
    W = brownian(M, N);
end

% Preallocate X
X = zeros(size(W));

X(:,1) = X0;
for i = 1:2^N
    dW = W(:,i+1) - W(:,i);
    X(:,i+1) = (X(:,i) + alpha*mu/2^N ...
        + sigma*sqrt(X(:,i)).*dW ...
        + sigma^2/4*(dW.^2 - 1/2^N))/(1+alpha/2^N);
end