% Simulate M trajectories of a Brownian motion in 2^N+1 points, using a
% Brownian bridge construction.
function W = brownian(M, N)
% Having M rows will give a speed-up when picking out all values from past
% trial, since matrices are stored column-wise.
W = zeros(M,2^N+1);

% Initialize vector of indices from 'last' step and W
Inm1 = [1; 2^N+1];
W(:,1) = 0;
W(:,end) = randn(M,1);

for n = 1:N
    % Find indices in which to simulate W
    In = linspace(1,2^N+1,2^n+1);
    % Disregard the indices in which W has already been simulated
    I = setdiff(In,Inm1);
    
    % Perform the moving average (column-wise) as a convolution. This is
    % slightly faster than the na�ve method using something a la
    %       (W(:,Inm1(2:end)) + W(:,Inm1(1:end-1)))/2.
    W(:,I) = conv2(W(:,Inm1),[0.5, 0.5],'valid') ...
        + sqrt(2^(-n-1))*randn(M,length(I));
    Inm1 = In;
end