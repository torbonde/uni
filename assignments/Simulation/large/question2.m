clear; clc; close all; rng(100);
%% Choose parameters
sigma = 0.1;
X0 = 10;
r = 0.03;
M = 300000;
N = 10;

%% Calculate values of Km explicitly
% E[X(1)] = X0*exp(r)
E = X0*exp(r);

% Var[X(1)] = X0^2*exp(2*mu)*(exp(sigma^2)-1)
Var = X0^2*exp(2*r)*(exp(sigma^2)-1);

% Preallocate K
K = zeros(5,1);

fprintf('  m\t K_m\n');
for m = 1:5
    K(m) = E + m*Var;
    fprintf('  %d\t %f\n', m, K(m));
end

%% Calculate price analyticallt
d = @(m) (log(X0/K(m))+r-0.5*sigma^2)/sigma;

% Preallocate Price
Price = zeros(5,1);
fprintf('  m\t Analytical price\n');
for m = 1:5
    Price(m) = exp(-r)*normcdf(d(m));
    fprintf('  %d\t %e\n', m, Price(m));
end

%% Estimate price by plain Monte Carlo
% Simulate trajectories
X = euler_scheme(r, sigma, X0, M, N);

% Preallocate vectors for prices and variances.
Price_mc = zeros(5,1);
Var_mc = zeros(5,1);
fprintf('  m\t MC/Euler price\t MC variance\n');
for m = 1:5
    % Find entries where X > Km, i.e. find payoff for each trajectory.
    XKm = X(:,end) > K(m);
    
    % Estimate price as discounted expectation
    Price_mc(m) = exp(-r)*sum(XKm)/M;
    
    % Monte Carlo variance
    Var_mc(m) = sum((exp(-r)*XKm).^2)/M - Price_mc(m).^2;
    fprintf('  %d\t %e\t %e\n', m, Price_mc(m),Var_mc(m)); 
end

%% Estimate price by Monte Carlo with importance sampling
% Smaller value of M still gives smaller variance.
M = 100000;

% Simulate M trajectories of the Brownian motion W
W = brownian(M, N);

% Matrix of increments to be added. This corresponds to adding a certain
% drift to each dW.
dt = 1/2^N;
incs = dt*(cumsum(ones(size(W)),2)-1);

% Preallocate vectors for prices and variances.
Price_is = zeros(5,1);
Var_is = zeros(5,1);
fprintf('  m\t MC/Imp. price\t MC/Imp. variance\n');
for m = 1:5
    % Choose drift
    mu = (log(K(m)/X0) - r)/sigma;
    
    % Set Wtilde to W + drift
    Wtilde = incs*mu + W;
    
    % Simulate Y with Wtilde instead of W
    Y = euler_scheme(r, sigma, X0, Wtilde);
    
    % Radon-Nikodym derivative
    R = @(x) exp((mu*(sigma*(mu-sigma) + 2*r - 2*log(x/X0)))/(2*sigma));
    
    % Payoff function
    h = @(x) x > K(m);
    
    % Estimate price as discounted expectation
    Eg = sum(h(Y(:,end)).*R(Y(:,end)))/M;
    Price_is(m) = exp(-r)*Eg;
    
    % Second moment of estimator
    Eg2 = sum(h(Y(:,end)).*R(Y(:,end)).^2)/M;
    
    % Monte Carlo variance
    Var_is(m) = Eg2 - Eg^2;
    
    fprintf('  %d\t %e\t %e\n', m, Price_is(m),Var_is(m));
end