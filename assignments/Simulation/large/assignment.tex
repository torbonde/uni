\documentclass[a4paper]{article}
\input{../../../preamble}
\newcommand\DistEq{\stackrel{\scriptscriptstyle\smash{d}}{=}}
\usepackage{natbib}
\usepackage[numbered]{matlab-prettifier}
\lstset{
  style              = Matlab-editor,
  basicstyle         = \mlttfamily,
  escapechar         = ",
  mlshowsectionrules = true,
  numberstyle=\tiny,
}
\begin{document}
\title{Simulation assignment}
\author{David Tor Bonde - 1469023}
\maketitle

The Matlab codes used for the following questions are listed in the section
Code. For Question 1, the file \texttt{question1.m} was used, and likewise for
Question 2 the file \texttt{question2.m}. These rely on the helper-files
\texttt{brownian.m}, \texttt{euler\_scheme.m} and \texttt{milstein\_scheme.m}.

\section*{Question 1}
In the following, I will consider the Cox-Ingersoll-Ross model in which the
short rate $r$ is described by the SDE
\begin{equation}
  \label{eq:8}
  \begin{cases}
    \diff r(t) = \alpha(\mu - r(t))\,\diff t + \sigma\sqrt{r(t)}\,\diff W(t)\\
    r(0) = r_0
  \end{cases}
\end{equation}
where $W$ is a Brownian motion and $r_0,\alpha,\mu,\sigma>0$ are
constants. Under this model, the short rate is guaranteed to be positive if
$\alpha\mu\geq\half\sigma^2$.

In order to approximate values $\tilde{r}\pex{N}_j(t_i)$, $i=0,\ldots,2^N$,
$j=1,\ldots,M$ of $M$ independent trajectories of $r$ in the points $t_i =
\frac{i}{2^N}$ I applied the drift-implicit Milstein scheme
\begin{align}
  \tilde{r}\pex{N}_j(t_{i+1}) ={}&\tilde{r}\pex{N}_j(t_i) + \alpha(\mu -
  \tilde{r}\pex{N}_j(t_{i+1}))\frac{1}{2^N} +
  \sigma\sqrt{\tilde{r}\pex{N}_j(t_i)}\bigl(\tilde{W}^j(t_{i+1}) - \tilde{W}^j(t_i)\bigr)\\
  &+ \frac{1}{4}\sigma^2\biggl(\bigl(\tilde{W}^j(t_{i+1}) -
  \tilde{W}^j(t_i)\bigr)^2 -
  \frac{1}{2^N}\biggr)
\end{align}
with $\tilde{r}\pex{N}_j(0) = r_0$, where the $\tilde{W}^j$'s are $M$
independent discrete-time approximations to the Brownian motion $W$, generated
using a Brownian bridge construction as described below. This of course requires
the equation to be rearranged so that $\tilde{r}_j\pex{N}(t_{i+1})$ only appears
on the left-hand side.

\begin{figure}
  \centering
  \includegraphics[width=11cm]{cir}
  \caption{$M=100$ trajectories of the simulated Cox-Ingersoll-Ross process with
    $N = 12$.}
  \label{fig:cir}
\end{figure}

For my simulation I chose $\alpha = 0.2$, $\mu = 0.05$, $\sigma = 0.1$ and $r_0
= 0.04$. These parameters can easily be seen to satisfy the condition for
positivity of the solution to \eqref{eq:8}, and as can be seen in Figure
\ref{fig:cir}, the simulated trajectories using the drift-implicit Milstein
scheme are all positive.

\begin{table}[b]
  \centering
  \begin{tabular}{lrr}
    $N$ & $e_w\pex{N}$         & $e_s\pex{N}$          \\
    1 & $1.536\cdot 10^{-04}$ & $5.490\cdot 10^{-04}$ \\
    2 & $1.300\cdot 10^{-04}$ & $3.890\cdot 10^{-04}$ \\
    3 & $7.972\cdot 10^{-05}$ & $2.198\cdot 10^{-04}$ \\
    4 & $4.389\cdot 10^{-05}$ & $1.170\cdot 10^{-04}$ \\
    5 & $2.178\cdot 10^{-05}$ & $5.943\cdot 10^{-05}$ \\
    6 & $1.090\cdot 10^{-05}$ & $2.936\cdot 10^{-05}$ \\
    7 & $5.529\cdot 10^{-06}$ & $1.503\cdot 10^{-05}$ \\
    8 & $2.722\cdot 10^{-06}$ & $7.501\cdot 10^{-06}$
  \end{tabular}
  \caption{Estimated weak and strong errors.}
  \label{tab:errors}
\end{table}

With these parameters, $N=16$ and $M=2000$, I estimated $K = \expect[r(1)]$ by
$\bar{K} = \frac{1}{M}\sum_{j=1}^M \tilde{r}_j\pex{N}(1) \approx 0.0426$, which
was used as the strike price in a European call option with payoff $F(x) =
(x-\bar{K})^+$. I could of course have calculated $K$ explicitly by $K =
r_0e^{-\alpha} + \mu(1-e^{-\alpha})\approx 0.0418$, but where is the fun in
that? Using the payoff function $F$ I approximated the weak and strong errors
with the following estimators
\begin{equation}
  \label{eq:14}
  e_w\pex{N} = \Bigl\vert\frac{1}{M}\sum_{j=1}^M\Bigl(F\bigl(\tilde{r}\pex{N}_j(1)\bigr) -
    F\bigl(\tilde{r}\pex{2N}_j(1)\bigr)\Bigr)\Bigr\vert \quad\text{and}\quad e_s\pex{N} = \Bigl(\frac{1}{M}\sum_{j=1}^M\bigl\vert\tilde{r}\pex{N}(1) - \tilde{r}\pex{2N}(1)\bigr\vert^2\Bigr)^\half
\end{equation}
for $N = 1,\ldots,8$ and $M = 2000$. The values so obtained can be seen in Table
\ref{tab:errors}. Furthermore, the weak and strong convergence rates were
estimated using a simple linear regression of $\log e_w\pex{N}$ and $\log
e_s\pex{N}$ against $\Delta t = \frac{1}{2^N}$, giving weak and strong
convergence rates $q_w = 0.8706$ and $q_s = 0.9109$ with residuals of $0.5040$
and $0.3182$, respectively. The error rates are plotted in
Figure~\ref{fig:errors} along with lines with slope $q_w$ and $q_s$,
respectively, for reference.

\begin{figure}[t]
  \centering
  \includegraphics[width=12cm]{errors}
  \caption{Estimated weak errors (left) and strong errors (right) for time steps
  $\Delta t = \frac{1}{2^N}$, $N=1,\ldots,8$. The slopes of the dashed lines,
  0.8706 and 0.9109, are the estimated weak and strong orders of convergence.}
  \label{fig:errors}
\end{figure}

\subsection*{The Brownian bridge construction}
One way of simulating trajectories of a Brownian motion $(W(t))_{t\in[0,1]}$ at
the points $t_i = \frac{i}{2^N}$, $i=0,\ldots,2^N$ is by the Brownian bridge
construction. The correctness of this method relies on the fact that for $0\leq
s<t<u\leq 1$,
\begin{equation}
  \label{eq:10}
  L(W(t) | W(s) = x, W(u) = y) = N\Bigl(x + \frac{t-s}{u-s}(y-x),
  \frac{(t-s)(u-t)}{u-s}\Bigr).
\end{equation}
In particular, the law of $W$ in a point half-way between two points $s<u$ is
given by
\begin{equation}
  \label{eq:11}
    L\Bigl(W\Bigl(\frac{s+u}{2}\Bigr)\bigm|W(s) = x, W(u) = y\Bigr) = N\Bigl(\frac{x+y}{2},
  \frac{u-s}{4}\Bigr).
\end{equation}
Using this fact, $W$ can be simulated in the points $t_i$ for some $N$,
recursively in $N$. So let for each $n\geq 0$ the set $Q_n =
\Bigl\{\frac{i}{2^n} : i=0,\ldots,2^n\Bigr\}$ denote the set of points in which
to simulate $W$ in the $n$th step of the recursive procedure, and observe that
$Q_n\subset Q_{n+1}$ for each $n\geq 0$. Let $W\pex{n}$ denote the points of $W$
simulated in the $nth$ step. The target is then to simulate $W$ in
the points $q\in Q_N$, and so the output of the algorithm will be
$W\pex{N}$. This is obtained by letting $W\pex{0}(0) = 0$ and $W\pex{0}(1) = Z$
for $Z\sim N(0,1)$, and for $n\geq 1$ and each $q\in Q_n$
\begin{equation}
  \label{eq:12}
    W\pex{n}(q) =
  \begin{cases}
    W\pex{n-1}(q) & \text{if } q\in W_{n-1}\\
    \frac{W\pex{n-1}(q-2^{-n}) + W\pex{n-1}(q+2^{-n})}{2} + \sqrt{2^{-n-1}}Z &
    \text{else, where } Z\sim N(0,1)
  \end{cases}
\end{equation}
Note that for each $n$ and each point $q$ a new value of $Z$ is to be simulated.

\section*{Question 2}
Let $r,\sigma,X_0>0$ be constants and let $X$ be given by the SDE
\begin{equation}
  \label{eq:1}
  \begin{cases}
    \diff X(t) = r X(t)\,\diff t + \sigma X(t)\,\diff W(t)\\
    X(0) = X_0
  \end{cases}
\end{equation}
where $W$ is a Brownian motion under the risk-neutral measure
$\probQ$. Furthermore, for $m=1,\ldots,5$ let $K_m = \expect_\probQ[X(1)] +
m\Var_\probQ[X(1)]$ and let $F_m(x) = \ind_{\{x>K_m\}}$ denote the payoff at
time $T=1$ of a digital option on $X$. By the Main Theorem of Pricing European
Options, the price of this option at time $t=0$ is given by
\begin{equation}
  \label{eq:2}
  V_m = e^{-r}\expect_\probQ[F_m(X(1))] = e^{-r}\probQ(X(1) > K_m).
\end{equation}
Note that when $X$ is given by \eqref{eq:1}, $X(t) = X_0 e^{(r-\half\sigma^2)t+\sigma W(t)}$, so
\begin{align}
  [X(1) > K_m] &= \bigl[X_0e^{r-\half\sigma^2+\sigma W(1)} > K_m\bigr]\\
  &= \Bigl[\ln X_0 + r - \half\sigma^2 + \sigma W(1) > \ln K_m\Bigr]\\
  &= \Biggl[-W(1) < \frac{\ln\frac{X_0}{K_m} + r - \half\sigma^2}{\sigma}\Biggr]
\end{align}
and observe that $W(1)\DistEq -W(1) \sim N(0,1)$ under $\probQ$. So by the above
and \eqref{eq:2}, the explicit formula for the price $V_m$ of the digital option
with payoff $F_m$ is
\begin{align}
  V_m = e^{-r}\probQ(W(1) < d_m) = e^{-r}\Phi(d_m),
\end{align}
where $\Phi$ denotes the standard normal cumulative distribution function, and
where $d_m$ is given by
\begin{equation}
  \label{eq:3}\noeqref{eq:3}
  d_m = \frac{\ln\frac{X_0}{K_m} + r - \half\sigma^2}{\sigma}.
\end{equation}

\begin{table}[b]
  \centering
  \begin{tabular}{lrr}
    $m$ & $K_m$ & Analytical price\\\midrule
    1 & $11.372$ & $1.458\cdot 10^{-01}$ \\ 
    2 & $12.439$ & $2.587\cdot 10^{-02}$ \\
    3 & $13.506$ & $2.843\cdot 10^{-03}$ \\
    4 & $14.573$ & $2.126\cdot 10^{-04}$ \\
    5 & $15.640$ & $1.171\cdot 10^{-05}$ 
  \end{tabular}
  \caption{Analytically calculated prices of the digtal options with strike
    prices $K_m$.}
  \label{tab:analyt}
\end{table}

To simulate values $\tilde{X}_j\pex{N}(t_i)$, $i=0,\ldots,2^N$, $j=1,\ldots,M$
of $M$ trajectories of $X$ in the points $t_i = \frac{i}{2^N}$ I applied the
implicit Euler scheme
\begin{equation}
  \label{eq:13}
  \tilde{X}_j\pex{N}(t_{i+1}) = \tilde{X}_j\pex{N}(t_i) + r
  \tilde{X}_j\pex{N}(t_{i+1})\frac{1}{2^N} +
  \sigma\tilde{X}\pex{N}_j(t_i)\bigl(\tilde{W}^j(t_{i+1}) -
  \tilde{W}^j(t_i)\bigr)
\end{equation}
with $\tilde{X}_j\pex{N}(0) = X_0$, where the $\tilde{W}^j$'s are $M$
independent discrete-time approximations to the Brownian motion $W$, generated
using a Brownian bridge construction as described above. As in the previous
section, in order to apply this scheme, the terms in the equation must be
rearranged, such that $\tilde{X}_j\pex{N}(t_{i+1})$ only appears on the
left-hand side.

Using parameters $X_0 = 10$, $\sigma = 0.1$ and $r=0.03$ I calculated the values of
$K_m$ explicitly by observing that
\begin{align}
  \expect[X(1)] &= X_0e^r\\
  \Var &= X_0^2e^{2r}(e^{\sigma^2}-1)
\end{align}
The values of $K_m$ and the analytically calculated prices are listed in Table~\ref{tab:analyt}. Moreover I calculated the prices using standard Monte Carlo
simulation with the estimator
\begin{equation}
  \label{eq:5}
  \bar{F}_m = \frac{1}{M}\sum_{j=1}^MF_m(X^j),
\end{equation}
where the $X^j$'s are independent simulations of $X(1)$. The results of are
listed in Table~\ref{tab:mc}. As can be seen however, the estimated Monte Carlo
variance is relatively large. To reduce the variance of the estimator I used
importance sampling as described below.

\subsection*{Importance sampling}
Suppose we define a process $\tilde{W}$ as $W$ with a drift $\mu_m\in\reals$. That
is $\tilde{W}(t) = W(t) + \mu_m t$, and thus $\diff\tilde{W}(t) = \diff W(t) +
\mu_m\,\diff t$. Define further the process $Y$ by the SDE
\begin{equation}
  \label{eq:4}
  \begin{cases}
    \diff Y(t) = r Y(t)\,\diff t + \sigma Y(t)\,\diff\tilde{W}(t) =
    (r+\sigma\mu_m)Y(t)\,\diff t + \sigma Y(t)\,\diff W(t)\\
    Y(0) = X_0
  \end{cases}
\end{equation}
which is a geometric Brownian motion under $\probQ$. Now to estimate the price
$V_m$, we need in particular to estimate the expectation under $\probQ$ of
$F_m(X(1))$. So let $f$ and $g$ denote the pdf's of $X(1)$ and $Y(1)$,
respectively. Then
\begin{align}
  \expect_f[F_m(X(1))] &= \int_{-\infty}^\infty F_m(x)f(x)\,\diff x\\
  &= \int_{-\infty}^\infty F_m(x)\frac{f(x)}{g(x)}g(x)\,\diff x\\
  &= \expect_g\Biggl[F_m(Y(1))\frac{f(Y(1))}{g(Y(1))}\Biggr].
\end{align}
Now since $X$ and $Y$ are geometric Brownian motions their pdf's are given by
\begin{align}
  f(x) &= \frac{1}{\sigma x\sqrt{2\pi}}\exp\Biggl(-\frac{\bigl(\ln\frac{x}{X_0}
    - r + \half\sigma^2\bigr)^2}{2\sigma^2}\Biggr)\\
  g(x) &= \frac{1}{\sigma x\sqrt{2\pi}}\exp\Biggl(-\frac{\bigl(\ln\frac{x}{X_0}
    - r -\sigma\mu_m + \half\sigma^2\bigr)^2}{2\sigma^2}\Biggr)
\end{align}
and so
\begin{align}
  \frac{f(x)}{g(x)} &= \frac{\exp\Biggl(-\frac{\bigl(\ln\frac{x}{X_0} - r +
      \half\sigma^2\bigr)^2}{2 \sigma^2}\Biggr)}{\exp\Biggl(-\frac{\bigl(\ln
      \frac{x}{X_0} - r -\sigma\mu_m + \half\sigma^2\bigr)^2}{2\sigma^2}\Biggr)}\\
  &= \exp\Biggl(\frac{\bigl(\ln\frac{x}{X_0}
    - r -\sigma\mu_m + \half\sigma^2\bigr)^2 - \bigl(\ln\frac{x}{X_0} - r +
    \half\sigma^2\bigr)^2}{2\sigma^2}\Biggr)\\
  &= \exp\Biggl(\frac{\mu_m\bigl(\sigma(\mu_m-\sigma) + 2r - 2\ln\frac{x}{X_0}\bigr)}{2\sigma}\Biggr)
\end{align}
In order for this technique to be beneficial, we need to choose the drift $\mu_m$
such that the event $[Y(1) > K_m]$ has a higher probability of occuring than the
event $[X(1) > K_m]$. One value of $\mu_m$, albeit perhaps not the optimal value,
is such that
\begin{equation}
  \label{eq:6}
  \expect_\probQ[Y(1)] = K_m.
\end{equation}
Since $\expect_\probQ[Y(1)] = X_0 e^{r+\sigma\mu_m}$, this can be obtained by
choosing
\begin{equation}
  \label{eq:7}
  \mu_m = \frac{\ln\frac{K_m}{Y_0} - r}{\sigma}.
\end{equation}
Hence, the new estimator is
\begin{equation}
  \label{eq:9}
  \tilde{F}_m = \frac{1}{M}\sum_{j=1}^M F_m(Y^j) \frac{f(Y^j)}{g(Y^j)},
\end{equation}
where the $Y^j$'s are independent simulations of $Y(1)$. The prices obtained by
this estimator are listed in Table~\ref{tab:mc} along with the Monte Carlo
variances using this estimator.

\subsection*{Conclusion}
Comparing the analytically calculated prices in Table~\ref{tab:analyt} with the
estimated prices stated in Table~\ref{tab:mc}, one can see that the prices
estimated using importance sampling are generally closer to the analytical
values, than the prices estimated using standard Monte Carlo. This is of course
no coincidence, as is obvious by observing that importance sampling reduces the
Monte Carlo variance by one to five orders of magnitude, depending on $m$. The
benefit of using the importance sampling estimator is noted to increase with
$m$ as expected, since $K_m$ moves further out on the tail of $X(1)$ for larger $m$.

\begin{table}[t]
  \centering
  \begin{tabular}{lllll}
    & \multicolumn{2}{l}{Importance sampling} & \multicolumn{2}{l}{Plain Monte Carlo}\\
    $m$ & Price & Variance & Price & Variance \\\midrule
    1 & $1.453\cdot 10^{-01}$ & $3.458\cdot 10^{-02}$ & $1.465\cdot 10^{-01}$ & $1.207\cdot 10^{-01}$ \\
    2 & $2.579\cdot 10^{-02}$ & $1.645\cdot 10^{-03}$ & $2.588\cdot 10^{-02}$ & $2.444\cdot 10^{-02}$ \\
    3 & $2.835\cdot 10^{-03}$ & $2.712\cdot 10^{-05}$ & $2.989\cdot 10^{-03}$ & $2.892\cdot 10^{-03}$ \\
    4 & $2.120\cdot 10^{-04}$ & $1.919\cdot 10^{-07}$ & $2.329\cdot 10^{-04}$ & $2.260\cdot 10^{-04}$ \\
    5 & $1.168\cdot 10^{-05}$ & $7.009\cdot 10^{-10}$ & $1.294\cdot 10^{-05}$ & $1.256\cdot 10^{-05}$ 
  \end{tabular}
  \caption{Prices calculated using the estimators given in \eqref{eq:5} and
    \eqref{eq:9}, for plain Monte Carlo and Monte Carlo with importance
    sampling, respectively. For each price, the corresponding Monte Carlo
    variance is stated as well.}
  \label{tab:mc}
\end{table}

\newpage
\section*{Code}
\lstinputlisting[caption = {\texttt{question1.m}}]{question1.m}
\lstinputlisting[caption = {\texttt{question2.m}}]{question2.m}
\lstinputlisting[caption = {\texttt{brownian.m}}]{brownian.m}
\lstinputlisting[caption = {\texttt{milstein\_scheme.m}}]{milstein_scheme.m}
\lstinputlisting[caption = {\texttt{euler\_scheme.m}}]{euler_scheme.m}

\nocite{*}
\bibliographystyle{plain}
\bibliography{lit}
\end{document}