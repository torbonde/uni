clear; clc; close all; rng(100);
%% Choose parameters
alpha = 0.2;
mu = 0.05;
sigma = 0.1;
r0 = 0.04;

% Check that parameters satisfy the condition for positivity
if alpha*mu >= sigma^2/2
    display('Parameters satisfy the condition for positivity');
else
    display('Parameters does not satisfy the condition for positivity');
end

%% Plot trajectories
M = 100;
N = 10;
X100 = milstein_scheme(alpha, mu, sigma, r0, M, N);
plot(linspace(0,1,2^N+1), X100')
xlabel('Time')
ylabel('Short rate')

%% Estimate K
% Simulate trajectories
M = 2000;
N = 16;
r = milstein_scheme(alpha, mu, sigma, r0, M, N);

K = mean(r(:,end));

% Payoff
F = @(x) max(x-K,0);

%% Estimate errors
% Simulate Brownian motion to use for all timesteps
Nmax = 16;
Wmax = brownian(M,Nmax);

% Preallocate vectors for weak and strong errors
weak_error = zeros(Nmax/2,1);
strong_error = zeros(Nmax/2,1);

for N = 1:(Nmax/2)
    % Pick values from Wmax to use for this iteration
    WN = Wmax(:, linspace(1, 2^Nmax+1, 2^N+1));
    W2N = Wmax(:, linspace(1,2^Nmax+1, 2^(2*N)+1));
    
    % Simulate CIR process using two different timesteps
    rN = milstein_scheme(alpha, mu, sigma, r0, WN);
    r2N = milstein_scheme(alpha, mu, sigma, r0, W2N);
    
    % Estimate expectation of the payoff using two different timesteps
    FN = mean(F(rN(:,end)));
    F2N = mean(F(r2N(:,end)));
    
    % Estimate weak and strong error
    weak_error(N) = abs(FN - F2N);
    strong_error(N) = sqrt(mean(abs(rN(:,end) - r2N(:,end)).^2));
end

%% Estimate rates (orders) of convergence
% dt-values
dts = 1./2.^(1:(Nmax/2));

% Perform least squares
A = [ones(Nmax/2,1), log(dts)'];

b_weak = log(weak_error);
b_strong = log(strong_error);

sol_weak = A\b_weak;
sol_strong = A\b_strong;

C_weak = exp(sol_weak(1));
C_strong = exp(sol_strong(1));

% Convergence rates
q_weak = sol_weak(2);
q_strong = sol_strong(2);

% Residuals
resid_weak = norm(A*sol_weak - b_weak);
resid_strong = norm(A*sol_strong - b_strong);

fprintf('Convergence rates\n')
fprintf('  Weak rate\t Strong rate\n')
fprintf('  %.4f\t %.4f\n', q_weak, q_strong)

fprintf('Residuals\n')
fprintf('  Weak\t\t Strong\n')
fprintf('  %.4f\t %.4f\n', resid_weak, resid_strong)

%% Plot errors
figure;
subplot(1,2,1);
loglog(dts,weak_error,dts,0.1*C_weak*dts.^q_weak,'--');
xlabel('\Delta t');
ylabel('Weak error');

subplot(1,2,2)
loglog(dts,strong_error,dts,0.1*C_strong*dts.^q_strong,'--');
xlabel('\Delta t');
ylabel('Strong error');