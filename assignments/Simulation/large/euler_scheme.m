% Simulate a geometric Brownian motion X using the implicit Euler scheme
function X = euler_scheme(mu, sigma, X0, varargin)
% The varargin-argument is assumed to either contain the a simulated
% Brownian motion W, or M and N determining the number of trajectories of X
% and the number of points in which X is simulated.
if length(varargin) == 1
    W = varargin{1};
    [~, pts] = size(W);
    N = log2(pts-1); % pts == 2^N+1
elseif length(varargin) == 2
    M = varargin{1};
    N = varargin{2};
    W = brownian(M, N);
end

% Preallocate X
X = zeros(size(W));

X(:,1) = X0;
for i = 1:2^N
    dW = W(:,i+1) - W(:,i);
    X(:,i+1) = (X(:,i) + sigma*X(:,i).*dW)/(1-mu/2^N);
end