\documentclass[a4paper]{article}
\input{../../preamble}
\begin{document}
\title{Stochastic Analysis assignment}
\author{David Tor Bonde - 1469023}
\maketitle
\begin{thm}[Main Theorem on Pricing European Options]
  Let $h$ be a non-negative $\calF_T$-measurable random variable such that
  $\expect_\probQ[h^2]<\infty$. Then $h$ is replicable and the value of the
  replicating portfolio at time $t$ is given by
  \begin{equation}
    \label{eq:1}
    \expect_\probQ(e^{-r(T-t)}h|\calF_t).
  \end{equation}
  Moreover, this is the fair price at time $t$ of the European option with
  payoff $h$ at time $T$.
\end{thm}

Now the payoffs of the three option types are given by
\begin{align}
  h_E &= (S_T - K)^+\\
  h_A &= \Bigl(\frac{1}{T}\int_0^T S_t\,\diff t - K\Bigr)^+\\
  h_L &= \Bigl(\min_{t\leq T}S_t - K\Bigr)^+
\end{align}
and their fair prices are
\begin{equation}
  \label{eq:2}
  V_E = \expect_\probQ[e^{-rT}(S_T-K)^+] = S_T \Phi(a) - Ke^{-rT}\Phi(b),
\end{equation}
where $a = \frac{\log(S_T/K) + (r+\half\sigma^2)T}{\sigma\sqrt{T}}$ and $b =
a-\sigma\sqrt{T}$, and
\begin{align}
  V_A &= \expect_\probQ\Biggl[e^{-rT}\Bigl(\frac{1}{T}\int_0^TS_t\,\diff t -
  K\Bigr)^+\Biggr]\\
  V_L &= \expect_\probQ\Bigl[e^{-rT}\bigl(\min_{t\leq T}S_t - K\bigr)^+\Bigr]
\end{align}

Our goal is to compare these three prices, and in particular show the
relationshiprela $V_L \leq V_A \leq V_E$. So let's first see that $V_L \leq V_A$. To
do this, note that
\begin{equation}
  \label{eq:3}
  \min_{t\in[0,T]}S_t = \frac{1}{T}\int_0^T\min_{t\in[0,T]}S_t\,\diff s \leq
  \frac{1}{T}\int_0^TS_t\,\diff t
\end{equation}
and then also
\begin{equation}
  \label{eq:4}
  h_L = \Bigl(\min_{t\in[0,T]}S_t - K\Bigr)^+ \leq
  \Bigl(\frac{1}{T}\int_0^TS_t\,\diff t - K\Bigr)^+ = h_A.
\end{equation}
Discounting both sides and taking expectations with respect to $\probQ$ shows us
that $V_L \leq V_A$. To show that $V_A \leq V_E$ requires a few more
steps. First of all, see that
\begin{equation}
  \label{eq:5}
  h_A = \Bigl(\frac{1}{T}\int_0^T S_t\,\diff t - K\Bigr)^+ =
  \Bigl(\frac{1}{T}\int_0^T(S_t - K)\,\diff t\Bigr)^+
\end{equation}
and recall the decomposition $S_t - K = (S_t-K)^+ - (S_t-K)^-$. Now we can split
the integral in two and get
\begin{align}
  h_A &= \Bigl(\frac{1}{T}\int_0^T(S_t-K)\,\diff t\Bigr)^+\\
  &= \Bigl(\frac{1}{T}\int_0^T(S_t-K)^+\,\diff t -
  \frac{1}{T}\int_0^T(S_t-K)^-\,\diff t\Bigr)^+\\
  &\leq \Bigl(\frac{1}{T}\int_0^T(S_t-K)^+\,\diff t\Bigr)^+\\
  &= \frac{1}{T}\int_0^T(S_t-K)^+\,\diff t.
\end{align}

Furthermore, we have that
\begin{align}
  e^{-rT}h_A &\leq e^{-rT}\frac{1}{T}\int_0^T(S_t-K)^+\,\diff t\\
  &= \frac{1}{T}\int_0^T(e^{-rT}S_t - e^{-rT}K)^+\,\diff t\\
  &\leq \frac{1}{T}\int_0^T(e^{-rt}S_t - e^{-rT}K)^+\,\diff t\\
  &= \frac{1}{T}\int_0^T(\tilde{S}_t - e^{-rT}K)^+\,\diff t,
\end{align}
which, by taking expectations gives us an upper boundary for the price of the
asian option
\begin{equation}
  \label{eq:6}
    V_A = \expect_\probQ[e^{-rT}h_a] \leq \expect_\probQ\Bigl[\frac{1}{T}\int_0^T(\tilde{S}_t - e^{-rT}K)^+\,\diff
    t\Bigr]= \frac{1}{T}\int_0^T\expect_\probQ[(\tilde{S}_t - e^{-rT}K)^+]\,\diff t.
\end{equation}
Here we have used that the integrand is non-negative, which allows us to
interchange the integral and the expectation.

Now define a process $M$ by $M_t = (\tilde{S}_t - e^{-rT}K)^+$ and observe that
$x\mapsto (x)^+$ is a convex function. By Jensens inequality, for $s\leq t$,
\begin{equation}
  \label{eq:10}
  \expect_\probQ(M_t|\calF_s) \geq
  \bigl(\expect_\probQ(\tilde{S}_t-e^{-rT}K|\calF_s)\bigr)^+ = (\tilde{S}_s
  -e^{-rT}K)^+ = M_s.
\end{equation}
Clearly, $M_t$ is $\calF_t$-measurable and
\begin{equation}
  \label{eq:9}
  \expect_\probQ[|M_t|] = \expect_\probQ[|(\tilde{S}_t - e^{-rT}K)^+|] \leq
  \expect_\probQ[|(\tilde{S}_t)^+|] \leq \expect_\probQ[|\tilde{S}_t|] < \infty,
\end{equation}
again using that $\tilde{S}$ is a $\probQ$-martingale, whence $M$ is a
$\probQ$-submartingale. In particular, for all $t\in[0,T]$, 
$M_t\leq \expect_\probQ(M_T|\calF_T)$ so by taking expecations on both sides
\begin{equation}
  \label{eq:11}
  \expect_\probQ[(\tilde{S}_t - e^{-rT}K)^+] \leq \expect_\probQ[(\tilde{S}_T -
  e^{-rT}K)^+].
\end{equation}
Finally, using this and \eqref{eq:6}, we see that
\begin{align}
  V_A &\leq \frac{1}{T}\int_0^T \expect_\probQ[(\tilde{S}_t -
  e^{-rT}K)^+]\,\diff t\\
  &\leq \frac{1}{T}\int_0^T\expect_\probQ[(\tilde{S}_T - e^{-rT}K)^+]\,\diff t\\
  &= \expect_\probQ[(\tilde{S}_T - e^{-rT}K)^+] = e^{-rT}\expect_\probQ[(S_T - e^{-rT}K)^+] = V_E
\end{align}
\end{document}