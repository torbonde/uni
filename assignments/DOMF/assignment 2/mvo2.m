% This function solves the optimization problem EP2
function [sigma,x] = mvo2(Sigma,mu,R)
n = length(mu);
e = ones(n,1);

cvx_begin quiet
    variable y(n)
    minimize (y'*Sigma*y)
    subject to
        mu'*y >= R;
        y'*e == 1;
        y >= 0;
cvx_end

% Return the minimum level of risk, and the portfolio found
sigma = sqrt(cvx_optval);
x = y;