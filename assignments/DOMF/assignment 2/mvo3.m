% This function solves the optimization problem EP3
function x = mvo3(Sigma,mu,delta)
n = length(mu);
e = ones(n,1);

cvx_begin quiet
    variable y(n)
    maximize (mu'*y-delta*y'*Sigma*y)
    subject to
        y'*e == 1;
        y >= 0;
cvx_end

% Return the portfolio found
x = y;