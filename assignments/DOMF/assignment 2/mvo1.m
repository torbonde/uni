% This function solves the optimization problem EP1
function [R,x] = mvo1(Sigma,mu,sigma)
n = length(mu);
e = ones(n,1);

cvx_begin quiet
    variable y(n)
    maximize (mu'*y)
    subject to
        y'*Sigma*y <= sigma^2;
        y'*e == 1;
        y >= 0;
cvx_end

% Return the maximum return, and the portfolio found
R = cvx_optval;
x = y;