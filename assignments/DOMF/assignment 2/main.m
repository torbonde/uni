% main.m

clear; clc; clf;
n = 3;
Sigma = [0.02778, 0.00387, 0.00021; 0.00387, 0.01112, -0.00020; 0.00021, -0.00020, 0.00115];
mu =    [0.10730; 0.07370; 0.06270];
e = ones(n,1);

i = 1;
for sigma = 0.035:0.005:0.065
  [R,x] = mvo1(Sigma,mu,sigma);
  Vec1_sigma(i) = sigma; Vec1_R(i) = mu'*x;
  i = i+1;
end

hold on
  plot(Vec1_sigma.^2,Vec1_R,'o red')
hold off

j = 1;
for R=0.065:0.001:0.080
  [sigma,x] = mvo2(Sigma,mu,R);
  Vec2_R(j) = R; Vec2_sigma(j) = sqrt(x'*Sigma*x);
  j = j+1;
end
hold on
  plot(Vec2_sigma.^2,Vec2_R,'s blue')
hold off

k = 1;
for delta = 3:1:10
  x = mvo3(Sigma,mu,delta);
  Vec3_R(k) = mu'*x; Vec3_sigma(k) = sqrt(x'*Sigma*x);
  k = k+1;
end
hold on
  plot(Vec3_sigma.^2,Vec3_R,'+ black')
  legend('EP1', 'EP2', 'EP3','Location','southeast')
hold off