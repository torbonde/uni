% Filters out values for options which are either inactive (given by
% VolMin) or illiquid (given by IntMin).
function [K, Price, Bid, Ask, Vol, Int] = FilterFacebook(K, Price, Bid, Ask, Vol, Int, VolMin, IntMin)
% Find indices for active options.
active_inds = find(Vol >= VolMin);

% Find indices for liquid options.
liquid_inds = find(Int >= IntMin);

% Intersect vectors to find indices of valid options, i.e. which are both
% active and liquid.
valid_inds = intersect(active_inds, liquid_inds);

% Take only valid options.
K = K(valid_inds);
Price = Price(valid_inds);
Bid = Bid(valid_inds);
Ask = Ask(valid_inds);
Vol = Vol(valid_inds);
Int = Int(valid_inds);