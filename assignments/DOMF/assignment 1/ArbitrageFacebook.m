% Detects arbitrage amongst options with strikes given by K and prices
% given by Price. 
% If no arbitrage is detected, arb will be 0 and y a zero-vector, with the
% same length as K.
% If arbitrage is detected, arb will be 1 and y a vector specifying a
% portfolio taking advantage of the arbitrage-opportunity. Note: the
% indices of y will correspond to those of K, and not of to those of the
% (internally used) K_sorted.
function [arb, y] = ArbitrageFacebook(K, Price)
% In order to detect arbitrage using the chosen method, the strike prices
% will need to be sorted. This is taken care of here, and the prices are
% sorted in the same order.
[K_sorted, I] = sort(K);
Price_sorted = Price(I);

% Create the matrix A given by Eq. (4.10) in Optimization Methods in
% Finance, taking advantage of the fact that we are dealing with European
% Call options.
n = length(K);
A = zeros(n);
A(n,:) = ones(1,n);
for j = 1:n-1
    for i = j:n-1
        A(i,j) = K_sorted(i+1) - K_sorted(j);
    end
end

% Try to solve the optimization problem given by Eq. (4.9) in Optimization
% Methods in Finance.
cvx_begin quiet
    variable x(n)
    minimize (Price_sorted'*x)
    subject to
        A*x >= 0;
cvx_end

% If the optimization problem was unbounded we have detected a type-A
% arbitrage opportunity. The portfolio is given by x, which is reordered to
% correspond to K.
if strcmp(cvx_status, 'Unbounded') == 1
    arb = 1;
    y = x(I);
else % If the solution wasn't unbounded, there is no arbitrage opportunity.
    arb = 0;
    y = zeros(n,1);
end