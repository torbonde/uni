% DATA: Call options on Facebook stock; prices as of 26.1.2015, all options expiring on 30.1.2015

%clear; clc;

% STEP ONE: READ DATA FROM EXCEL FILE TO MATLAB
filename = 'facebook.xlsx';
[K, Price, Bid, Ask, Vol, Int] = ReadFacebook(filename);

% STEP TWO: FILTER THE DATA
VolMin =  300; IntMin = 500;
[K, Price, Bid, Ask, Vol, Int] = FilterFacebook(K, Price, Bid, Ask, Vol, Int, VolMin, IntMin);
n = length(K);

% STEP THREE: CHECK WHETHER YOU CAN DETECT ARBITRAGE IN FILTERED DATA
[arb, y] = ArbitrageFacebook(K, Price);
if arb == 1 
    display('Arbitrage in filtered data with original price');
else
    display('No arbitrage in filtered data with original price');
end    

% STEP FOUR: CHANGE PRICES. AGAIN CHECK WHETHER YOU CAN DETECT ARBITRAGE
SpotPrice = (Bid + Ask)/2;
[arb, y] = ArbitrageFacebook(K, SpotPrice);
if arb == 1 
    display('Arbitrage in filtered data with average price');
else
    display('No arbitrage in filtered data with average price');
end  

% STEP FIVE: FIND PETER'S PORTFOLIO x
x_recovered = FindPetersPortfolio(K);