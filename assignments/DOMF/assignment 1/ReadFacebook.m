% Reads Excel-file specified by the filename-parameter
function [K, Price, Bid, Ask, Vol, Int] = ReadFacebook(filename)
% Read data from Sheet1 into a matrix, with columns
% Strike, Price, Change, Bid, Ask, Volume and Open interest
table = xlsread(filename, 'Sheet1');

% Extract individual columns from the matrix
K = table(:, 1);
Price = table(:, 2);
Bid = table(:, 4);
Ask = table(:, 5);
Vol = table(:, 6);
Int = table(:, 7);