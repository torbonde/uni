% Assuming we have access to a function Payoff, giving the payoff of a
% portfolio of European Call options with strike prices K, reconstructs
% that portfolio.
function x = FindPetersPortfolio(K)
% The way the algorithm works is by sorting the strike prices, and then
% looking at the payoff of the portfolio in between consecutive strike
% prices (and after the last one). So assume we have strike prices
% K_1 < K_2 < ... < K_n. For S <= K_1 we will have Payoff(S) = 0.
% For K_1 < S <= K_2, we will have Payoff(S) = x_1*(S - K_1), where x_1 is
% the number of units of options with strike price K_1 we hold in our
% portfolio. Now for K_2 < S <= K_3 we will have
%         Payoff(S) = x_2*(S - K_2) + x_1*(S - K_1)
% from which we get x_2. In the same way, we can obtain all of the
% portfolio weights x_1,...,x_n.

% Sort strike prices.
[K_sorted, I] = sort(K);

% Find midpoints between strike prices, and a point after the last spot
% price.
diffs = [diff(K_sorted)/2; 1];
midpoints = K_sorted + diffs;

% Find payoffs at the midpoints.
payoffs = arrayfun(@Payoff, midpoints);
n = length(K);
y = zeros(n,1);

% Determine the weights of the portfolio.
for i = 1:n
    y(i) = (payoffs(i) - sum(y*(K_sorted(i) + diffs(i))) + y'*K_sorted)/diffs(i);
end
x = y(I);