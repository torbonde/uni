library("QRM")
# COPULA FITTING AND VISUALIZATION

# Obtaining copula data with probability transform

data.normal <- rmnorm(1000, Sigma=equicorr(3,0.7))
data.t <- rmt(1000, df=4, Sigma=equicorr(3,0.7))
data.gausscopula <- apply(data.normal,2,pnorm)
data.tcopula <- apply(data.t,2,pt,df=4)
pairs(data.gausscopula)
hist(data.gausscopula[,1])
pairs(data.tcopula)

# Bivariate Visualization

ll <- c(0.01,0.99)
BiDensPlot(func=dcopula.gauss,xpts=ll,ypts=ll,Sigma=equicorr(2,0.5))
BiDensPlot(func=dcopula.t,xpts=ll,ypts=ll,df=4,Sigma=equicorr(2,0.5))

# Figure 5.3

data.gausscopula <- rcopula.gauss(2000,Sigma=equicorr(2,0.7))
data.gumbelcopula <- rcopula.gumbel(2000,theta=2, d=2)
data.claytoncopula <- rcopula.clayton(2000, theta=2.2, d=2)
data.tcopula <- rcopula.t(2000, df=4, Sigma=equicorr(2,rho=0.71))
par(mfrow=c(2,2),cex=0.4)
plot(data.gausscopula)
plot(data.gumbelcopula)
plot(data.claytoncopula)
plot(data.tcopula)
par(mfrow=c(1,1))

# Figure 5.4

data.metagauss <- apply(data.gausscopula,2,qnorm)
data.metagumbel <- apply(data.gumbelcopula,2,qnorm)
data.metaclayton <- apply(data.claytoncopula,2,qnorm)
data.metat <- apply(data.tcopula,2,qnorm)
par(mfrow=c(2,2),cex=0.4)
plot(data.metagauss)
plot(data.metagumbel)
plot(data.metaclayton)
plot(data.metat)
par(mfrow=c(1,1))

# the 3D view

ll <- c(-3,3)
normal.metagumbel <- function(x,theta){
  exp(dcopula.gumbel(apply(x,2,pnorm),theta,log=TRUE) + apply(log(apply(x,2,dnorm)),1,sum))
}
normal.metaclayton <- function(x,theta){
  exp(dcopula.clayton(apply(x,2,pnorm),theta,log=TRUE) + apply(log(apply(x,2,dnorm)),1,sum))
}
normal.metat <- function(x,df,Sigma){
  exp(dcopula.t(apply(x,2,pnorm),df,Sigma,log=TRUE) + apply(log(apply(x,2,dnorm)),1,sum))
}

BiDensPlot(dmnorm,xpts=ll,ypts=ll,mu=c(0,0),Sigma=equicorr(2,0.7))
BiDensPlot(normal.metagumbel,xpts=ll,ypts=ll,npts=80,theta=2)
BiDensPlot(normal.metaclayton,xpts=ll,ypts=ll,npts=80,theta=2.2)
BiDensPlot(normal.metat,xpts=ll,ypts=ll,npts=80,df=4,Sigma=equicorr(2,0.71))

BiDensPlot(dmnorm,type="contour",xpts=ll,ypts=ll,mu=c(0,0),Sigma=equicorr(2,0.7))
BiDensPlot(normal.metagumbel,type="contour",xpts=ll,ypts=ll,npts=80,theta=2)
BiDensPlot(normal.metaclayton,type="contour",xpts=ll,ypts=ll,npts=80,theta=2.2)
BiDensPlot(normal.metat,type="contour",xpts=ll,ypts=ll,npts=80,df=4,Sigma=equicorr(2,0.71))


