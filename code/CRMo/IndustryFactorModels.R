remove(list=ls())
load("BerndtReturnData.RData")
require(timeSeries)
date = as.Date(rownames(BerndtReturnData))


head(BerndtReturnData,n=3)
tail(BerndtReturnData,n=3)

# centre and scale

BerndtReturnData = apply(BerndtReturnData,2,function(v){(v-mean(v))/sd(v)})
apply(BerndtReturnData,2,mean)
var(BerndtReturnData)


# define dummies for sectors

(asset.names = colnames(BerndtReturnData))
(n.stocks = ncol(BerndtReturnData))
tech.dum = oil.dum = other.dum = rep(0,n.stocks)
tech.dum[c(4,5,9,13)] = 1
oil.dum[c(3,6,10,11,14)] = 1
other.dum = 1 - tech.dum - oil.dum
A.mat = cbind(tech.dum,oil.dum,other.dum)
dimnames(A.mat) = list(asset.names,c("TECH","OIL","OTHER"))
A.mat

# A simple way to estimate factors

F1 = apply(BerndtReturnData[,(tech.dum==1)],1,mean)
F2 = apply(BerndtReturnData[,(oil.dum==1)],1,mean)
F3 = apply(BerndtReturnData[,(other.dum==1)],1,mean)

F.hat2 = cbind(F1,F2,F3)
colnames(F.hat2) = c("TECH","OIL","OTHER")
F.hat2.std = apply(F.hat2,2,function(v){(v-mean(v))/sd(v)})
var(F.hat2.std)

# More sophisticated way involving cross-sectional regression

F.hat.t = solve(crossprod(A.mat)) %*% t(A.mat) %*% t(BerndtReturnData)
F.hat = t(F.hat.t)
F.hat.std = apply(F.hat,2,function(v){(v-mean(v))/sd(v)})
var(F.hat.std)

# Display factors as time series

F.hat.std.ts = timeSeries(F.hat.std,date)
plot(F.hat.std.ts,type="h")

# Construct the systematic terms for each obligor

systematic.factor = t(A.mat %*% t(F.hat.std))
# colnames(systematic.factor) = asset.names
head(systematic.factor)

# Check factor model assumptions

apply(BerndtReturnData,2,var)
apply(systematic.factor,2,var)

# Estimate beta_i terms by regression

results = matrix(0,nrow = n.stocks,ncol = 1)
rownames(results)=asset.names
for (i in 1:n.stocks){
tmp = lm(BerndtReturnData[,i] ~ systematic.factor[,i] - 1)
results[i,1] = coef(tmp)^2
}
summary(tmp)
results

# Show graphically

par(mfrow=c(3,5),cex=0.5)
for (i in 1:n.stocks){
	plot(systematic.factor[,i],BerndtReturnData[,i],xlab="F",ylab="X",main=asset.names[i])
	abline(0,sqrt(results[i,1]))
	}
par(mfrow=c(1,1))






# Improved factor model (generalized least squares)

E.hat = t(BerndtReturnData) - A.mat %*% F.hat.t
diagD.hat = apply(E.hat,1,var)
Dinv.hat = diag(diagD.hat^(-1))

H.hat = solve(t(A.mat) %*% Dinv.hat %*% A.mat) %*% t(A.mat) %*% Dinv.hat
F.hat.gls.t = H.hat %*% t(BerndtReturnData)
F.hat.gls = t(F.hat.gls.t)

F.hat.gls.std = apply(F.hat.gls,2,function(v){(v-mean(v))/sd(v)})
var(F.hat.gls.std)
systematic.factor = t(A.mat %*% t(F.hat.gls.std))
# colnames(systematic.factor) = asset.names
head(systematic.factor)

# Check factor model assumptions

apply(BerndtReturnData,2,var)
apply(systematic.factor,2,var)

# Estimate beta_i terms by regression

results.B = matrix(0,nrow = n.stocks,ncol = 1)
rownames(results.B)=asset.names
for (i in 1:n.stocks){
tmp = lm(BerndtReturnData[,i] ~ systematic.factor[,i] - 1)
results.B[i,1] = coef(tmp)^2
}
results.B
