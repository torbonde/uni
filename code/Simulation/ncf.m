function y = ncf(x)
y = 0.5*(1+erf(x/sqrt(2)));