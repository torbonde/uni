function x = myncfinv(y)
x = sqrt(2)*erfinv(2*y-1);