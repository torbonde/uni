clear; clc;
N = 1000000;
r = 0.05;
sigma = 0.2;
T = 1;
S0 = 100;
K = 100;
W = sqrt(T)*randn(N,1);

%%%%% (a)
Wtilde = -W;
S = @(w) S0*exp((r-0.5*sigma^2)*T+sigma*w);
f = @(w) exp(-r*T)*max(0,S(w)-K);
fAV = mean(0.5*(f(W) + (f(Wtilde))));
freg = mean(f(W));
fan = european_call(r,sigma,T,S0,K,'value');

% Estimated correlation between f(W) and f(-W)
estcorr = corr(f(W),f(Wtilde));

% Variance reduction (by the formula on the slides):
%   var(fAV) = 0.5*(var(f(W)) + cov(f(W),f(Wtilde))
% dividing both sides by var(f(W)) we get
%   varred = var(fAV)/var(f(W)) = 0.5*(1+corr(f(W),f(Wtilde)))
varred = 0.5*(1+estcorr);

%%%% (b)
c = @(w) exp(-r*T)*S(w);
fw = f(W);
fave = mean(fw);
cw = c(W);
cave = mean(cw);

% estimated lambda
lambda = sum((fw-fave).*(cw-cave))/sum((fw-fave).^2);

fcv = fave - lambda*(cave - S0);

varredcv = 1-corr(fw,cw)^2;
