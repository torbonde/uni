randn('state',100);

clear; clc;
Y0 = 50;
mu = 2;
sigma = .2;
T = 1;
N = 1000;

[Y, h] = myeuler_vec(Y0, mu, sigma, T, N,10);

hold on
plot(h:h:1,Y)
hold off