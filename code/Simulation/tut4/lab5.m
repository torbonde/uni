randn('state',100);

clear; clc;
Y0 = 50;
mu = 2;
sigma = .2;
T = 1;
N = 1000;
M = 1000;

K = Y0*exp(mu*T);% K = E[X_T]
call_payoff = @(x) max(x-K,0);

[Y,h] = myeuler_vec(Y0, mu, sigma, T, N, M);
E_approx = mean(call_payoff(Y(N,:)));

% Actual expected value
d1 = @(y) (log(y./K) + (mu+sigma^2/2)*T)/(sigma*sqrt(T));
d2 = @(y) d1(y) - sigma*sqrt(T);
C = @(y) normcdf(d1(y)).*y - normcdf(d2(y))*K*exp(-mu*T);
E_actual = mean(exp(mu*T)*C(Y(N,:)));

% Error plot
Ys = cell(5,1);
hs = zeros(5,1);
for j = 1:5
    Nj = 2^(j+7);
    [Ys{j}, hs(j)] = myeuler_vec(Y0, mu, sigma, T, Nj, M);
end

% Error plot
strong_error = zeros(4,1);
weak_error = zeros(4,1);

for j = 1:4
   Ef2h = mean(call_payoff(Ys{j}(end,:)));
   Efh = mean(call_payoff(Ys{j+1}(end,:)));
   
   weak_error(j) = abs(Efh-Ef2h);
   strong_error(j) = sqrt(mean(abs(Ys{j+1}(end,:)-Ys{j}(end,:)).^2));
end
plot(hs(2:5), weak_error, hs(2:5), strong_error)