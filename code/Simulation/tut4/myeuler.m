function [Y, X, h] = myeuler(Y0, mu, sigma, T, N)

h= T/N;

dW = sqrt(h)*randn(N,1);
dW(1) = 0;
Y = zeros(N,1);
Y(1) = Y0;

X = zeros(N,1);
X(1) = Y0;
W = cumsum(dW);

for k = 2:N
   Y(k) = Y(k-1) + mu*Y(k-1)*h + sigma*Y(k-1)*dW(k);
   
   X(k) = X(1)*exp((mu-0.5*sigma^2)*(k-1)*h + sigma*W(k));
end