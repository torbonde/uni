randn('state',100);

clear; clc;
Y0 = 50;
mu = 2;
sigma = .2;
T = 1;
N = 1000;

[X, Y, h] = myeuler(Y0, mu, sigma, T, N);

hold on
plot(h:h:1,X,'o')
plot(h:h:1,Y)

hold off
Y(N)
X(N)