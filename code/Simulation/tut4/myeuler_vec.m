function [Y, h] = myeuler_vec(Y0, mu, sigma, T, N, M)

h= T/N;

dW = sqrt(h)*randn(N,M);
dW(1,:) = 0;
Y = zeros(N,M);
Y(1,:) = Y0;

for k = 2:N
   Y(k,:) = Y(k-1,:) + mu*Y(k-1,:)*h + sigma*Y(k-1,:).*dW(k,:);
end